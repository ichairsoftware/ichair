<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php include 'utils/tools.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <title>Submission Preview</title>
  <link href="css/main_<?php print(Tools::getConfig("miscellaneous/submissionSkin"));?>.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="css/print_<?php print(Tools::getConfig("miscellaneous/submissionSkin"));?>.css" rel="stylesheet" type="text/css" media="print" />
  <link rel="SHORTCUT ICON" type="image/png" href="images/icon.png" />
</head>
<body class="nomargin">

<h1>Submission Preview</h1>

<div class="showPreview">
<?php 
print("<img src=\"imgpreview.php?id=".$_GET['id']."&version=".$_GET['version']."\" />");
?>
</div>

<form>
<div class="floatRight">
<input type="submit" class="buttonLink bigButton" value="Close Window" onClick="javascript:window.close();"/>
</div>
</form>

</body>
</html>
