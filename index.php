<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Instructions';
include 'utils/tools.php';
if(!Tools::isConfigured()) {return;}
include 'header.php';

if(file_exists("utils/custom_index_content.part")) {
  readfile("utils/custom_index_content.part");
} else {

?>

<p>Welcome to the <a href="http://lasecwww.epfl.ch/iChair/">iChair</a> Submission server for <a href="<?php print(Tools::getConfig("conference/site")) ?>"><?php Tools::printHTML(Tools::getConfig('conference/name'));?></a>.</p>

<?php if($subHTML) { ?>
  <h2><a href="guidelines/guidelines.html" target="_blank">Submission Guidelines</a></h2>     

  <p>A list of answers to the most frequently asked questions is available in
  the <a href="guidelines/guidelines.html" target="_blank">Submission Guidelines</a> page

  <?php if($subPDF && $subTXT) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a> and <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } else if($subPDF) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a>)
  <?php } else if($subTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>

  . Be sure to read them before submitting your article.</p>

<?php } else if($subPDF) { ?>

  <h2><a href="guidelines/guidelines.pdf" target="_blank">Submission Guidelines</a></h2>     

  <p>A list of answers to the most frequently asked questions is available in
  the <a href="guidelines/guidelines.pdf" target="_blank">Submission Guidelines</a> page

  <?php if($subTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>

  . Be sure to read them before submitting your article.</p>

<?php } else if($subTXT) { ?>

  <h2><a href="guidelines/guidelines.txt" target="_blank">Submission Guidelines</a></h2>     

  <p>A list of answers to the most frequently asked questions is available in
  the <a href="guidelines/guidelines.txt" target="_blank">Submission Guidelines</a> page. 
  Be sure to read them before submitting your article.</p>

<?php } ?>

<h2><a href="submit.php">Submitting a New File</a></h2>

<p>Use the <a href="submit.php">Submission Form</a> to submit a new article.
Be sure to provide a valid email address for the contact author as it will
not be possible to change it afterwards. Your submission must be in
PostScript or PDF. Any other format will be rejected by the server.</p>


<p>Please have a look at the 

<?php if($cfpHTML) { ?>
  <a href="cfp/cfp.html" target="_blank">Call for Papers</a>
  <?php if($cfpPDF && $cfpTXT) { ?>
    (also available in <a href="cfp/cfp.pdf" target="_blank">[pdf]</a> and <a href="cfp/cfp.txt" target="_blank">[text]</a>)
  <?php } else if($cfpPDF) { ?>
    (also available in <a href="cfp/cfp.pdf" target="_blank">[pdf]</a>)
  <?php } else if($cfpTXT) { ?>
    (also available in <a href="cfp/cfp.txt" target="_blank">[text]</a>)
  <?php } ?>
<?php } else if($cfpPDF) { ?>
  <a href="cfp/cfp.pdf" target="_blank">Call for Papers</a>
  <?php if($cfpTXT) { ?>
    (also available in <a href="cfp/cfp.txt" target="_blank">[text]</a>)
  <?php } ?>
<?php } else if($cfpTXT) { ?>
  <a href="cfp/cfp.txt" target="_blank">Call for Papers</a>
<?php } else { ?>
  Call for Papers
<?php } ?>

and at the 

<?php if($subHTML) { ?>
  <a href="guidelines/guidelines.html" target="_blank">Submission Guidelines</a>
  <?php if($subPDF && $subTXT) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a> and <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } else if($subPDF) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a>)
  <?php } else if($subTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>
<?php } else if($subPDF) { ?>
  <a href="guidelines/guidelines.pdf" target="_blank">Submission Guidelines</a>
  <?php if($subTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>
<?php } else if($subTXT) { ?>
  <a href="guidelines/guidelines.txt" target="_blank">Submission Guidelines</a>
<?php } else { ?>
  Submission Guidelines
<?php } ?>

before submitting.</p>

<h2><a href="revise.php">Modifying a Previous Submission</a></h2>

<p>At any time you can modify your submission with the <a
href="revise.php">Revision Form</a>. You will need to provide the unique
ID which was given to you upon the first submission and was also
emailed to the contact author. You can do as many revisions as you want
before the submission deadline.</p>

<h2><a href="withdraw.php">Withdrawing a Submission</a></h2>

<p>You have the possibility to withdraw a submission from the conference
with the <a href="withdraw.php">Withdrawal Form</a>. As for the revisions,
you will need to provide the unique ID which was given to you upon
the first submission and was also emailed to the contact author. Note that
once a paper has been withdrawn it will not be possible to
&quot;un-withdraw&quot; it: you will need to submit it again.</p>

<center>
<table>
  <tr>
    <td>
      <img src="images/uarrow.gif" alt="" />
    </td>
    <td>
      <img src="images/rarrow.gif" alt=""  />
    </td>
  </tr>
  <tr>
    <td>
      <img src="images/larrow.gif" alt=""  />
    </td>
    <td>
      <img src="images/cross.gif" alt=""  />
    </td>
  </tr>
</table>
</center>

<?php } ?>
<?php include("w3c.php"); ?>
<?php include("footer.php"); ?>
