<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Article Discussions';
include '../utils/tools.php';
include 'header.php';

if(($currentReviewerGroup != null) && ($currentReviewer->isAllowedToViewOtherReviews())  && ((Tools::getCurrentPhase() == Tools::$DECISION) || (Tools::getCurrentPhase() == Tools::$REVIEW))) {

    /* Update the number of articles displayed for the current reviewer */
    
    $numberOfArticlesPerPage = Tools::readPost('numberOfArticlesPerPage');
    if(preg_match("/^[1-9][0-9]*$/", $numberOfArticlesPerPage)) {
      $currentReviewer->setNumberOfArticlesPerPage($numberOfArticlesPerPage);
    }

   
    /* If post not empty, set user sortKey profile */

    if(Tools::readPost('setLastVisitDate') == "Set my Last Visit Date to the Current Time") {
      $date = Tools::readPost('date');
      if($currentReviewer->setLastVisitDate($date)) {
	print('<div class="OKmessage">Your last visit date has been successfully set to the current time.</div>');
      }
    }

    /* Display a form allowing to change the number of article displayed on the same page */
    ?>
    <div class="floatRight">
       <form action="discuss.php" method="post">
       <table><tr>
        <td><div class="versionAuthors">Number of articles displayed per page:&nbsp;</div></td>
        <td><input type="text" name="numberOfArticlesPerPage" size="3" class="largeInput" value="<?php print($currentReviewer->getNumberOfArticlesPerPage()); ?>" /></td>
	<td><input type="submit" class="buttonLink" value="Ok" /></td>
       </tr></table>
      </form>
    </div>
    <div class="clear"></div>
    <?php 

    if(Tools::readPost('updateView') == "Update View") {
      $newSortKey = 0x0000;
      if(Tools::readPost('sortKey') == "overallAverage") {
	$newSortKey = Reviewer::$SORT_BY_OVERALL_GRADE;
      } else if(Tools::readPost('sortKey') == "articleNumber") {
	$newSortKey = Reviewer::$SORT_BY_ARTICLE_NUMBER;
      } else if(Tools::readPost('sortKey') == "acceptance") {
	$newSortKey = Reviewer::$SORT_BY_ACCEPTANCE_STATUS;
      } else if(Tools::readPost('sortKey') == "lastModificationDate") {
	$newSortKey = Reviewer::$SORT_BY_LAST_MODIFICATION;
      } else {
	$newSortKey = Reviewer::$SORT_BY_ACCEPTANCE_STATUS_AND_GRADE;
      }
      $currentReviewer->setSortKeyString($newSortKey, 
					 (Tools::readPost('reverseSort') == "yes"),
					 (Tools::readPost('viewType') == "extendedView"),
					 (Tools::readPost('restrict') == "yes"),
					 (Tools::readPost('recent') == "yes"),
					 $currentReviewer->sortUseSnapshot());
    }

    /* set the sort variables according to the user preferences */

    $viewType="simpleView";
    $reverseSort = false;
    $restrict = false;
    $recent = false;
    if($currentReviewer->sortIsExtended()) {
      $viewType="extendedView";
    } 
    if($currentReviewer->sortIsReversed()) {
      $reverseSort = true;
    } 
    if($currentReviewer->sortIsRestricted()) {
      $restrict = true;
    }
    if($currentReviewer->sortIsRecentOnly()) {
      $recent = true;
    }
    $sortKey = $currentReviewer->getSortKey();

    /* Print Sorting box */

    ?>
    <div class="paperBox">
    <div class="paperBoxDetails">
      <form action="discuss.php" method="post"> 
        <center>
	  <table>
	    <tr>
              <td>
	        <input type="radio" class="noBorder" name="viewType" id="simpleViewType" value="simpleView" <?php if($viewType == "simpleView") { ?>checked="checked" <?php } ?>/>
		<label for="simpleViewType">Simple View</label>
              </td>
	      <td>Sorted By:</td>
	      <td>
	        <input type="radio" class="noBorder" name="sortKey" id="sortKeyArticleNumber" value="articleNumber" <?php if($sortKey == Reviewer::$SORT_BY_ARTICLE_NUMBER) { ?>checked="checked" <?php } ?>/>
		<label for="sortKeyArticleNumber">Article Number</label>	        
	      </td>
	      <td>
	        <input type="radio" class="noBorder" name="sortKey" id="sortKeyOverallAverage" value="overallAverage" <?php if($sortKey == Reviewer::$SORT_BY_OVERALL_GRADE) { ?>checked="checked" <?php } ?>/>
		<label for="sortKeyOverallAverage">Overall Grade</label>	        
	      </td>
	      <td>
	        <input type="radio" class="noBorder" name="sortKey" id="sortKeyAcceptance" value="acceptance" <?php if($sortKey == Reviewer::$SORT_BY_ACCEPTANCE_STATUS) { ?>checked="checked" <?php } ?>/>
		<label for="sortKeyAcceptance">Accept &amp; Reject Status</label>	        
	      </td>
              <td> 
                <input type="radio" class="noBorder" name="sortKey" id="sortKeyAcceptanceAndGrade" value="acceptance_and_grade" <?php if($sortKey == Reviewer::$SORT_BY_ACCEPTANCE_STATUS_AND_GRADE) { ?>checked="checked" <?php } ?>/>
                <label for="sortKeyAcceptanceAndGrade">Status &amp; Grade</label>               
              </td>
              <td> 
                <input type="radio" class="noBorder" name="sortKey" id="sortKeyLastModification" value="lastModificationDate" <?php if($sortKey == Reviewer::$SORT_BY_LAST_MODIFICATION) { ?>checked="checked" <?php } ?>/>
                <label for="sortKeyLastModification">Last Modification</label>               
              </td>
	    </tr>
	    <tr>
              <td>
	        <input type="radio" class="noBorder" name="viewType" id="extendedViewType" value="extendedView" <?php if($viewType == "extendedView") { ?>checked="checked" <?php } ?>/>
		<label for="extendedViewType">Extended View</label>
    	      </td>
	      <td>&nbsp;</td>
	      <td colspan="3">
                <input type="checkbox" class="noBorder" name="recent" id="recent" value="yes" <?php if($recent) { ?>checked="checked" <?php } ?>/>      
                <label for="recent">Show only articles modified since your last visit date</label>
	      </td>
              <td colspan="2">
                <input type="checkbox" class="noBorder" name="restrict" id="restrict" value="yes" <?php if($restrict) { ?>checked="checked" <?php } ?>/>      
                <label for="restrict">Show only articles you worked on</label>
              </td>
 	    </tr>
	    <tr>
              <td colspan="2">&nbsp;</td>
              <td colspan="2">
	        <input type="checkbox" class="noBorder" name="reverseSort" id="reverseSort" value="yes" <?php if($reverseSort) { ?>checked="checked" <?php } ?>/>
		<label for="reverseSort">Reverse sort order</label>               
	      </td>
              <td colspan="3">&nbsp;</td>
            </tr>
	  </table>
          <input type="submit" class="buttonLink bigButton" name="updateView" value="Update View" />
	</center>
      <?php if ($currentReviewer->getLastVisitDate() != 0) {?>
        <div class="floatRight">
          Last Visit: 
	  <em>
	    <?php 
	    if(Tools::use12HourFormat()) {
  	      print(gmdate("D, j M Y h:i a", $currentReviewer->getLastVisitDate()));
	    } else {	      
	      print(gmdate("D, j M Y H:i", $currentReviewer->getLastVisitDate()));
	    }
	    ?>
	  </em>
        </div>
      <?php }
      $articleToDisplay = Tools::readPost('articleToDisplay');
      if(preg_match("/^[0-9]+$/", $articleToDisplay)) {
	print('<input type="hidden" name="articleToDisplay" value="'.$articleToDisplay.'"');
      }
      ?>
      </form>
      <div class="clear"></div>
    </div>
    <div class="floatRight">
    <form action="discuss.php" method="post">
      <input type="hidden" name="date" value="<?php print(gmdate("U"));?>" />
      <input type="submit" class="buttonLink" name="setLastVisitDate" value="Set my Last Visit Date to the Current Time"/>
    </form>
    </div>
    <div class="clear"></div>
    </div>
    <?php 
 
  /* Display general messages from the chair */

  $generalMessages = Meeting::getAllNonTrashedGeneralMessages();
  $specificMessages = Meeting::getAllNonTrashedAuthorizedArticleMessagesInDoubleArrayIndexedByArticleNumber($currentReviewer);

  foreach($generalMessages as $message) { ?>
  <center>
  <table>
    <tr>
      <td>
        <div class="paperBoxTitle" style="margin: 0;">
	  <div class="floatRight">Date: <?php print(gmdate("D, j M Y h:i a", $message->getDate())); ?></div>
	  Message from the Chair
   	  <br />
	</div>
      </td>
    </tr>
    <tr>
      <td><div class="messageBox"><?php Tools::printHTMLbr($message->getContent()); ?></div></td>
    </tr>
  </table>
  </center>
  <br />
  <?php } 


  /* Get Articles sorted according to the reviewer choice */

  $allArticles = Article::getAllArticles();
  $articleNumbers = Article::getNotBlockedSortedArticleNumbers($currentReviewer, $sortKey, $reverseSort, $restrict, $recent, $allArticles, $currentReviewer->getAllowedToViewOtherReviews());

  if(count($articleNumbers) == 0) {
    print('<div class="OKmessage">No articles to display.</div>');
    include('footer.php');
    return;
  }

  /* Get the article to display from the POST */
  $articleToDisplay = Tools::readPost('articleToDisplay');
  if(!preg_match("/^[0-9]+$/", $articleToDisplay)) {
    $articleToDisplay = $articleNumbers[0];
  }

  /* Get the range of article to display */

  $rangeOfArticleNumbers=array();
  $startKey = array_search($articleToDisplay, $articleNumbers);
  $currentButtonNbr = floor($startKey/$currentReviewer->getNumberOfArticlesPerPage());
  $startKey = $currentButtonNbr*$currentReviewer->getNumberOfArticlesPerPage();
  $nbrArticles = count($articleNumbers);
  for($i=0; ($i<$currentReviewer->getNumberOfArticlesPerPage()) && ($startKey+$i < $nbrArticles); $i++) {
    $rangeOfArticleNumbers[$i] = $articleNumbers[$startKey+$i];
  }
  
  
  /* Display buttons */

  $numberOfButtons = ceil(count($articleNumbers)/$currentReviewer->getNumberOfArticlesPerPage());
  for($btNbr=0; $btNbr<$numberOfButtons; $btNbr++) {
    ?>
    <div class="floatLeft">
    <form action="discuss.php" method="post">
      <?php $buttonLabel = 'Page '.($btNbr+1); 
      if($sortKey == Reviewer::$SORT_BY_ARTICLE_NUMBER) {
	$buttonLabel = $articleNumbers[$btNbr*$currentReviewer->getNumberOfArticlesPerPage()];
	$end = $articleNumbers[min(count($articleNumbers), ($btNbr+1)*$currentReviewer->getNumberOfArticlesPerPage())-1];
	if($end != $buttonLabel) {
	  $buttonLabel .= ' - ' . $end;
	}
      }
      if($currentButtonNbr == $btNbr) { ?>
        <input type="submit" class="buttonLink currentButton" value="<?php print($buttonLabel); ?>" />
      <?php } else {?>
        <input type="submit" class="buttonLink" value="<?php print($buttonLabel); ?>" />
      <?php } ?>
      <input type="hidden" name="articleToDisplay" value="<?php print($articleNumbers[($numberOfButtons == 1)? 0 :round((count($articleNumbers)-1)*$btNbr/($numberOfButtons-1))]); ?>" />
    </form>
    </div>
    <?php 
  }
  print('<div class="clear"></div>');



  if($viewType == "simpleView") {
    ?>
    <table class="dottedTable">
    <tr>
      <th class="topRow">Your<br />Work</th>
      <th class="topRow">Article<br />Number</th>
      <th class="topRow">
        <table>
          <tr><td class="leftAlign topRow">Title</td></tr>
          <tr><td class="leftAlign topRow">Number of Comments - Date of Last Comment/Review</td></tr>
        </table>      
      </th>
      <th class="topRow">Average<br />Overall Grade</th>
      <th class="topRow">Status</th>
      <th class="topRow">&nbsp;</th>
    </tr>
    <?php    
  } 

  foreach($rangeOfArticleNumbers as $articleNumber) {
    $article = $allArticles[$articleNumber];

    if($viewType == "extendedView") {

    ?>
    <div class="paperBox" id="nbr<?php print($articleNumber); ?>">

    <?php /* Print a title for the discussion */?>

      <div class="paperBoxTitle">
        <div class="paperBoxNumber">Article&nbsp;<?php print($articleNumber) ?></div>
	<div class="floatRight rightAlign"><?php $assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());
           if (!is_null($assignement)) {
	     if($assignement->isSomeWorkDone()){
	       print('Your Work<br>');
	       $assignement->printWorkDone();
	     }
	   } ?></div>
          <?php $article->printTitleAndOther(); ?>
      </div>
      
    <?php /* Print some details about the completed/in progress reviews, grades (if the current reviewer is allowed to see them)... */?>

    <div class="paperBoxDetails">

      <?php /* Display the list of all the completed reviews concerning this article    */?>
      <?php /* If details is set to true, show the details of the review (grades, etc.) */?>
      <?php $assignements  = Assignement::getByArticleNumberAndReviewStatus($articleNumber, Assignement::$COMPLETED); ?>
      <div class="floatRight">
        <?php 
    $acceptance = $article->getAcceptance();
    if($acceptance == Article::$ACCEPT) {?>
        <div class="bigNumber acceptArticle rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign acceptArticle" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$MAYBE_ACCEPT) { ?>
        <div class="bigNumber maybeAcceptArticle rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign maybeAcceptArticle" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$DISCUSS) { ?>
        <div class="bigNumber discussArticle rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign discussArticle" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$MAYBE_REJECT) { ?>
        <div class="bigNumber maybeRejectArticle rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign maybeRejectArticle" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$REJECT) { ?>
        <div class="bigNumber rejectArticle rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign rejectArticle" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$CUSTOM1) { ?>
        <div class="bigNumber customAccept1 rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign customAccept1" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$CUSTOM2) { ?>
        <div class="bigNumber customAccept2 rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign customAccept2" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$CUSTOM3) { ?>
        <div class="bigNumber customAccept3 rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign customAccept3" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$CUSTOM4) { ?>
        <div class="bigNumber customAccept4 rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign customAccept4" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$CUSTOM5) { ?>
        <div class="bigNumber customAccept5 rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign customAccept5" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$CUSTOM6) { ?>
        <div class="bigNumber customAccept6 rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign customAccept6" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else if($acceptance == Article::$CUSTOM7) { ?>
        <div class="bigNumber customAccept7 rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign customAccept7" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } else { ?>
        <div class="bigNumber voidArticle rightAlign" title="Last Computed Average Overall Grade"><?php print($article->roundAverageOverallGrade(2)); ?></div>
        <div class="rightAlign voidArticle" title="Accept &amp; Reject Status"><?php $article->printAcceptance(); ?></div>
<?php    } ?>
      </div>
      <?php if(($assignements == null) || (count($assignements) == 0)) { ?>
        <div class="versionAbstract">
	  No completed review at this time.
	</div>
      <?php } else { ?> 
	  <table class="smallCentered">
	    <tr>
	      <th>&nbsp;</th>
	      <?php ReviewReader::printReviewRowHead(); ?> 
	    </tr>
	    <?php 
	    foreach($assignements as $assignement) {
	      $reviewerNumber = $assignement->getReviewerNumber();
	      $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
	      $reviewReader = new ReviewReader();
	      $reviewReader->createFromXMLFile($assignement);
	      ?>
	      <tr>
	     <?php if (($currentReviewer->getReviewerNumber() == $reviewerNumber) && (Tools::getCurrentPhase() != Tools::$DECISION) && ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP)) { ?>
 		<td class="button">
		  <div class="popUp">
		    <form action="review_article.php" method="post" target="_blank">
	              <input type="hidden" name="articleNumber" value="<?php print($articleNumber); ?>" />
	              <input type="submit" class="buttonLink" value="&nbsp;&nbsp;Edit&nbsp;&nbsp;" />
                    </form>
		    <div class="hidden">
		      <?php $reviewReader->printDetailsBoxPopUp(); ?>
		    </div>
		  </div>
		</td>
             <?php } else { ?> 
		<td class="button">
		  <div class="popUp">
		    <form action="discuss_review.php" method="post" target="_blank">
	              <input type="hidden" name="articleNumber" value="<?php print($articleNumber); ?>" />
	              <input type="hidden" name="reviewerNumber" value="<?php print($reviewerNumber); ?>" />
	              <input type="submit" class="buttonLink" value="Details" />
                    </form>
		    <div class="hidden">
		      <?php $reviewReader->printDetailsBoxPopUp(); ?>
		    </div>
		  </div>
		</td>
	     <?php } ?>
		<?php $reviewReader->printReviewRow(false); ?>	
	      </tr>
	    <?php } ?>
	  </table>
      <?php } ?>
      <div class="clear"></div>

      <?php 
	 /* Check if the current reviewer has made any personnal comments in his review of the article*/
	 $assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());
         $reviewReader = new ReviewReader();
	 $reviewReader->createFromXMLFile($assignement);
	 if (trim($reviewReader->getPersonalNotes()) != "") {
	   print('Personal Notes:<div class="chairComments">');
	   Tools::printHTMLbr($reviewReader->getPersonalNotes());
	   print('</div>');
	 }

         /* Display a button that gives to access to the details of the discussion */
	 ?>
      <table class="smallCentered"><tr><td class="button">
      <div class="popUp">
        <form action="discuss_article.php?articleNumber=<?php print($articleNumber); ?>#firstNotSeen" target="_blank" method="post">
	  <input type="submit" class="buttonLink" value="Reviews &amp; Discussion" />
        </form>
	 <div class="hidden">
	 <div class="paperBoxDetails">
	 <?php $article->printLastComments(8); ?>
	 </div>
	 </div>
      </div>
      </td>
      <td>
      <?php $article->printLastModificationDateAndNumberOfComments($currentReviewer); ?>
      </td>
      </tr></table>
      <?php if(array_key_exists($articleNumber, $specificMessages) && !is_null($specificMessages[$articleNumber])) { 
        foreach($specificMessages[$articleNumber] as $message) { ?>
          <div class="messageBox">
            <?php 
	    Tools::printHTMLbr($message->getContent()); 
	    ?>
          </div>
        <?php } ?>
      <?php } ?>
      <?php /* End of paperBoxDetails */?>
    </div>

  </div>

  <?php /* END OF EXTENDED VIEW, BEGINING OF SIMPLE VIEW */?>

  <?php } else { ?>

  <tr>
    <td>
      <?php $assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());
         if (!is_null($assignement)) {
           $assignement->printWorkDone();
         } ?>
    </td>
    <td><div class="bigNumber"><?php print($articleNumber);?></div></td>
    <td>
      <table>
        <tr><td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),60); ?></td></tr>
        <tr><td class="leftAlign"><?php $article->printLastModificationDateAndNumberOfComments($currentReviewer); ?></td></tr>
      </table>
    </td>
    <?php 
    $acceptance = $article->getAcceptance();
    if($acceptance == Article::$ACCEPT) {
      print('<td><div class="bigNumber acceptArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="acceptArticle">Accept</div></td>');
    } else if($acceptance == Article::$MAYBE_ACCEPT) {
      print('<td><div class="bigNumber maybeAcceptArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="maybeAcceptArticle">Maybe Accept</div></td>');
    } else if($acceptance == Article::$DISCUSS) {
      print('<td><div class="bigNumber discussArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="discussArticle">Discuss</div></td>');
    } else if($acceptance == Article::$MAYBE_REJECT) {
      print('<td><div class="bigNumber maybeRejectArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="maybeRejectArticle">Maybe Reject</div></td>');
    } else if($acceptance == Article::$REJECT) {
      print('<td><div class="bigNumber rejectArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="rejectArticle">Reject</div></td>');
    } else if($acceptance == Article::$CUSTOM1) {
      print('<td><div class="bigNumber customAccept1">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="customAccept1">' . Tools::getCustomAccept1() . '</div></td>');
    } else if($acceptance == Article::$CUSTOM2) {
      print('<td><div class="bigNumber customAccept2">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="customAccept2">' . Tools::getCustomAccept2() . '</div></td>');
    } else if($acceptance == Article::$CUSTOM3) {
      print('<td><div class="bigNumber customAccept3">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="customAccept3">' . Tools::getCustomAccept3() . '</div></td>');
    } else if($acceptance == Article::$CUSTOM4) {
      print('<td><div class="bigNumber customAccept4">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="customAccept4">' . Tools::getCustomAccept4() . '</div></td>');
    } else if($acceptance == Article::$CUSTOM5) {
      print('<td><div class="bigNumber customAccept5">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="customAccept5">' . Tools::getCustomAccept5() . '</div></td>');
    } else if($acceptance == Article::$CUSTOM6) {
      print('<td><div class="bigNumber customAccept6">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="customAccept6">' . Tools::getCustomAccept6() . '</div></td>');
    } else if($acceptance == Article::$CUSTOM7) {
      print('<td><div class="bigNumber customAccept7">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="customAccept7">' . Tools::getCustomAccept7() . '</div></td>');
    } else {
      print('<td><div class="bigNumber voidArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
      print('<td><div class="voidArticle">-</div></td>');
    }
    ?>
    <td>
      <form action="discuss_article.php?articleNumber=<?php print($articleNumber); ?>#firstNotSeen" method="post" target="_blank">
	<input type="submit" class="buttonLink" value="Reviews &amp; Discussion" />
      </form>
    </td>
  </tr>        

  <?php } ?>

  <?php } ?>
  <?php 
  if($viewType == "simpleView") {
    print('</table>');
  } else {
  ?>   
<div class="clear bottomSpacer"></div>
<?php }} ?>
<?php include('footer.php'); ?>

