<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Load Submissions for Review';
include '../utils/tools.php';
include 'header_reduced.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $submissionNumber = $_GET['submissionNumber'];
  
  $submission = Submission::getBySubmissionNumber($submissionNumber);

  /* if the submission number was indeed a submission, $submission is not null */
  if(!is_null($submission)) {

    $version = $submission->getLastVersion();
    
    print('<div class="paperBox"><div class="paperBoxTitle">');
    print('<div class="paperBoxNumber">Submission ' . $submissionNumber . '</div>');
    $count=2;
    if(Tools::useCategory()) { 
      print('Category: ' . htmlentities(Tools::getCategoryNameById($version->getCategory()), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '<br/>');
      $count--;
    }
    if(Tools::useKeywords()) { 
      print('Keywords: ' . htmlentities($version->getKeywords(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '<br/>');
      $count--;
    }
    for (;$count>0; $count--){
      print('&nbsp;<br/>');
    }
    print('</div>');    
    if ($submission->getIsCommitteeMember()) {
      print('<div class="paperBoxDetailsCommittee">');
    } else {
      print('<div class="paperBoxDetails">');
    }
    if (Tools::useCountry()) {
      Tools::printContinentDivs($version->getCountryArray());
    } else {
      print('<div><div><div>');
    }
    $submission->printCustomCheckboxes(true);
    print('<div class="versionTitle">Title: ' . htmlentities($version->getTitle(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . "</div>\n");
    print('<div class="versionAuthors">Authors: ' . htmlentities($version->getAuthors(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . "</div>\n");
    if(Tools::useAffiliations()) {
      print("Affiliations: " . htmlentities($version->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') ."<br />\n");
    }
    if(Tools::useCountry()) {
      print("Countries: " . htmlentities($version->getCountry(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') ."<br />\n");
    }
    if(Tools::useAbstract()) {
      print('Abstract:<br /><div class="versionAbstract">' . nl2br(htmlentities($version->getAbstract(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')) . '</div>');
    }
    print('Chair comments:<br />');
    $submission->printAllChairComments(); 
    $submission->printJavascriptWarnings();
    $submission->printMD5collisions();
    print("</div></div></div></div></div>");
    
  }
  
  ?>

  <center>
    <input type="submit" class="buttonLink bigButton" value="Close this Window" onclick="javascript:window.close()" />
  </center>

<?php } ?>

<?php include('footer.php'); ?>
