<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Personal Profile";
include '../utils/tools.php';
include "header.php";

if (!is_null($currentReviewer)) {

$fullName;
$email;

$allcategories;
if (Tools::useCategory()) {
  $allcategories = Tools::getAllCategories();
} 
  
/* Check if the full name and mail are in the post. If not, see in DB */

if(count($_POST) != 0) {

  $fullName = Tools::readPost('fullName');
  $email = Tools::readPost('email');
  $preferedCategories='';
  foreach($allcategories as $catid => $category) {
    if (Tools::readPost('cat_'.$catid) != "") {
      $preferedCategories .= ';' . $catid;
    }
  }
  $preferedCategories = substr($preferedCategories,1);
  $autoVisit = (Tools::readPost('autoVisit') == 'yes');
  
  if(!Tools::isValidEmail($email)) {
    ?>
      <div class="ERRmessage">Please provide a valid email address (either a single email address, or a comma separated list of addresses). Your profile was not updated. </div>
    <?php 
  } else {
    $currentReviewer->setFullName(trim($fullName));
    $currentReviewer->setEmail(trim($email));
    $currentReviewer->setPreferedCategories($preferedCategories);
    $currentReviewer->setAutoVisitDate($autoVisit);
    $currentReviewer->updateInDB();
    Log::logEditProfile($currentReviewer);
    ?>
      <div class="OKmessage">Your profile was succesfully updated. </div>
    <?php 
  }

} else {
  $fullName = $currentReviewer->getFullName();
  $email = $currentReviewer->getEmail();
}

?>
<center>
<table><tr class="topAlign"><td><div class="profileLeft"><center>
<h2>Personal Information</h2>
<form action="profile.php" method="post">
  <table>
    <tr><td>Full Name:</td></tr>
    <tr><td><input type="text" size="50" name="fullName" value="<?php Tools::printHTML($fullName); ?>" /></td></tr>
    <tr><td>E-mail Address:</td></tr>
    <tr><td><input type="text" size="50" name="email" value="<?php Tools::printHTML($email); ?>" /></td></tr>
    <?php if((Tools::useCategory()) && ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP)) {?>
    <tr>
      <td>Please choose among the conference main categories</td>
    </tr>
    <tr>
      <td>the ones which best fit your review preferences:</td>
    </tr>
    <tr><td>
    <table>
      <?php 
        $preferedCategories = ' ;' . $currentReviewer->getPreferedCategory() . ';';
        foreach($allcategories as $catid => $category) {
          if (strpos($preferedCategories,';'.$catid.';')) {
            print('<tr><td><input type="checkbox" class="noBorder" name="cat_' . $catid . '" id="cat_' . $catid . '" values="yes" checked="checked"/>');
          } else {
            print('<tr><td><input type="checkbox" class="noBorder" name="cat_' . $catid . '" id="cat_' . $catid . '" values="yes"/>');
          }
          print('</td><td><label for="cat_' . $catid . '">' . htmlentities($category, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</label></td></tr>');
        }
      ?>
    </table>
    </td></tr>
    <?php }?>
    <tr>
      <td>
        <input class="noBorder" type="radio" name="autoVisit" value="yes" id="autoVisitYes"<?php if ($currentReviewer->useAutoVisitDate()) print(' checked="checked"');?>/>
        <label for="autoVisitYes">Automatically update my last visit date</label>
      </td>
    </tr>
    <tr>
      <td>
        <input class="noBorder" type="radio" name="autoVisit" value="no" id="autoVisitNo"<?php if (!$currentReviewer->useAutoVisitDate()) print(' checked="checked"');?>/>
        <label for="autoVisitNo">Only update my last visit date manually</label>
      </td>
    </tr>
  </table>   
  <input type="submit" class="buttonLink bigButton" name="buttonName" value="Update Profile" />
</form>
</center></div></td>
<td><div><center>
<h2>Password</h2>
<form action="profile_result.php" method="post">
  <table>
    <tr>
      <td>Old Password:</td>
      <td><input type="password" name="old" /></td>
    </tr>
    <tr>
      <td>New Password:</td>
      <td><input type="password" name="new1" /></td>
    </tr>
    <tr>
      <td>Repeat New Password:</td>
      <td><input type="password" name="new2" /></td>
    </tr>
  </table>
  <input type="submit" class="buttonLink bigButton" name="buttonName" value="Update Password" />
</form>
</center></div></td></tr></table>
</center>
<?php } ?>
<?php include('footer.php'); ?>
