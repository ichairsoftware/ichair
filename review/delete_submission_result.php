<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Delete Submission';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

$submission = Submission::getBySubmissionNumber(Tools::readPost('submissionNumber'));

if(!is_null($submission)) {

  $status = "";
  $title = $submission->getLastVersion()->getTitle();
  if(!$submission->deleteSubmission()) {
    print("<div class=\"ERRmessage\">Submission number "
	  . $submission->getSubmissionNumber() .
	  " was deleted from the database, but its directory could not be removed.</div>");

  } else {
    Log::logDeleteSubmission($submission,$title,$currentReviewer);
    print("<div class=\"OKmessage\">Submission number "
	  . $submission->getSubmissionNumber() .
	  " was deleted successfully.</div>");
  }
  ?>
  <form action="examine.php#<?php print($submission->getSubmissionNumber()); ?>" method="post">
    <div class="floatRight">
    <input type="submit" class="buttonLink bigButton" value="Ok" />
    </div>
  </form>
  <?php 

}}
?>
</body>
</html>

