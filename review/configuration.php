<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Configuration';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $configFileName = Tools::getConfig('server/reviewsPath') . "reviews_config.xml";
  
  /***************************************************************************/
  /* Display the config.xml file in an html format using the config.xsl file */
  /***************************************************************************/
  
  $config = new DOMDocument();
  /* The .xml is always supposed to be valid, as, at each update, we make sure that the */
  /* updated .xml file is valid.                                                        */
  $config->load($configFileName);
  $config->encoding = "iso-8859-1";

  $styleSheet = new DOMDocument();
  $styleSheet->load("../utils/config_chair.xsl");
  
  $xsl = new XSLTProcessor();
  $xsl->importStyleSheet($styleSheet);
  
  print($xsl->transformToXML($config));
  
}

include('footer.php');
