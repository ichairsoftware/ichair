<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
header("Content-type: text/html; charset=iso-8859-1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1" />
<title><?php print(htmlentities(Tools::getConfig("conference/name"), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')." - ".$page_title); ?></title>
<link href="../css/review_<?php print(Tools::getConfig("miscellaneous/reviewSkin"));?>.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/print_<?php print(Tools::getConfig("miscellaneous/reviewSkin"));?>.css" rel="stylesheet" type="text/css" media="print" />
<link rel="SHORTCUT ICON" type="image/png" href="../images/icon_review.png" />
</head>
<body>

<?php 

/* Before the reviewer database can be accessed, the admin has to configure it properly. */
/* The path to the reviews must exist and so shall the config.xml file */
if(!file_exists('../utils/config.xml') || !file_exists(Tools::getConfig('server/reviewsPath'))) {
    ?>
    <div class="ERRmessage">The path to the reviewers database does not seem
    to be configured yet. Please use the admin functionalities of this
    software to configure iChair according to your own needs. For this problem, 
    the <em>Reviews Path</em> should be configured properly.</div>
    <?php 
    return false;
}

$currentReviewerGroup = Reviewer::getGroupByLogin(); 
$currentReviewer = Reviewer::getReviewerByLogin(); 
if(!is_null($currentReviewer)) {

$currentReviewer->autoUpdateVisitDate();

/* boolean used to know if there are some review guidelines files*/
$revHTML = Tools::hasReviewGuidelinesHTML("..");
$revPDF  = Tools::hasReviewGuidelinesPDF("..");
$revTXT  = Tools::hasReviewGuidelinesTXT("..");

?>


<div class="navigate">

  <?php if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) { ?>
  <div class="navigateTitle"><a href="index_chair.php">Chair Menu</a></div>
  <div class="navigateLinks tinymargins">
    <p><a href="index_chair.php">Chairs Main Page</a></p>
    <p><a href="mailing.php">Mass Mailing</a></p>
    <p><a href="meeting_edit.php">Messages to Committee</a></p>
    <p><a href="configuration.php">Select iChair Phase</a></p>
    <p><a href="export_list.php">Export Article List</a></p>
    <p><a href="stats.php">Conference Statistics</a></p>
  </div>
  <div class="navigateLinks">
    <?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$SUBMISSION)) { ?>
      <p><a href="examine.php">Current Submissions</a></p>
      <p><a href="transfer.php">Load Submissions for Review</a></p>
    <?php } ?>
    <?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() != Tools::$DECISION)) { ?>
      <p><a href="assign_reviewers_list.php">Assign Reviewers to Articles</a></p>
      <p><a href="assign_articles_list.php">Assign Articles to Reviewers</a></p>
    <?php } ?>
    <?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$ASSIGNATION)) { ?>
      <p><a href="automatic_assignation.php">Automatic Assignment</a></p>
    <?php } ?>
    <?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$REVIEW)) { ?>
      <p><a href="supervise_reviewers.php">Reviewers Access Privileges</a></p>
    <?php } ?>
    <?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) { ?>
      <p><a href="accept_and_reject.php">Accept &amp; Reject</a></p>
    <?php } ?>
  </div>
  <?php } ?>

  <div class="navigateTitle"><a href="index.php">Reviewer Menu</a></div>
    <div class="navigateLinks">
      <p><a href="index.php">Main Page</a></p>

      <?php if($revHTML) { ?>
        <p class="guidelines"><a class="guidelines" href="guidelines/guidelines.html" target="_blank">Review Guidelines</a>
        <?php if($revPDF) { ?>
          <a class="noblock" href="guidelines/guidelines.pdf" target="_blank">[pdf]</a>
        <?php } 
           if($revTXT) { ?>
          <a class="noblock" href="guidelines/guidelines.txt" target="_blank">[text]</a>
        <?php } ?>
        </p>
      <?php } else if($revPDF) { ?>
        <p class="guidelines"><a class="guidelines" href="guidelines/guidelines.pdf" target="_blank">Review Guidelines</a>
        <?php if($revTXT) { ?>
          <a class="noblock" href="guidelines/guidelines.txt" target="_blank">[text]</a>
        <?php } ?>
        </p>
      <?php } else if($revTXT) { ?>
        <p><a href="guidelines/guidelines.txt" target="_blank">Review Guidelines</a></p>
      <?php } ?>

      <p><a href="meeting_show.php">Messages from Chair</a></p>
      <?php if(Tools::getCurrentPhase() != Tools::$DECISION) {?>
        <p><a href="profile.php">Personal Profile</a></p>
      <?php } ?>
      <?php if(Tools::getCurrentPhase() == Tools::$ELECTION) { ?>
        <p><a href="affinity_list.php">Preferred Articles</a></p>
      <?php } ?>
      <?php if((Tools::getCurrentPhase() == Tools::$REVIEW) && ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP)) { ?>
        <p><a href="review.php">Your Reviews</a></p>
      <?php } ?>
      <?php if(((Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) && ($currentReviewer->isAllowedToViewOtherReviews())) { ?>
          <p><a href="discuss.php">Reviews &amp; Discussions</a></p>
      <?php } ?>
      <p class="topLine"><a href="about.php">About iChair</a></p>
    </div>

  <div class="navigateTitle"><a href="http://www.time.gov/timezone.cgi?UTC/s/0/java" target="_new">Current Time</a></div>
  <div class="navigateTimes">
    <div class="timeTitleTop">UTC Time</div>
    <?php if(Tools::use12HourFormat()) { ?>
      <div class="time"><?php print(gmdate("j M y - h:i&#160;a")); ?></div>
    <?php } else {?>
      <div class="time"><?php print(gmdate("j M y - H:i")); ?></div>      
    <?php } ?>
  </div>

  <?php if(Tools::getCurrentPhase() == Tools::$ELECTION) {?>
  <div class="navigateTitle">Preferred Articles Deadline</div>
  <div class="navigateTimes">
    <div class="timeTitleTop">UTC Time</div>
    <div class="time">
       <?php 
         $U = Tools::getUFromDateAndTime(Tools::getConfig("review/preferredDeadlineDate"), Tools::getConfig("review/preferredDeadlineTime"));
         if(Tools::use12HourFormat()) {
	    print(gmdate("j M y - h:i&#160;a", $U)); 
	 } else {
	    print(gmdate("j M y - H:i", $U)); 
	 }
       ?>
    </div>
    <div class="timeTitle">Time left</div>
    <div class="time">
    <?php 
      $timeLeft = $U - gmdate("U");
      if($timeLeft > 0) {
        print(Tools::stringTimeLeft($timeLeft));
      } else {
        print("<div class=\"timeUp\">Time is up!</div>");
      }
    ?>
    </div>
  </div>
  <?php } ?>


  <?php if((Tools::getCurrentPhase() == Tools::$REVIEW) && (!Tools::hideTimeBox())) {?>
  <div class="navigateTitle">Review Deadline</div>
  <div class="navigateTimes">
    <div class="timeTitleTop">UTC Time</div>
    <div class="time">
      <?php 
        $U = Tools::getUFromDateAndTime(Tools::getConfig("review/deadlineDate"), Tools::getConfig("review/deadlineTime"));
        if(Tools::use12HourFormat()) {
	    print(gmdate("j M y - h:i&#160;a", $U)); 
        } else {
	    print(gmdate("j M y - H:i", $U)); 
	}
      ?>
    </div>
    <div class="timeTitle">Time left</div>
    <div class="time">
    <?php 
      $timeLeft = $U - gmdate("U");
      if($timeLeft > 0) {
        print(Tools::stringTimeLeft($timeLeft));
      } else {
        print("<div class=\"timeUp\">Time is up!</div>");
      }
    ?>
    </div>
  </div>
  <?php } ?>

  <?php if(($currentReviewerGroup == Reviewer::$CHAIR_GROUP) && (Tools::getCurrentPhase() == Tools::$SUBMISSION)) { ?>
  <div class="navigateTitle">Server Shutdown</div>
  <div class="navigateTimes">
    <div class="timeTitleTop">UTC Time</div>
    <div class="time">
      <?php 
        $U = Tools::getUFromDateAndTime(Tools::getConfig("server/shutdownDate"), Tools::getConfig("server/shutdownTime"));
        if(Tools::use12HourFormat()) {
	    print(gmdate("j M y - h:i&#160;a", $U)); 
        } else {
	    print(gmdate("j M y - H:i", $U)); 
	}
      ?>
    </div>
    <div class="timeTitle">Time left</div>
    <div class="time">
    <?php 
      $timeLeft = $U - gmdate("U");
      if($timeLeft > 0) {
        print(Tools::stringTimeLeft($timeLeft));
      } else {
        print("<div class=\"timeUp\">Time is up!</div>");
      }
    ?>
    </div>
  </div>
  <?php } ?>

</div>

<h1><?php print(htmlentities(Tools::getConfig("conference/name"), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')." - " . htmlentities($currentReviewer->getFullName()) . " - ".$page_title); ?></h1>
<?php } ?>
