<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Messages to Committee';
include '../utils/tools.php';
include 'header.php';
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

$articles = Article::getAllArticles();

/* If a new message is added, the 'article' field in the POST in non-empty */

if(Tools::readPost('article') != "") {
  $articleNumber = explode(" ", Tools::readPost('article'));
  $articleNumber = $articleNumber[0];
  $content = Tools::readPost('newContent');
  if((preg_match("/^[0-9]+$/" , $articleNumber)) || ($articleNumber == "None")) {
    Meeting::addMessage($articleNumber, $content, $currentReviewer);
  }
}


/* If a message has to be trashed, the 'trash' field of the POST variable is not empty */

if(Tools::readPost('trash') != "") {

  $messageNumber = Tools::readPost('messageNumber');
  if(preg_match("/^[0-9]+$/" , $messageNumber)) {
    Meeting::trashMessage($messageNumber);
  }

}

/* If a message has to be updated, the 'update' field of the POST variable is not empty */

if(Tools::readPost('update') != "") {

  $messageNumber = Tools::readPost('messageNumber');
  $content = Tools::readPost('content');
  if(preg_match("/^[0-9]+$/" , $messageNumber)) {
    Meeting::updateMessage($messageNumber, $content, $currentReviewer);
  }

}

/* If a message has to be deleted, the 'delete' field of the POST variable is not empty */

if(Tools::readPost('delete') != "") {

  $messageNumber = Tools::readPost('messageNumber');
  if(preg_match("/^[0-9]+$/" , $messageNumber)) {
    Meeting::deleteMessage($messageNumber);
  }

}


?>

<?php /* Display a box that allows to add a comment */ ?>

<center>
  <form method="post" action="meeting_edit.php">
    <textarea name="newContent" cols="80" rows="5"></textarea><br />
    <table><tr style="valign: top;">
      <td><?php Article::printSelectArticle(); ?></td>
      <td><input type="submit" class="buttonLink" value="Add Comment" /></td>
    </tr></table>
  </form>
</center>


<h2>Displayed Messages</h2>

<?php 

$generalMessages = Meeting::getAllNonTrashedGeneralMessages();
$specificMessages = Meeting::getAllNonTrashedArticleMessages();

if((count($generalMessages) + count($specificMessages)) == 0) {
  print('No message yet!');
}

if(count($generalMessages) != 0) {

  /* Display a list of "general" purpose messages together with buttons that allow to update/delete the message */
  ?>
  <center>
    <?php foreach($generalMessages as $message) { ?>
      <form method="post" action="meeting_edit.php">
        <input type="hidden" name="messageNumber" value="<?php print($message->getMessageNumber()); ?>" />
        <table>
	  <tr>
	    <td>
	      <div class="paperBoxTitle" style="margin: 0;">
	        <div class="floatRight"><em><?php print(gmdate("D, j M Y h:i a", $message->getDate())); ?></em></div>
   	        <br />
	      </div>
	    </td>
	  </tr>
	  <tr>
	    <td><textarea name="content" cols="80" rows="5"><?php Tools::printHTML($message->getContent()); ?></textarea></td>
	  </tr>
	  <tr>
	    <td style="text-align: right;">
	      <input name="update" type="submit" class="buttonLink" value="Update" />
	      <input name="trash" type="submit" class="buttonLink" value="Trash" />
	    </td>
	  </tr>
        </table>
      </form>
      <br />
    <?php } ?>
  </center>
  <?php 
}

if(count($specificMessages) != 0) {

  /* Display a list of "general" purpose messages together with buttons that allow to update/delete the message */
  ?>
  <center>
    <?php foreach($specificMessages as $message) { ?>
      <?php $article = $articles[$message->getArticleNumber()]; ?>
      <form method="post" action="meeting_edit.php">
        <input type="hidden" name="messageNumber" value="<?php print($message->getMessageNumber()); ?>" />
        <table style="border-collapse: collapse;">
	  <tr>
	    <td>
	      <div class="paperBoxTitle" style="margin: 0;">
	        <div class="floatRight"><em><?php print(gmdate("D, j M Y h:i a", $message->getDate())); ?></em></div>
   	        <b><?php print('Article ' . $message->getArticleNumber()); ?>: </b><?php if (!is_null($article)) Tools::printHTMLsubstr($article->getTitle(), 30);  else print('<em>Article withdrawn</em>');?>
	      </div>
	    </td>
	  </tr>
	  <tr>
	    <td><textarea name="content" cols="80" rows="5" style="margin: 0;"><?php Tools::printHTML($message->getContent()); ?></textarea></td>
	  </tr>
	  <tr>
	    <td style="text-align: right;">
              <a href="discuss_article.php?articleNumber=<?php print($message->getArticleNumber()); ?>#firstNotSeen" class="buttonLink" target="_blank">Reviews & Discussion</a>
	      <input name="update" type="submit" class="buttonLink" value="Update" />
	      <input name="trash" type="submit" class="buttonLink" value="Trash" />
	    </td>
	  </tr>
        </table>
      </form>
      <br />
    <?php } ?>
  </center>
  <?php 
}

?>



<h2>Deleted Messages</h2>


<?php 

$messages = Meeting::getAllTrashedMessages();
if(count($messages) == 0) {
  print('<p>No message yet!</p>');
} else {

  /* Display a list of "general" purpose messages together with buttons that allow to update/delete the message */
  ?>
  <center>
    <?php foreach($messages as $message) { 
      $articleNumber = $message->getArticleNumber();
      $article = null;
      if ($articleNumber != "None") {
        $article = $articles[$articleNumber];
      }
      ?>
      <form method="post" action="meeting_edit.php">
        <input type="hidden" name="messageNumber" value="<?php print($message->getMessageNumber()); ?>" />
        <table>
	  <tr>
	    <td>
	      <div class="paperBoxTitle" style="margin: 0;">
	        <div class="floatRight"><em><?php print(gmdate("D, j M Y h:i a", $message->getDate())); ?></em></div>
		<?php if($articleNumber != "None") { ?>
   	          <b><?php print('Article ' . $message->getArticleNumber()); ?>: </b><?php if (!is_null($article)) Tools::printHTMLsubstr($article->getTitle(), 30); else print('<em>Article withdrawn</em>');?>
		<?php } else { ?>
                  <br />
                <?php } ?>
	      </div>
	    </td>
	  </tr>
	  <tr>
	    <td><textarea name="content" cols="80" rows="5"><?php Tools::printHTML($message->getContent()); ?></textarea></td>
	  </tr>
	  <tr>
	    <td style="text-align: right;">
	      <?php if(($articleNumber == "None") || (!is_null($article))) { ?><input name="update" type="submit" class="buttonLink" value="Restore" /> <?php } ?>
	      <input name="delete" type="submit" class="buttonLink" value="Delete" />
	    </td>
	  </tr>
        </table>
      </form>
      <br />
    <?php } ?>
  </center>
  <?php 
}

?>




<?php 
} 
?>
<?php include('footer.php'); ?>
