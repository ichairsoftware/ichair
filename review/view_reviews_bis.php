<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='View Reviews';
include '../utils/tools.php';
include 'header_reduced.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $articleNumber = $_GET['articleNumber'];
  $article = Article::getByArticleNumber($articleNumber);
  $reviewReader = new ReviewReader();

  $reviewerNumber = Tools::readPost('reviewerNumber');  
  if(preg_match("/^[0-9]+$/",$reviewerNumber)) {
    $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
    if(!is_null($assignement)) {
      $assignement->setReviewStatus(Tools::readPost('newReviewStatus'));
      $article->computeAverage();
    }
  }

  if(!is_null($article)) {

    $article->printDetailsBoxForChairPopUp();

    $articleNumber = $article->getArticleNumber();

    // print personnal comments, if any

    $assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());
    $reviewReader->createFromXMLFile($assignement);

    if ($reviewReader->getPersonalNotes() != ""){
      print('<div class="paperBoxDetails">Personal Notes:');
      print('<div class="versionAbstract">');
      Tools::printHTMLbr($reviewReader->getPersonalNotes());
      print('</div></div>');
    }


    $completedAssignedReviewers = $article->getAssignedReviewersByStatus(Assignement::$COMPLETED);
    $inProgressAssignedReviewers = $article->getAssignedReviewersByStatus(Assignement::$INPROGRESS);
    $notStartedAssignedReviewers = $article->getAssignedReviewersByStatus(Assignement::$VOID);
    $completedAuthorizedReviewers = $article->getAuthorizedReviewersByStatus(Assignement::$COMPLETED);
    $inProgressAuthorizedReviewers = $article->getAuthorizedReviewersByStatus(Assignement::$INPROGRESS);
    
    $total = count($completedAssignedReviewers);
    $total += count($inProgressAssignedReviewers);
    $total += count($notStartedAssignedReviewers);
    $assignedTotal = $total;
    $total += count($completedAuthorizedReviewers);
    $total += count($inProgressAuthorizedReviewers);
    $authorizedTotal = $total - $assignedTotal;

    if ($total == 0) {
      print('<div class="OKmessage">No review for article ' . $articleNumber . ', sorry!</div>');
    } else {

      if($assignedTotal != 0) {

	print('<h2>Reviewers Assigned for Article '.$articleNumber.'</h2>');
	
	foreach($completedAssignedReviewers as $reviewerNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $reviewReader->createFromXMLFile($assignement);
	  $reviewReader->printDetailsBoxForChairBis();
	}

	foreach($inProgressAssignedReviewers as $reviewerNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $reviewReader->createFromXMLFile($assignement);
	  $reviewReader->printDetailsBoxForChairBis();
	}

	foreach($notStartedAssignedReviewers as $reviewerNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $reviewReader->createFromXMLFile($assignement);
	  $reviewReader->printDetailsBoxForChairBis();
	}

      }

      if($authorizedTotal != 0) {

	print('<h2>Other Reviewers for Article ' .$articleNumber . '</h2>');
	
	foreach($completedAuthorizedReviewers as $reviewerNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $reviewReader->createFromXMLFile($assignement);
	  $reviewReader->printDetailsBoxForChairBis();
	}

	foreach($inProgressAuthorizedReviewers as $reviewerNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $reviewReader->createFromXMLFile($assignement);
	  $reviewReader->printDetailsBoxForChairBis();
	}

      }


    }
    
  
  ?>

  <center>
    <input type="submit" class="buttonLink bigButton" value="Close this Window" onclick="javascript:window.close()" />
  </center>

<?php }} ?>

<?php include('footer.php'); ?>
