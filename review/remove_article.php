<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Remove Article from Database';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {
  $articleNumber = Tools::readPost('articleNumber');
  $article = Article::getByArticleNumber($articleNumber);
  if (!is_null($article)) {
    $article->printDetailsBoxForChair();
    ?>
      <div class="ERRmessage">
	 You are about to remove the article above from the database.
	 This will also delete all assignments you had made for it. Are
	 you sure you want to continue?
      </div>
<center>
<table>
<tr>
  <td>
    <form action="transfer.php" method="post">
      <input type="submit" class="buttonLink bigButton" value="Go Back" />
    </form>
  </td>
  <td>
    <form action="remove_article_result.php" method="post">
      <input type="hidden" name="articleNumber" value="<?php print($articleNumber);?>" />
      <input type="submit" class="buttonLink bigButton" value="Remove Article <?php print($articleNumber);?> from the Database" />
    </form>
  </td>
</tr>
</table>
</center>

<?php 
  }



}
include('footer.php'); ?>
