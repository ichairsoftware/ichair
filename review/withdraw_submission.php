<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Withdraw Submission';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

$submission = Submission::getBySubmissionNumber(Tools::readPost('submissionNumber'));

if(!is_null($submission)) {
  print("<div class=\"ERRmessage\">Are you sure you want to Withdraw the submission number " 
	. $submission->getSubmissionNumber() .
	"? Note that this will not affect any article you might have already transfered.</div>");
  ?>
  <center>
  <table>
  <tr>
  <td>
  <form action="examine.php#<?php print($submission->getSubmissionNumber()); ?>" method="post">
    <input type="submit" class="buttonLink bigButton" value="Go Back" />
  </form>
  </td>
  <td>
  <form action="withdraw_submission_result.php" method="post">
    <input name="submissionNumber" type="hidden" value="<?php Tools::printHTML(Tools::readPost('submissionNumber')); ?>" />
    <input type="submit" class="buttonLink bigButton" value="Withdraw Submission" />
  </form>
  </td>
  </tr>
  </table>
  </center>
  <?php 

}}
?>
</body>
</html>

