<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Configuration';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  /***********************************************************/
  /* Update the config.xml file if something has been posted */
  /***********************************************************/
  
  if(count($_POST) != 0) {

    $phaseValue = Tools::UTF8readPost('phase');
    $restrictedChairMenuValue = Tools::UTF8readPost('restrictedChairMenu');
    $hideTimeValue = Tools::UTF8readPost('hideTime');
    $committeeMeetingValue = Tools::UTF8readPost('committeeMeeting');

    $newConfig = new DOMDocument();
    $newConfig->formatOutput = true;
    $newConfig->encoding = "iso-8859-1";
    
    $xml = $newConfig->createElement('xml'); 
    $xml = $newConfig->appendChild($xml);
    
    /* phase selection */

    $phase = $newConfig->createElement('phase');
    $phase = $xml->appendChild($phase);

    /* current phase value */

    $current = $newConfig->createElement('current');
    $current = $phase->appendChild($current);
    
    $value = $newConfig->createTextNode($phaseValue);
    $current->appendChild($value);

    /* Hide Review Deadline time box */
    
    $hideTime = $newConfig->createElement('hideTime');
    $hideTime = $phase->appendChild($hideTime);
    
    $value = $newConfig->createTextNode($hideTimeValue);
    $hideTime->appendChild($value);

    /* restrictedChairMenu */

    $restrictedChairMenu = $newConfig->createElement('restrictedChairMenu');
    $restrictedChairMenu = $xml->appendChild($restrictedChairMenu);
    
    $value = $newConfig->createTextNode($restrictedChairMenuValue);
    $restrictedChairMenu->appendChild($value);
        
    $configFileName = Tools::getConfig('server/reviewsPath') . "reviews_config.xml";
    $backupConfigFileName = Tools::getConfig('server/reviewsPath') . "reviews_config.xml.bak";
    rename($configFileName, $backupConfigFileName);
    if($newConfig->save($configFileName)) {
      print('<div class="OKmessage">The configuration file was updated successfully.</div>');
    } else {
      rename($backupConfigFileName, $configFileName);
      print("<div class=\"ERRmessage\">There was an error writing the configuration file.</div>");
    }
  } 

  ?>
  <div class="floatRight">
  <form action="index_chair.php" method="post">
    <input type="submit" class="buttonLink bigButton" value="Ok" />
  </form>
  </div>
  <?php 


}

?>
</body>
</html>

