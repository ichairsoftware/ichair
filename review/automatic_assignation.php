<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Automatic Assignation';
include '../utils/tools.php';
include 'header.php';
set_time_limit(0);
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {
?>


<div class="infoMessage">
  Choose one of the buttons below to automatically generate a new temporary assignment table. 
  You can choose to do it <em>from scratch</em> (all previous assignments will be ignored) 
  or as an <em>update</em> (keeping all previous assignments untouched). Depending on the 
  number of submissions/committee members, this operation can take up to a few minutes.
</div>
<center>
<table>
  <tr>
    <td>
    <form action="automatic_assignation.php" method="post">
      <input type="hidden" name="action" value="generate" />
      <input type="submit" class="buttonLink bigButton" value="Generate From Scratch"/>
    </form>
    </td>
    <td>
    <form action="automatic_assignation.php" method="post">
      <input type="hidden" name="action" value="update" />
      <input type="submit" class="buttonLink bigButton" value="Generate an Update"/>
    </form>
    </td>
  </tr>
</table>    
</center>

<?php 

$action = Tools::readPost('action');
if(($action == "generate") || ($action == "update")) {
  $status = "";
  if($action == "generate") {
    $status = Assignement::generateSuggestionAssignationTable(false);
  } else {
    $status = Assignement::generateSuggestionAssignationTable(true);
  }
  Log::logAutomaticSuggestion($currentReviewer);
  print('<div class="OKmessage">The temporary assignment table was generated successfully. This suggestion will only be taken into account once you press the <em>Accept this Suggestion</em> button at the bottom of this page.</div>');
  if($status != "") {
    print('<div class="ERRmessage">');
    Tools::printHTMLbr($status);
    print('</div>');
  }  
}
if($action == "delete") {
  unlink(Tools::getConfig('server/reviewsPath') . "tmp_assignements.db");
  print('<div class="OKmessage">The temporary assignment table was deleted successfully.</div>');
  Log::logAutomaticDeletion($currentReviewer);
}

$allcategories;
if (Tools::useCategory()) {
  $allcategories = Tools::getAllCategories();
}

$path = Tools::getConfig('server/reviewsPath');
$dbFile = $path . 'tmp_assignements.db';
if(file_exists($dbFile)) {
  $db = new SQLite3($dbFile);
  print('<h2>Suggested Assignments</h2>');
  $reviewers = Reviewer::getAllActiveReviewers();
  foreach($reviewers as $reviewer) {
    $fullName = $reviewer->getFullName();
    $reviewerNumber = $reviewer->getReviewerNumber();
    ?>
    <div class="paperBox">
      <div class="paperBoxTitle">
        <div class="paperBoxNumber"><?php print($fullName); ?></div>
        <?php $reviewer->printInfo(); ?>
      </div>
      <div class="paperBoxDetails">
      <?php 
      $count = $db->querySingle('SELECT count(articleNumber) FROM assignements WHERE reviewerNumber="'. $reviewerNumber .'" AND assigned ="true"');
      $result = $db->query('SELECT articleNumber FROM assignements WHERE reviewerNumber="'. $reviewerNumber .'" AND assigned ="true"');
      ?>
      <div class="floatRightClear">
        <div class="goodRatio">
          <?php print($count); ?>
        </div>
      </div>
      <table class="articlesListTable">
      <tr>
        <td>Number and Title</td>
        <td>Authors</td>
      <?php if (Tools::useCategory()) {?>
        <td>Category</td>
      <?php } ?>
      </tr>
      <?php 
      while($dbrow = $result->fetchArray()) {
	$article = Article::getByArticleNumber($dbrow['articleNumber']);
	if (!is_null($article)) {
	?>
	<tr>
	  <td><?php print($article->getArticleNumber() . ": " . Tools::HTMLsubstr($article->getTitle(),40)); ?></td>
	  <td><?php print(Tools::HTMLsubstr($article->getAuthors(),40)); ?></td>
        <?php if (Tools::useCategory()) {?>
	  <td><?php Tools::printHTML($allcategories[$article->getCategory()]); ?></td>
        <?php } ?>
	</tr>
	<?php 
	}
      }
      ?>
      </table>
      <div class="clear"></div>
      </div>      
    </div>
    <?php 
  }
  ?>
  <center>
    <table>
      <tr>
        <td>
          <form action="automatic_assignation.php" method="post">
            <input type="hidden" name="action" value="delete" />
            <input type="submit" class="buttonLink bigButton" value="Delete this Suggestion"/>
          </form>
        </td>
        <td>
          <form action="automatic_assignation_confirm.php" method="post">
            <input type="submit" class="buttonLink bigButton" value="Accept this Suggestion"/>
          </form>
	</td>
      </tr>
    </table>
  </center>    
  				     
  <?php 
}
 






   
?>















 <?php } ?>

<?php include('footer.php'); ?>
