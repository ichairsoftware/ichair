<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Main Page';
include '../utils/tools.php';
include 'header.php';
if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {
?>

<p>Welcome to the iChair Submission &amp; Review Software! This page is the
main page for the Conference Chair.</p>


<?php 

if (Tools::getCurrentPhase() == Tools::$SUBMISSION) {

  Submission::printSummaryTable();

} else if ((Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) {

  Article::printAcceptanceSummaryTable();

}

?>


<h2><a href="mailing.php">Mass Mailing</a></h2>

<p>Use <a href="mailing.php">Mass Mailing</a> to send emails to all the members of the program committee (e.g., to tell the program committee members that they can chose their preferences) or to the contact authors (e.g., to tell the authors that their paper has been accepted to the conference).</p>

<h2><a href="meeting_edit.php">Messages to Committee</a></h2>
<p>The Program Chair has the possibility to edit some messages for the committee members. These messages can either be of general order or specific to a particular article. If they concern a specific article, they will also be displayed in the <em>Reviews &amp; Discussion</em> page concerning this article.</p>

<h2><a href="configuration.php">Select iChair Phase</a></h2>

<p>iChair can be either in the
<em>Submission/Transfer/Authorizations</em> phase, <em>Preferred
Articles Election</em> phase, <em>Assignations</em> phase,
<em>Review/Discussion/Decision</em> phase, or in the <em>Decision
only</em> phase. Currently, iChair is in the
<?php 
if (Tools::getCurrentPhase() == Tools::$SUBMISSION) {
  print('<em>Submission/Transfer/Authorizations</em>');
} else if (Tools::getCurrentPhase() == Tools::$ELECTION) {
  print('<em>Preferred Articles Election</em>');
} else if (Tools::getCurrentPhase() == Tools::$ASSIGNATION) {
  print('<em>Assignations</em>');
} else if (Tools::getCurrentPhase() == Tools::$REVIEW) {
  print('<em>Review/Discussion/Decision</em>');
} else {
  print('<em>Decision only</em>');
}
?>
phase. The current phase can be selected from the <a href="configuration.php">Select iChair Phase</a> menu.</p>

<h2><a href="export_list.php">Export Article List</a></h2>

From the <a href="export_list.php">Export Article List</a> page, one can download a list of all the submissions/articles in text format. Several fields can be included/excluded and the list can be sorted in many different ways.

<h2><a href="stats.php">Conference Statistics</a></h2>

The <a href="stats.php">Conference Statistics</a> page gives access to
several statistics concerning the submissions, the reviewers, and the
subreviewers. Some of the statistics depend on the current iChair
phase, like for example the time&amp;date at which accepted articles
have been submitted which is only available during the
<em>Review/Discussion/Decision</em> or the <em>Decision only</em>
phases.

<?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$SUBMISSION)) { ?>

<h2><a href="examine.php">Current Submissions</a></h2>

<p>Use <a href="examine.php">Current Submissions</a> to have a look at all
the submissions made to the conference. From there, you can
withdraw/unwithdraw the submission, view the details of the different
versions (from different updates made by the authors) of a submissions, and
download the corresponding files. For each version, you can add your own
comments. You may find this feature useful during the submission and
assignement procedures. For instance, you can notice potential conflicts of
interest here.</p>

<h2><a href="transfer.php">Load Submissions for Review</a></h2>

<p>When the submission process is over, the review process can
start. For this to happen, you need to migrate the database of all the
submissions made to the conference to a database of articles. Use <a
href="transfer.php">Load Submissions for Review</a> to select the
submissions that you want to transfer to the article database so that
they can be reviewed. Tipically, this should be done in
<em>Submission/Transfer/Authorizations</em> phase.</p>

<p>Note that it is still possible to add new submissions even if an
article database exists, and migrate them one by one even if the review
process has already started. This may be useful when a new version of
a submission is available, when a previous version was already
migrated to the article database. In such a case, the new version will
overwrite the old one.</p>

<p>Also note that withdrawn submission
cannot be transfered to the article database.</p>

<?php } ?>
<?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() != Tools::$DECISION)) { ?>


<h2><a href="assign_reviewers_list.php">Assign Reviewers to Articles</a></h2>

<p>
Use <a href="assign_reviewers_list.php">Assign Reviewers to Articles</a> to get a list of
all the articles in the article database. For each article, you can</p>
<ul>
  <li>
  assign a reviewer, meaning that this reviewer will have
  to review the article,
  </li>
  <li>
  authorize a reviewer, meaning that this reviewer will
  have access to the article (just as if this article was assigned to
  him) but won't have to review it,
  </li>
  <li>
  block a reviewer, if you do not want this reviewer to have access
  to the article.
  </li>  
</ul>
<p>
A counter displays the current number of assigned reviewers for each
article, together with the number of reviewers that <em>should</em> be
assigned for this article. If the number of assigned reviewers is
lower than the expected number of reviewers, the ratio is displayed in
red. You can change the number of reviewers needed for a particular
article by using the arrows on the right of the ratio. For example,
articles written by a member of the program committee might need more
reviewers. Note that the default number of reviewers expected for each
article can be changed by the administrator of the iChair software.
</p>

<p>
Until <a href="assign_reviewers_list.php">Assign Reviewers to
Articles</a> is used for the first time, all the reviewers are authorized
for all the articles.</p>

<p>The best thing to do the first time you use <a
href="assign_reviewers_list.php">Assign Reviewers to Articles</a> is
to click on <em>Assign Reviewers</em> for the first article, possibly
block some specific reviewers, click on <em>Save Changes and Go to
Next Article</em>. Iterate until the reviewers have suitable
permissions for the <em>last</em> article of the conference. This will
allow each reviewer to take a first look at the list of articles which
are available to her/him and to give an <em>ability/willingness to
review grade</em> to each article (this is done through the <a
href="affinity_list.php">Preferred Articles</a> page). This grade
shall help the reviewer when choosing whom a specific article should
be assign to. Once the reviewers have given their grades, the chair
should use <a href="assign_reviewers_list.php">Assign Reviewers to
Articles</a> once again to actually <em>assign articles</em> to
reviewers.  </p>

<h2><a href="assign_articles_list.php">Assign Articles to Reviewers</a></h2>

<p>Use <a href="assign_articles_list.php">Assign Articles to Reviewers</a>
to get a kind of transpose of <a href="assign_reviewers_list.php">Assign
Reviewers to Articles</a>. In this case, you get a list of all the reviewers
(an observers), and for each one, you can can choose to
assign/authorized/block articles by clicking on the <em>Assign Articles</em>
button. For each reviewer, a counter displays the number of articles already
assigned to this reviewer, which shall help the chair to be <em>fair</em>
during the assignment process (here, it means that each reviewer should have
the same number of assigned reviews). </p>

<p>At the bottom of the page, you will find a list of observers. You
cannot assign articles to observer, but only authorize or block
articles to them.
</p>

<?php } ?>
<?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$ASSIGNATION)) { ?>

<h2><a href="automatic_assignation.php">Automatic Assignment</a></h2>

<p>Once the submissions have been transfered and the reviewers have
chosen their prefered articles, you can use the <a
href="automatic_assignation.php">Automatic Assignment</a> page to
<em>automatically</em> assign articles to each of them. You have the
possibility to modify the proposition made by iChair and keep any
existing assignement as a basis for the automated process (meaning
that you won't loose all you have done until now). Note also that the
algorithm is not deterministic: running it twice won't give the same
result.</p>

<?php } ?>
<?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$REVIEW)) { ?>

<h2><a href="supervise_reviewers.php">Reviewers Access Privileges</a></h2>

<p>Committe members can only see reviews from other members if the
chair allows them to do so. This has to be done through the <a
href="supervise_reviewers.php">Reviewers Access Privileges</a>
page. At the same time, this will grant the selected reviewers access
to the discussions.</p>

<?php } ?>
<?php if((!Tools::restrictChairMenu()) || (Tools::getCurrentPhase() == Tools::$REVIEW) || (Tools::getCurrentPhase() == Tools::$DECISION)) { ?>

<h2><a href="accept_and_reject.php">Accept &amp; Reject Articles</a></h2>

<p>Use the <a href="accept_and_reject.php">Accept &amp; Reject
Articles</a> page to set, for each article, whether it should be
<em>accepted</em>, <em>rejected</em>, <em>discussed</em>...</p>

<?php } ?>






<?php }?>

<?php include('footer.php'); ?>
