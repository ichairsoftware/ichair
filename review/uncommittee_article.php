<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Committee Member Article';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

$article = Article::getByArticleNumber(Tools::readPost('articleNumber'));

if(!is_null($article)) {
  $article->setNotCommittee();
  print("<div class=\"OKmessage\">Article number "
	. $article->getArticleNumber() .
	" was successfully marked as NOT involving any member of the program committee.</div>");
  ?>
  <form action="assign_reviewers_list.php#nbr<?php print($article->getArticleNumber()); ?>" method="post">
    <div class="floatRight">
    <input type="hidden" name="articleNumber" value="<?php print($article->getArticleNumber()); ?>" />
    <input type="submit" class="buttonLink bigButton" value="Ok" />
    </div>
  </form>
  <?php 

}}
?>
</body>
</html>

