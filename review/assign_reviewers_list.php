<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Assign Reviewers to Articles';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  /* Update the number of articles displayed for the current reviewer */

  $numberOfArticlesPerPage = Tools::readPost('numberOfArticlesPerPage');
  if(preg_match("/^[1-9][0-9]*$/", $numberOfArticlesPerPage)) {
    $currentReviewer->setNumberOfArticlesPerPage($numberOfArticlesPerPage);
  }

  /* If there is something in the POST, it means that the number of reviewer has changed for some article */

  $articleNumber = Tools::readPost('articleNumber');
  if($articleNumber != "") {
    $article = Article::getByArticleNumber($articleNumber);
    if(!is_null($article)) {
      if(Tools::readPost('increment') == "+") {
	$article->setNumberOfReviewers($article->getNumberOfReviewers() + 1);
      } else if(Tools::readPost('increment') == "-") {
	$article->setNumberOfReviewers(max($article->getNumberOfReviewers() - 1, 0));
      }
    }
  } else {
    $articleNumber = 1;
  }

  /* Get all articles */
  $articles = Article::getAllArticles();

  /* If the list is empty, print a message */

  if(count($articles) == 0) {
    print('<div class="OKmessage">There is no article to assign yet, you may wish to go ' .
	  'to the <a href="transfer.php">Load Submissions for Review</a> section to create a ' .
	  'first list of articles to review or to the ' . 
	  '<a href="index_chair.php">Chair Main Page</a> for more instructions.</div>');
  } else {

  /* Display a form allowing to change the number of article displayed on the same page */
  ?>
    <div class="floatRight">
      <form action="assign_reviewers_list.php" method="post">
       <input type="hidden" name="articleNumber" value="<?php print($articleNumber); ?>" />
       <table><tr>
        <td><div class="versionAuthors">Number of articles displayed per page:&nbsp;</div></td>
        <td><input type="text" name="numberOfArticlesPerPage" size="3" class="largeInput" value="<?php print($currentReviewer->getNumberOfArticlesPerPage()); ?>" /></td>
	<td><input type="submit" class="buttonLink" value="Ok" /></td>
       </tr></table>
      </form>
    </div>
    <div class="clear"></div>
  <?php 

  /* Get all articles */
  $rangeOfArticles = Article::getRangeOfArticles($articleNumber, $currentReviewer->getNumberOfArticlesPerPage());
  reset($rangeOfArticles);
  $firstArticleOfThePage = key($rangeOfArticles); 

  /* Display a list of all articles with a list of buttons allowing to go from one page to another */

  /* Display the buttons */
  
  reset($articles);
  for($btNbr=0; $btNbr<ceil(count($articles) / $currentReviewer->getNumberOfArticlesPerPage())-1; $btNbr++) {
    /* Display a form for button number $btNbr */
    $startNbr = key($articles);
    for($i=0; $i<$currentReviewer->getNumberOfArticlesPerPage()-1; $i++) {
      next($articles);
    }
    $stopNbr = key($articles);
    next($articles);
    print('<div class="floatLeft">');
    print('<form action="assign_reviewers_list.php" method="post">');
    print('<input type="hidden" name="articleNumber" value="'.$startNbr.'" />');
    if($startNbr != $stopNbr) { 
      if($firstArticleOfThePage == $startNbr) {
	print('<input type="submit" class="buttonLink currentButton" value="'.$startNbr . ' - ' . $stopNbr.'" />');
      } else {
	print('<input type="submit" class="buttonLink" value="'.$startNbr . ' - ' . $stopNbr.'" />');
      }
    } else { 
      if($firstArticleOfThePage == $startNbr) {
	print('<input type="submit" class="buttonLink currentButton" value="'.$startNbr.'" />');
      } else {
	print('<input type="submit" class="buttonLink" value="'.$startNbr.'" />');
      }
    }
    print('</form></div>');
  }
  $startNbr = key($articles);
  end($articles);
  $stopNbr = key($articles);
  print('<div class="floatLeft">');
  print('<form action="assign_reviewers_list.php" method="post">');
  print('<input type="hidden" name="articleNumber" value="'.$startNbr.'" />');
  if($startNbr != $stopNbr) { 
    if($firstArticleOfThePage == $startNbr) {
      print('<input type="submit" class="buttonLink currentButton" value="'.$startNbr . ' - ' . $stopNbr.'" />');
    } else {
      print('<input type="submit" class="buttonLink" value="'.$startNbr . ' - ' . $stopNbr.'" />');
    }
  } else { 
    if($firstArticleOfThePage == $startNbr) {
      print('<input type="submit" class="buttonLink currentButton" value="'.$startNbr.'" />');
    } else {
      print('<input type="submit" class="buttonLink" value="'.$startNbr.'" />');
    }
  }
  print('</form></div>');
  print('<div class="clear"></div>');
  
  /* Display the list of articles */
  
  foreach($rangeOfArticles as $article) {
    $articleNumber = $article->getArticleNumber();
    print('<div class="paperBox" id="nbr' . $articleNumber . '"><div class="paperBoxTitle">');
    print('<div class="paperBoxNumber">Article&nbsp;' . $articleNumber . '</div>');    

    $article->printDetailsBoxTitle();
    print("\n</div>\n");
    if ($article->getIsCommitteeMember()){
      print('<div class="paperBoxDetailsCommittee">');
    } else {
      print('<div class="paperBoxDetails">');
    }

    if (Tools::useCountry()) {
      Tools::printContinentDivs($article->getCountryArray());
    } else {
      print('<div><div><div>');
    }
    $article->printAssignedRatio();
    $article->printCustomCheckboxes(true);

    print("<div class=\"versionTitle\">Title: ". htmlentities($article->getTitle(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') ."</div>\n");
    print("<div class=\"versionAuthors\">Authors: ".htmlentities($article->getAuthors(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')."</div>\n");
    if(Tools::useAffiliations()) {
      print("Affiliations: " . htmlentities($article->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') ."<br />\n");
    }
    if(Tools::useCountry()) {
      print("Countries: " . htmlentities($article->getCountry(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') ."<br />\n");
    }

    $article->printAssignedReviewers();
    $article->printBlockedReviewers();
    $chairComments = $article->getChairComments();
    if ($chairComments != "None.") {
      print('Chair comments:<br />');
      print("<div class=\"chairComments\">\n");
      Tools::printHTMLbr($chairComments);
      print("\n</div>\n");
    }
    if ($article->getAcceptance() == Article::$ACCEPT) {
      print('Status: <span class="acceptArticle">Accept</span>');
    } else if ($article->getAcceptance() == Article::$MAYBE_ACCEPT) {
      print('Status: <span class="maybeAcceptArticle">Maybe accept</span>');
    } else if ($article->getAcceptance() == Article::$DISCUSS) {
      print('Status: <span class="discussArticle">Discuss</span>');
    } else if ($article->getAcceptance() == Article::$MAYBE_REJECT) {
      print('Status: <span class="maybeRejectArticle">Maybe reject</span>');
    } else if ($article->getAcceptance() == Article::$REJECT) {
      print('Status: <span class="rejectArticle">Reject</span>');
    } else if ($article->getAcceptance() == Article::$CUSTOM1) {
      print('Status: <span class="customAccept1">' . htmlentities(Tools::getCustomAccept1(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</span>');
    } else if ($article->getAcceptance() == Article::$CUSTOM2) {
      print('Status: <span class="customAccept2">' . htmlentities(Tools::getCustomAccept2(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</span>');
    } else if ($article->getAcceptance() == Article::$CUSTOM3) {
      print('Status: <span class="customAccept3">' . htmlentities(Tools::getCustomAccept3(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</span>');
    } else if ($article->getAcceptance() == Article::$CUSTOM4) {
      print('Status: <span class="customAccept4">' . htmlentities(Tools::getCustomAccept4(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</span>');
    } else if ($article->getAcceptance() == Article::$CUSTOM5) {
      print('Status: <span class="customAccept5">' . htmlentities(Tools::getCustomAccept5(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</span>');
    } else if ($article->getAcceptance() == Article::$CUSTOM6) {
      print('Status: <span class="customAccept6">' . htmlentities(Tools::getCustomAccept6(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</span>');
    } else if ($article->getAcceptance() == Article::$CUSTOM7) {
      print('Status: <span class="customAccept7">' . htmlentities(Tools::getCustomAccept7(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</span>');
    }

    print('</div></div></div></div>');
  ?>
    <div class="floatRight">
      <form action="assign_reviewers.php" method="post">
        <input type="hidden" name="articleNumber" value="<?php print($articleNumber); ?>" />
        <input type="submit" class="buttonLink" value="Assign Reviewers" />
      </form>
    </div>
<?php 
    if ($article->getIsCommitteeMember()) {
      $article->printNotCommitteeLink();
    } else {
      $article->printCommitteeLink();
    }
    print('<div class="clear"></div></div>');
  }


  /* Display the buttons */
  
  reset($articles);
  for($btNbr=0; $btNbr<ceil(count($articles) / $currentReviewer->getNumberOfArticlesPerPage())-1; $btNbr++) {
    /* Display a form for button number $btNbr */
    $startNbr = key($articles);
    for($i=0; $i<$currentReviewer->getNumberOfArticlesPerPage()-1; $i++) {
      next($articles);
    }
    $stopNbr = key($articles);
    next($articles);
    ?>
    <div class="floatLeft">
    <form action="assign_reviewers_list.php" method="post">
      <input type="hidden" name="articleNumber" value="<?php print($startNbr); ?>" />
      <?php if($startNbr != $stopNbr) { ?>
        <input type="submit" class="buttonLink" value="<?php print($startNbr . ' - ' . $stopNbr); ?>">
      <?php } else { ?>
        <input type="submit" class="buttonLink" value="<?php print($startNbr); ?>">
      <?php } ?>
    </form>
    </div>
    <?php 
  }
  $startNbr = key($articles);
  end($articles);
  $stopNbr = key($articles);
  ?> 
  <div class="floatLeft">
  <form action="assign_reviewers_list.php" method="post">
    <input type="hidden" name="articleNumber" value="<?php print($startNbr); ?>" />
    <?php if($startNbr != $stopNbr) { ?>
      <input type="submit" class="buttonLink" value="<?php print($startNbr . ' - ' . $stopNbr); ?>">
    <?php } else { ?>
      <input type="submit" class="buttonLink" value="<?php print($startNbr); ?>">
    <?php } ?>
  </form>
  </div>
  <div class="clear"></div>

<?php }} ?>

<?php include('footer.php'); ?>
