<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include '../utils/tools.php';
$reviewerNumber = Tools::readPost('reviewerNumber');
/* Do not print the reviewer name here, as we cannot test if the currentReviewerGroup is CHAIR_GROUP or not */
$page_title='Assign Articles to a Reviewer';

include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $allcategories;
  if(Tools::useCategory()) {
    $allcategories = Tools::getAllCategories();
  }

  $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
  if (!$reviewer) {
    return;
  }
  $nextReviewerNumber = $reviewer->getNextActiveReviewer();
  $reviewerToUpdate = Reviewer::getByReviewerNumber(Tools::readPost('reviewerToUpdate'));

  $reviewerWasUpdated = false;
  if(!is_null($reviewerToUpdate)) {
    $assignements = Assignement::getAssignementsByReviewerNumber($reviewerToUpdate->getReviewerNumber());
    foreach($assignements as $assignement) {
      $newAssignementStatus = Tools::readPost("" . $assignement->getArticleNumber());
      if(Assignement::isValidAssignementStatus($newAssignementStatus) && ($newAssignementStatus != $assignement->getAssignementStatus())) {
	$assignement->setAssignementStatus($newAssignementStatus);
	$reviewerWasUpdated = true;
      }
    }
    if($reviewerWasUpdated) {
      Log::logAssignementReviewer($reviewerToUpdate, $currentReviewer);
      ?>
      <div class="OKmessage">Permissions for Reviewer <?php Tools::printHTML($reviewerToUpdate->getFullName()); ?> have been saved successfully.</div>
      <?php 
    }

  } 

  /* If we just updated the last reviewer... */

  if ((!is_null($reviewerToUpdate)) && (Tools::readPost('buttonName') == "Save Changes for this Last Reviewer")) {
    ?>
    <div class="OKmessage">This was the last reviewer. You can go back to the list.</div>
    <div class="floatRight">
       <form action="assign_articles_list.php" method="post">
         <input type="submit" class="buttonLink bigButton" value="Go Back to the List"/>
       </form>
    </div>
    <?php 
  } else {
    /* Display a white box with all the informations about the reviewer to whom the chair is assigning papers */
    ?>

    <div class="paperBox">
      <div class="paperBoxDetails">
        <?php 
        $reviewer->printLong(); 
        /* Print the preferred categories of each reviewer */
	if(Tools::useCategory()) {
	  print('Preferred Categories: ');
	  $preferredCat = '';
          if($reviewer->getPreferedCategory() != '') {
            foreach(explode(";",$reviewer->getPreferedCategory()) as $catid) {
              $preferredCat .= '; ' . $allcategories[$catid];
            }
          }
	  if($preferredCat != '') {
	    print(substr($preferredCat, 2, strlen($preferredCat)));
	  } else {
	    print('<em>none</em>');
	  }
	}
        ?>
      </div>
    </div>


    <?php /* Displaying information */ ?>

    <center>

      <form action="assign_articles_list.php#nbr<?php print($reviewerNumber);?>" method="post">
        <input type="submit" class="buttonLink bigButton" value="Go Back to the List" />
      </form>

      <form action="assign_articles.php" method="post">
        <input type="hidden" name="reviewerToUpdate" value="<?php print($reviewerNumber);?>" />

	<?php 
        $assignedArticles = $reviewer->getArticlesByStatus(ASSIGNEMENT::$ASSIGNED);
        $authorizedArticles = $reviewer->getArticlesByStatus(ASSIGNEMENT::$AUTHORIZED);
	$blockedArticles = $reviewer->getArticlesByStatus(ASSIGNEMENT::$BLOCKED);
        ?>

	<table class="usersTable">
	  <tr>
	    <th colspan="4">&nbsp;</th>
	    <th colspan="3">Status</th>
	    <th>&nbsp;</th>
	  </tr>
          <tr>
	    <th>Reviewer<br/>Choice</th>
	    <th>Article<br/>Number</th>
	    <th>&nbsp;</th>
	    <th class="leftAlign">
	      <table>
	        <tr><th class="leftAlign">Title</th></tr>
	        <tr><th class="leftAlign"><em>Authors</em></th></tr>
	      </table>
	    </th>
            <th><div class="bigNumber completedReview"><?php 
		print(count($assignedArticles));
	        if ($reviewer->getNumberOfArticles() != "") {
		  print('/'.$reviewer->getNumberOfArticles());
		}?></div></th>
            <th><div class="bigNumber inProgressReview"><?php print(count($authorizedArticles)); ?></div></th>
            <th><div class="bigNumber voidReview"><?php print(count($blockedArticles)); ?></div></th>
	    <th>Assignment<br/>Ratio</th>
          </tr>
          <script>
          function checkAll(theBox, rExp){
            var elem = theBox.form.elements;
            for(var i=0;i<elem.length;i++){
              if(elem[i].type=="radio" && elem[i].id.match(rExp)){
                 elem[i].checked = true;
              }
            }
          }
          </script>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><label class="buttonLink" onClick='javascipt:checkAll(this,"assign");' />Assign all</label></td>
            <td><label class="buttonLink" onClick='javascipt:checkAll(this,"authorize");' />Authorize all</label></td>
            <td><label class="buttonLink" onClick='javascipt:checkAll(this,"block");' />Block all</label></td>
            <td><label class="buttonLink" onClick='javascipt:checkAll(this,"default");' />Reset all</label></td>
          </tr>

	  <?php 
	  foreach($assignedArticles as $articleNumber) {
	    $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	    $article = Article::getByArticleNumber($articleNumber);
	    print('<tr>');
	    switch ($assignement->getAffinity()){
	    case 2:
	      print('<td><div class="completedReview">Willing' ."</div></td>\n");
	      break;
	    case 1:
	      print('<td><div class="inProgressReview">Neutral' . "</div></td>\n");
	      break;
	    default:
	      print('<td><div class="voidReview">Not able' . "</div></td>\n");
	    }
	      ?>
	    <td><div class="bigNumber"><?php print($article->getArticleNumber());?></td>
	    <td class="button">
	    <div class="popUp">
	      <a href="article_details_chair.php?articleNumber=<?php print($articleNumber); ?>" target="_blank">Details</a>
	      <div class="hidden">
	        <?php $article->printDetailsBoxForChairPopUp();?>
	      </div>
	    </div>
	    </td>
            <td class="leftAlign">
	      <table>
	        <tr><td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),60) ?></td></tr>
	        <tr><td class="leftAlign"><em><?php Tools::printHTMLsubstr("Authors: ".$article->getAuthors(),60) ?></em></td></tr>
	      </table>
	    </td>
            <?php 
	    print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="default_assign' .$articleNumber .'" value="'. Assignement::$ASSIGNED .'" checked="checked" /><label for="assign' .$articleNumber ."\">Assigned</label></td>\n");
	    print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="authorize' . $articleNumber .'" value="'. Assignement::$AUTHORIZED .'" /><label for="authorize' . $articleNumber . "\">Authorized</label></td>\n");
	    print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="block' . $articleNumber .'" value="'. Assignement::$BLOCKED .'" /><label for="block' . $articleNumber . "\">Blocked</label></td>\n");
	    print('<td><div class="bigNumber">'.count($article->getAssignedReviewers()).'/'.$article->getNumberOfReviewers().'</div></td></tr>');
	  }
	  foreach($authorizedArticles as $articleNumber) {
	    $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	    $article = Article::getByArticleNumber($articleNumber);
	    print('<tr>');
	    switch ($assignement->getAffinity()){
	    case 2:
	      print('<td><div class="completedReview">Willing' ."</div></td>\n");
	      break;
	    case 1:
	      print('<td><div class="inProgressReview">Neutral' ."</div></td>\n");
	      break;
	    default:
	      print('<td><div class="voidReview">Not able' ."</div></td>\n");
	    }
	      ?>
            <td><div class="bigNumber"><?php print($article->getArticleNumber());?></td>
	    <td class="button">
	    <div class="popUp">
	      <a href="article_details_chair.php?articleNumber=<?php print($articleNumber); ?>" target="_blank">
	        Details
	      </a>
	      <div class="hidden">
	        <?php $article->printDetailsBoxForChairPopUp(); ?>
	      </div>
	    </div>
	    </td>
            <td class="leftAlign">
	      <table>
	        <tr><td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),60) ?></td></tr>
	        <tr><td class="leftAlign"><?php Tools::printHTMLsubstr("Authors: ".$article->getAuthors(),60) ?></em></td></tr>
	      </table>
	    </td>
	    <?php 
	    if($reviewer->getGroup() != Reviewer::$OBSERVER_GROUP) {
              print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="assign' .$articleNumber .'" value="'. Assignement::$ASSIGNED .'" /><label for="assign' .$articleNumber ."\">Assigned</label></td>\n");
	    } else {
	      print("<td></td>\n");
	    }
            print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="default_authorize' . $articleNumber .'" value="'. Assignement::$AUTHORIZED .'" checked="checked" /><label for="authorize' . $articleNumber . "\">Authorized</label></td>\n");
            print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="block' . $articleNumber .'" value="'. Assignement::$BLOCKED .'" /><label for="block' . $articleNumber . "\">Blocked</label></td>\n");
	    print('<td><div class="bigNumber">'.count($article->getAssignedReviewers()).'/'.$article->getNumberOfReviewers().'</div></td></tr>');
	  }
	  foreach($blockedArticles as $articleNumber) {
	    $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	    $article = Article::getByArticleNumber($articleNumber);
	    print('<tr>');
	    switch ($assignement->getAffinity()){
	    case 2:
	      print('<td><div class="completedReview">Willing' ."</div></td>\n");
	      break;
	    case 1:
	      print('<td><div class="inProgressReview">Neutral' ."</div></td>\n");
	      break;
	    default:
	      print('<td><div class="voidReview">Not able' ."</div></td>\n");
	    }
	      ?>
            <td><div class="bigNumber"><?php print($article->getArticleNumber());?></td>
	    <td class="button">
	    <div class="popUp">
	      <a href="article_details_chair.php?articleNumber=<?php print($articleNumber); ?>" target="_blank">
	        Details
	      </a>
	      <div class="hidden">
	        <?php $article->printDetailsBoxForChairPopUp(); ?>
	      </div>
	    </div>
	    </td>
            <td class="leftAlign">
	      <table>
	        <tr><td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),60) ?></td></tr>
	        <tr><td class="leftAlign"><?php Tools::printHTMLsubstr("Authors: ".$article->getAuthors(),60) ?></em></td></tr>
	      </table>
	    </td>
	    <?php 
	    if($reviewer->getGroup() != Reviewer::$OBSERVER_GROUP) {
              print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="assign' .$articleNumber .'" value="'. Assignement::$ASSIGNED .'" /><label for="assign' .$articleNumber ."\">Assigned</label></td>\n");
	    } else {
	      print("<td></td>\n");
	    }
            print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="authorize' . $articleNumber .'" value="'. Assignement::$AUTHORIZED .'" /><label for="authorize' . $articleNumber . "\">Authorized</label></td>\n");
            print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="default_block' . $articleNumber .'" value="'. Assignement::$BLOCKED .'" checked="checked" /><label for="block' . $articleNumber . "\">Blocked</label></td>\n");
	    print('<td><div class="bigNumber">'.count($article->getAssignedReviewers()).'/'.$article->getNumberOfReviewers().'</div></td></tr>');
	  }
	  ?>
      </table>   
      

	  <?php  if (!is_null($nextReviewerNumber)){  ?>
              <input type="hidden" name="reviewerNumber" value="<?php print($nextReviewerNumber);?>" />
	      <input type="submit" name="buttonName" class="buttonLink bigButton" value="Save Changes and Go To Next Reviewer" />
          <?php } else { ?>
              <input type="hidden" name="reviewerNumber" value="<?php print($reviewerNumber);?>" />
	      <input type="submit" name="buttonName" class="buttonLink bigButton" value="Save Changes for this Last Reviewer" />
	  <?php  }  ?>
    </form>
  </center>
<div class="clear bottomSpacer"></div>

<?php }} ?>

<?php include('footer.php'); ?>
