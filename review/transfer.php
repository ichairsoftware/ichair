<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Load Submissions for Review';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $submissions = Submission::getAllNotWithdrawnSubmissions();
  $transferedArticles = Article::getAllArticles();
  $toBeRemoved = array();
  $allcategories;
  if (Tools::useCategory()) {
    $allcategories = Tools::getAllCategories();
  } 
  
  foreach($transferedArticles as $article) {
    if (!array_key_exists($article->getArticleNumber(), $submissions)){
      $toBeRemoved[$article->getArticleNumber()] = $article;
    }
  }

  if (count($toBeRemoved) != 0) {?>
    <h2>Withdrawn Transfered Articles</h2>

Below is a list of articles which have been withdrawn from the
conference but were already transfered in the database. You can
use the <em>remove</em> button to untransfer them.

  <center>
    <table class="dottedTable">
      <tr>
	<th class="topRow" colspan="2">&nbsp;</th>
        <th class="leftAlign topRow">
	  <table>
	    <tr>
	      <th class="leftAlign topRow">Title</th>
	    </tr><tr>
	      <th class="leftAlign topRow"><em>Authors</em></th>
	    </tr>
	  </table>
        </th>
        <th class="topRow">Transfered<br /> Versions</th>
        <th class="topRow">&nbsp;</th>
      </tr>
     <?php 
     foreach($toBeRemoved as $article) {
       $articleNumber = $article->getArticleNumber();
     ?>
        <tr>
	  <td>
	    <div class="popUp">
	      <a href="article_details_chair.php?articleNumber=<?php print($articleNumber); ?>" target="_blank">Details</a>
	    <div class="hidden">
	      <?php $article->printDetailsBoxForChairPopUp();?>
	    </div>
	  </div>
	</td>
        <td>
	  <div class="bigNumber"><?php print($articleNumber); ?></div>
	</td>
        <td class="leftAlign">
	  <table>
	        <tr><td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),50) ?></td></tr>
	        <tr><td class="leftAlign"><em><?php Tools::printHTMLsubstr("Authors: ".$article->getAuthors(),50) ?></em></td></tr>
	      </table>
	    </td>
	   <td><div class="bigNumber">
	     <?php print($article->getVersionNumber()); ?>
	   </div></td>
	   <td>
	     <form action="remove_article.php" method="post"> 
	       <input type="hidden" name="articleNumber" value="<?php print($articleNumber);?>" />
	       <input type="submit" class="buttonLink" value="Remove from Database" />
	     </form>
	   </td>
	 </tr>

     
<?php   }
    print('</table>');
  }

  if (count($submissions) == 0) {
    
    print('</center><div class="OKmessage">There are no submissions to transfer at the moment.</div>');
  } else {
  ?>
<h2>Submissions Available for Transfer</h2>
<form action="transfer_result.php" method="post">
  <center>
    <table class="dottedTable">
      <tr>
        <th class="topRow">Transfer</th>
	<th class="topRow" colspan="2">&nbsp;</th>
        <th class="leftAlign topRow">
	  <table class="leftAlign">
	    <tr>
	      <th class="leftAlign topRow">Title</th>
	    </tr><tr>
	      <th class="leftAlign topRow"><em>Authors</em></th>
	    </tr>
	  </table>
        </th>
        <th class="topRow">Warnings</th>
        <th class="topRow">Last/Transfered<br /> Versions</th>
      </tr>
      <?php 
     foreach($submissions as $submission) {
       $submissionNumber = $submission->getSubmissionNumber();
       /* Do we have to check the Transfer box ? Yes if no article corresponding to the submission can be found
	* or if there is a more recent version of the file in the submission database */
       if (array_key_exists($submission->getSubmissionNumber(), $transferedArticles)) {
         $transferedArticle = $transferedArticles[$submission->getSubmissionNumber()];
         $doCheck = false;
       } else {
         $doCheck = true;
       }
       $transferedVersion = "<em>None</em>";
       if(!$doCheck) {
	 $transferedVersion = $transferedArticle->getVersionNumber();
	 if($transferedVersion != $submission->getLastVersionNumber()) {
	   $doCheck = true;
	 }
       }
       ?>
         <tr>
	   <td>
	     <input class="noBorder" type="checkbox" name="transfer<?php print($submissionNumber); ?>" value="<?php print($submissionNumber); ?>" <?php if($doCheck) print("checked=\"checked\""); ?> />
	   </td>
	   <td>
	     <div class="popUp">
	       <a href="transfer_view_details.php?submissionNumber=<?php print($submission->getSubmissionNumber()); ?>" target="_blank">
	         Details
	       </a>
	       <div class="hidden">
	       <?php 
	         $version = $submission->getLastVersion();
    
                 print('<div class="paperBox">');
                 if ($submission->getIsCommitteeMember()) {
		   print('<div class="paperBoxDetailsCommittee">');
		 } else {
		   print('<div class="paperBoxDetails">');
		 }
		 if (Tools::useCountry()) {
                   Tools::printContinentDivs($version->getCountryArray());
                 } else {
                  print('<div><div><div>');
                 }
                 $submission->printCustomCheckboxes(true);
                 print('<div class="versionTitle">Title: ' . htmlentities($version->getTitle(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . "</div>\n");
                 print('<div class="versionAuthors">Authors: ' . htmlentities($version->getAuthors(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . "</div>\n");
                 if(Tools::useAffiliations()) {
		   print("Affiliations: " . htmlentities($version->getAffiliations(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') ."<br />\n");
		 }
                 if(Tools::useCountry()) {
		   print("Countries: " . htmlentities($version->getCountry(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') ."<br />\n");
		 }
                 if(Tools::useAbstract()) {
                   print('Abstract:<br /><div class="versionAbstract">' . htmlentities($version->getAbstract(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</div>');
                 }
                 if(Tools::useCategory()) {
		   print("Category: " . htmlentities($allcategories[$version->getCategory()], ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . "<br />");
		 }
                 if(Tools::useKeywords()) {
                   print('Keywords: ' . htmlentities($version->getKeywords(), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '<br />');
                 }
                 print('Chair comments:<br />');
                 $submission->printAllChairComments(); 
                 $submission->printJavascriptWarnings();
                 $submission->printMD5collisions();
                 print("</div></div></div></div></div>");
               ?>
	       </div>
             </div>
	   </td>
	   <td>
	     <div class="bigNumber"><?php print($submissionNumber); ?></div>
	   </td>
	   <td class="leftAlign">
  	    <table>
	      <tr>
	        <td class="leftAlign"><?php Tools::printHTMLsubstr($submission->getLastVersion()->getTitle(),50); ?></td>
	      </tr><tr>
	        <td class="leftAlign"><em><?php Tools::printHTMLsubstr("Authors: ".$submission->getLastVersion()->getAuthors(),50); ?></em></td>
	      </tr>
	    </table>
	   </td>
           <td>
             <?php 
             $md5Coll = ($submission->getMD5collisions() != "");
             $lastVersionJS = preg_match("/(^|;)". $submission->getLastVersionNumber() ."($|;)/", $submission->getJavascriptWarnings());
             if($md5Coll || $lastVersionJS) {
	       if($md5Coll) {
		 print("MD5 collision<br />");
	       }
	       if($lastVersionJS) {
		 print("Javascript");
	       }
	     } else {
	       print("-");
	     }
             ?>
           </td>
	   <td><div class="bigNumber">
	     <?php print($submission->getLastVersionNumber()); ?>/<?php print($transferedVersion); ?>
	   </div></td>
	 </tr>
       <?php 
     }
      ?>
    </table>
    <input type="submit" class="buttonLink bigButton" value="Update/Create the Article Database" />
  </center>
</form>

<div class="clear bottomSpacer"></div>

<?php }}?>

<?php include('footer.php'); ?>
