<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include '../utils/tools.php';

$currentReviewerGroup = Reviewer::getGroupByLogin(); 
$currentReviewer = Reviewer::getReviewerByLogin(); 

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

$submissionNumber = Tools::readPost('submissionNumber');

$submission = Submission::getBySubmissionNumber($submissionNumber);

if (!is_null($submission)) {
  Log::logDownloadSubmission($submission,$currentReviewer);
  $versions = $submission->getAllVersions();
  $file = $versions[Tools::readPost('versionNumber')]->getFile();
  
  if(preg_match("/\.[pP][dD][fF]$/", $file)) {
    header('Content-type: application/pdf');
  } else if(preg_match("/\.[pP][sS]$/", $file)) {
    header('Content-type: application/postscript');
  } else {
    header('Content-type: application/binary');
  }     
  
  header('Content-Disposition: attachment; filename="' . $file . '"');
  header('Content-Length: '.filesize($submission->getFolder() . $file));
  @readfile($submission->getFolder() . $file);
}

}

?>
