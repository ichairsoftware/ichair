<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Messages to Committee';
include '../utils/tools.php';
include 'header.php';
if(!is_null($currentReviewer)) {

$articles = Article::getAllArticles();

$generalMessages = Meeting::getAllNonTrashedGeneralMessages();
$specificMessages = Meeting::getAllNonTrashedAuthorizedArticleMessages($currentReviewer);


if((count($generalMessages) + count($specificMessages)) == 0) {
  print('<div class="OKmessage"><em>No message yet!</em></div>');
} else {


/* Display a list of "general" purpose messages together with buttons that allow to update/delete the message */
?>
<center>
  <?php foreach($generalMessages as $message) { ?>
    <table>
      <tr>
        <td>
	  <div class="paperBoxTitle" style="margin: 0;">
	    <div class="floatRight"><em><?php print(gmdate("D, j M Y h:i a", $message->getDate())); ?></em></div>
   	    <br />
	  </div>
	</td>
      </tr>
      <tr>
        <td><div class="messageBox"><?php Tools::printHTMLbr($message->getContent()); ?></div></td>
      </tr>
    </table>
    <br />
  <?php } ?>

  <?php foreach($specificMessages as $message) { ?>
    <?php $article = $articles[$message->getArticleNumber()]; ?>
    <table>
      <tr>
	<td>
	  <div class="paperBoxTitle" style="margin: 0;">
	    <div class="floatRight"><em><?php print(gmdate("D, j M Y h:i a", $message->getDate())); ?></em></div>
   	    <b><?php print('Article ' . $message->getArticleNumber()); ?>: </b><?php Tools::printHTMLsubstr($article->getTitle(), 60); ?>
	  </div>
	</td>
      </tr>
      <tr>
        <td><div class="messageBox"><?php Tools::printHTMLbr($message->getContent()); ?><br /></div></td>
      </tr>
      <tr>
        <td style="text-align: right; padding: 7px 0px 0 0;">
          <a href="discuss_article.php?articleNumber=<?php print($message->getArticleNumber()); ?>#firstNotSeen" class="buttonLink" target="_blank">Reviews & Discussion</a>
        </td>
      </tr>
    </table>
    <br />
  <?php } ?>
</center>
<?php 
      } }
?>
<?php include('footer.php'); ?>
