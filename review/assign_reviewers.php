<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include '../utils/tools.php';
$articleNumber = Tools::readPost('articleNumber');
$page_title='Assign Reviewers for Article&nbsp;' . $articleNumber;
include 'header.php';


if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $article = Article::getByArticleNumber($articleNumber);
  $nextArticleNumber = $article->getNextArticleNumber();

  $articleToUpdate = Article::getByArticleNumber(Tools::readPost('articleToUpdate'));
  
  $articleWasUpdated = false;
  if(!is_null($articleToUpdate)) {
    $assignements = Assignement::getAssignementsByArticleNumber($articleToUpdate->getArticleNumber());
    foreach($assignements as $assignement) {
      $newAssignementStatus = Tools::readPost("" . $assignement->getReviewerNumber());
      if(Assignement::isValidAssignementStatus($newAssignementStatus) && ($newAssignementStatus != $assignement->getAssignementStatus())) {
	$assignement->setAssignementStatus($newAssignementStatus);
	$articleWasUpdated = true;
      }
    }
    if($articleWasUpdated) {
      Log::logAssignementArticle($articleToUpdate, $currentReviewer);
      ?>
        <div class="OKmessage">Permissions for article <?php Tools::printHTML($articleToUpdate->getArticleNumber()); ?> have been saved successfully.</div>
      <?php 
    }
  }

  /* If we just updated the last article... */

  if((!is_null($articleToUpdate)) && (Tools::readPost('buttonName') == "Save Changes for this Last Article")) {?>
    <div class="OKmessage">This was the last article. You can go back to the list.</div>
    <div class="floatRight">
       <form action="assign_reviewers_list.php" method="post">
         <input type="hidden" name="articleNumber" value="<?php print($articleNumber); ?>" />
         <input type="submit" class="buttonLink bigButton" value="Go Back to the List"/>
       </form>
    </div><?php 
  } else {

    $article->printDetailsBoxForChairPopUp();
    ?>
    <center>

    <form action="assign_reviewers_list.php#nbr<?php print($articleNumber);?>" method="post">
      <input type="hidden" name="articleNumber" value="<?php print($articleNumber); ?>" />
      <input type="submit" class="buttonLink bigButton" value="Go Back to the List" />
    </form>

    <form action="assign_reviewers.php" method="post">
    <input type="hidden" name="articleToUpdate" value="<?php print($articleNumber);?>" />

    <?php 
    $assignedReviewers = $article->getAssignedReviewers();
    $authorizedReviewers = $article->getAuthorizedReviewers();
    $blockedReviewers = $article->getBlockedReviewers();
    ?>

    <table class="usersTable">
    <tr>
      <th>Preference</th>
      <th>Full Name</th>
      <th colspan="3">Status</th>
      <th>Number of Assignments</th>
    </tr>
    <?php 
    foreach($assignedReviewers as $reviewerNumber) {
      $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
      $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
      switch ($assignement->getAffinity()){
      case 2:
	print('<tr><td><div class="completedReview">Willing' ."</div></td>\n");
	break;
      case 1:
	print('<tr><td><div class="inProgressReview">Neutral' ."</div></td>\n");
	break;
      default:
	print('<tr><td><div class="voidReview">Not able' ."</div></td>\n");
      }
      print("<td>" . $reviewer->getFullName() ."</td>\n");
      print('<td><input class="noBorder" type="radio" name="' . $reviewerNumber . '" id="assign' .$reviewerNumber .'" value="'. Assignement::$ASSIGNED .'" checked="checked" /><label for="assign' .$reviewerNumber ."\">Assigned</label></td>\n");
      print('<td><input class="noBorder" type="radio" name="' . $reviewerNumber . '" id="authorize' . $reviewerNumber .'" value="'. Assignement::$AUTHORIZED .'" /><label for="authorize' . $reviewerNumber . "\">Authorized</label></td>\n");
      print('<td><input class="noBorder" type="radio" name="' . $reviewerNumber . '" id="block' . $reviewerNumber .'" value="'. Assignement::$BLOCKED .'" /><label for="block' . $reviewerNumber . "\">Blocked</label></td>\n");
      print('<td><div class="bigNumber">'.$reviewer->getNumberOfAssignedArticles());
      if ($reviewer->getNumberOfArticles() != "") {
	print('/'.$reviewer->getNumberOfArticles());
      }
      print('</div></td></tr>');
    }
    foreach($authorizedReviewers as $reviewerNumber) {
      $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
      $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
      switch ($assignement->getAffinity()){      
      case 2:     
	print('<tr><td><div class="completedReview">Willing' ."</div></td>\n");              
	break;    
      case 1:    
	print('<tr><td><div class="inProgressReview">Neutral' ."</div></td>\n");              
	break;    
      default:    
	print('<tr><td><div class="voidReview">Not able' ."</div></td>\n");              
      }
      print("<td>" . $reviewer->getFullName() ."</td>\n");
      if($reviewer->getGroup() != Reviewer::$OBSERVER_GROUP) {
        print('<td><input class="noBorder" type="radio" name="' . $reviewerNumber . '" id="assign' .$reviewerNumber .'" value="'. Assignement::$ASSIGNED .'" /><label for="assign' .$reviewerNumber ."\">Assigned</label></td>\n");                  
      } else {
	print("<td></td>\n");
      }
      print('<td><input class="noBorder" type="radio" name="' . $reviewerNumber . '" id="authorize' . $reviewerNumber .'" value="'. Assignement::$AUTHORIZED .'" checked="checked" /><label for="authorize' . $reviewerNumber . "\">Authorized</label></td>\n");
      print('<td><input class="noBorder" type="radio" name="' . $reviewerNumber . '" id="block' . $reviewerNumber .'" value="'. Assignement::$BLOCKED .'" /><label for="block' . $reviewerNumber . "\">Blocked</label></td>\n");
      print('<td><div class="bigNumber">'.$reviewer->getNumberOfAssignedArticles());
      if ($reviewer->getNumberOfArticles() != "") {
	print('/'.$reviewer->getNumberOfArticles());
      }
      print('</div></td></tr>');
    }
    foreach($blockedReviewers as $reviewerNumber) {
      $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
      $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
      switch ($assignement->getAffinity()){
      case 2:
	print('<tr><td><div class="completedReview">Willing' ."</div></td>\n");
	break;
      case 1: 
	print('<tr><td><div class="inProgressReview">Neutral' ."</div></td>\n");
	break;
      default:
	print('<tr><td><div class="voidReview">Not able' ."</div></td>\n");
      }
      print("<td>" . $reviewer->getFullName() ."</td>\n");
      if($reviewer->getGroup() != Reviewer::$OBSERVER_GROUP) {
        print('<td><input class="noBorder" type="radio" name="' . $reviewerNumber . '" id="assign' .$reviewerNumber .'" value="'. Assignement::$ASSIGNED .'" /><label for="assign' .$reviewerNumber ."\">Assigned</label></td>\n");
      } else {
	print("<td></td>\n");
      }
      print('<td><input class="noBorder" type="radio" name="' . $reviewerNumber . '" id="authorize' . $reviewerNumber .'" value="'. Assignement::$AUTHORIZED .'" /><label for="authorize' . $reviewerNumber . "\">Authorized</label></td>\n");
      print('<td><input class="noBorder" type="radio" name="' . $reviewerNumber . '" id="block' . $reviewerNumber .'" value="'. Assignement::$BLOCKED .'" checked="checked" /><label for="block' . $reviewerNumber . "\">Blocked</label></td>\n");
      print('<td><div class="bigNumber">'.$reviewer->getNumberOfAssignedArticles());
      if ($reviewer->getNumberOfArticles() != "") {
	print('/'.$reviewer->getNumberOfArticles());
      }
      print('</div></td></tr>');

    }
    ?>
    <tr>
       <td colspan="2" class="left"><div class="bigNumber">Totals:</div></td>
       <td><div class="bigNumber completedReview"><?php print(count($assignedReviewers). "/" . $article->getNumberOfReviewers()); ?></div></td>
       <td><div class="bigNumber inProgressReview"><?php print(count($authorizedReviewers)); ?></div></td>
       <td><div class="bigNumber voidReview"><?php print(count($blockedReviewers)); ?></div></td>
    </tr>
    </table>

    <?php  if (!is_null($nextArticleNumber)){  ?>
              <input type="hidden" name="articleNumber" value="<?php print($nextArticleNumber);?>" />
	      <input type="submit" name="buttonName" class="buttonLink bigButton" value="Save Changes and Go To Next Article" />
          <?php } else { ?>
              <input type="hidden" name="articleNumber" value="<?php print($articleNumber);?>" />
	      <input type="submit" name="buttonName" class="buttonLink bigButton" value="Save Changes for this Last Article" />
     <?php  }  ?>
  </form>					  
  </center>
  <?php }} ?>

<?php include('footer.php'); ?>
