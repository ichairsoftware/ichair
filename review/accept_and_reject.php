<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Accept &amp; Reject Articles';
include '../utils/tools.php';
include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $resetPageToDisplay = false;
  $lastVisitDateSet = false;

  if((Tools::readPost('action') == "Set my Last Visit Date to the Current Time") || (Tools::readPost('action') == "Update Acceptances and Set Visit Date")) {
    $date = Tools::readPost('date');
    $lastVisitDateSet = $currentReviewer->setLastVisitDate($date);
  }

  if(Tools::readPost('action') == "Take a Snapshot of Current Averages") {
    Article::snapshotAverages();
  } 

  /* Update the number of articles displayed for the current reviewer */
  
  $numberOfArticlesPerPage = Tools::readPost('numberOfArticlesPerPage');
  if(preg_match("/^[1-9][0-9]*$/", $numberOfArticlesPerPage)) {
    $currentReviewer->setNumberOfArticlesPerPage($numberOfArticlesPerPage);
  }

  /* update view according to the chair preferences */

  if(Tools::readPost('action') == "Update View") {

    $newSortKey = 0x0000;
    if(Tools::readPost('sortKey') == "overallAverage") {
      $newSortKey = Reviewer::$SORT_BY_OVERALL_GRADE;
    } else if(Tools::readPost('sortKey') == "articleNumber") {
      $newSortKey = Reviewer::$SORT_BY_ARTICLE_NUMBER;
    } else if(Tools::readPost('sortKey') == "acceptance") {
      $newSortKey = Reviewer::$SORT_BY_ACCEPTANCE_STATUS;
    } else if(Tools::readPost('sortKey') == "lastModificationDate") {
      $newSortKey = Reviewer::$SORT_BY_LAST_MODIFICATION;
    } else {
      $newSortKey = Reviewer::$SORT_BY_ACCEPTANCE_STATUS_AND_GRADE;
    }
    if($newSortKey != $currentReviewer->getSortKey()) {
      $resetPageToDisplay = true;
    } else if((Tools::readPost('recent') == "yes") != $currentReviewer->sortIsRecentOnly()) {
      $resetPageToDisplay = true;
    } else if((Tools::readPost('reverseSort') == "yes") != $currentReviewer->sortIsReversed()) {
      $resetPageToDisplay = true;
    } else if((Tools::readPost('useSnapshot') == "yes") != $currentReviewer->sortUseSnapshot()) {
      $resetPageToDisplay = true;      
    }
    $currentReviewer->setSortKeyString($newSortKey, 
				       (Tools::readPost('reverseSort') == "yes"),
				       (Tools::readPost('viewType') == "extendedView"),
				       $currentReviewer->sortIsRestricted(),
				       (Tools::readPost('recent') == "yes"),
				       (Tools::readPost('useSnapshot') == "yes"));
  }
  
  /* set the sort variables according to the user preferences */
  
  $viewType="simpleView";
  $reverseSort = false;
  $useSnapshot = false;
  $recent = false;
  if($currentReviewer->sortIsExtended()) {
    $viewType="extendedView";
  } 
  if($currentReviewer->sortIsReversed()) {
    $reverseSort = true;
  } 
  if($currentReviewer->sortUseSnapshot()) {
    $useSnapshot = true;
  } 
  if($currentReviewer->sortIsRecentOnly()) {
    $recent = true;
  }
  $sortKey = $currentReviewer->getSortKey();
  
  $allArticles = Article::getAllArticles();

  $acceptancesUpdated = false;
  if((Tools::readPost('action') == "Update Acceptances") || (Tools::readPost('page') != "") || (Tools::readPost('action') == "Update Acceptances and Set Visit Date")) {
		
    foreach($allArticles as $article) {
      $articleNumber = $article->getArticleNumber();
      $newAcceptance = Tools::readPost('newAcceptance' . $articleNumber);
      if($newAcceptance != "") {
	/* if there was a change, update modification date */
	$oldAcceptance = $article->getAcceptance();
	if($newAcceptance == "Accept") {
	  $article->setAcceptance(Article::$ACCEPT);
	} else if($newAcceptance == "Maybe Accept") {
	  $article->setAcceptance(Article::$MAYBE_ACCEPT);
	} else if($newAcceptance == "Discuss") {
	  $article->setAcceptance(Article::$DISCUSS);
	} else if($newAcceptance == "Maybe Reject") {
	  $article->setAcceptance(Article::$MAYBE_REJECT);
	} else if($newAcceptance == "Reject") {
	  $article->setAcceptance(Article::$REJECT);
	} else if($newAcceptance == "Void"){
	  $article->setAcceptance(Article::$VOID);
	} else if($newAcceptance == Tools::getCustomAccept1()){
	  $article->setAcceptance(Article::$CUSTOM1);
	} else if($newAcceptance == Tools::getCustomAccept2()){
	  $article->setAcceptance(Article::$CUSTOM2);
	} else if($newAcceptance == Tools::getCustomAccept3()){
	  $article->setAcceptance(Article::$CUSTOM3);
	} else if($newAcceptance == Tools::getCustomAccept4()){
	  $article->setAcceptance(Article::$CUSTOM4);
	} else if($newAcceptance == Tools::getCustomAccept5()){
	  $article->setAcceptance(Article::$CUSTOM5);
	} else if($newAcceptance == Tools::getCustomAccept6()){
	  $article->setAcceptance(Article::$CUSTOM6);
	} else if($newAcceptance == Tools::getCustomAccept7()){
	  $article->setAcceptance(Article::$CUSTOM7);
	}
	if ($article->getAcceptance() != $oldAcceptance) {
	  $article->setLastModificationDate();
	  $acceptancesUpdated=true;
	  Log::logAcceptanceChange($articleNumber, $article->getAcceptance(), $currentReviewer);
	}
      }
    }
    
  }

  $articleNumbers = Article::getSortedArticleNumbersForChair($currentReviewer, $sortKey, $reverseSort, $useSnapshot, $recent, $allArticles);
  $reviewReader = new ReviewReader();

  /* Get the article to display from the POST */
  $postedPage = Tools::readPost('page');
  $pageToDisplay;
  if((!$resetPageToDisplay)) {
    if(preg_match("/^[0-9]+/", $postedPage)) {
      /* when sorting by article number */
      $articleToDisplay = explode(" ", $postedPage);
      $articleToDisplay = $articleToDisplay[0];
      $position = array_search($articleToDisplay, $articleNumbers);
      $pageToDisplay = floor($position/$currentReviewer->getNumberOfArticlesPerPage())+1;
    } else if(preg_match("/^Page [0-9]+/", $postedPage)) {
      /* when sorting by something else */
      $pageToDisplay = explode(" ", $postedPage);
      $pageToDisplay = $pageToDisplay[1];
    } else {
      if(preg_match("/^[0-9]+$/", Tools::readPost('previousPage'))) {
	$pageToDisplay = Tools::readPost('previousPage');
      } else {
	$pageToDisplay = 1;
      }
    }
  } else {
    $pageToDisplay = 1;
  }

    /* Display a form allowing to change the number of article displayed on the same page */
    ?>
    <div class="floatRight">
       <form action="accept_and_reject.php" method="post">
       <input type="hidden" name="previousPage" value="<?php print($pageToDisplay); ?>" />
       <table><tr>
        <td><div class="versionAuthors">Number of articles displayed per page:&nbsp;</div></td>
        <td><input type="text" name="numberOfArticlesPerPage" size="3" class="largeInput" value="<?php print($currentReviewer->getNumberOfArticlesPerPage()); ?>" /></td>
	<td><input type="submit" class="buttonLink" value="Ok" /></td>
       </tr></table>
      </form>
    </div>
    <div class="clear"></div>


    <div class="paperBox">
    <div class="paperBoxDetails">
      <form action="accept_and_reject.php" method="post"> 
        <center>
	  <table>
	    <tr>
              <td>
	        <input type="radio" class="noBorder" name="viewType" id="simpleViewType" value="simpleView" <?php if($viewType == "simpleView") { ?>checked="checked" <?php } ?>/>
		<label for="simpleViewType">Simple View</label>
              </td>
	      <td>Sort By:</td>
	      <td>
	        <input type="radio" class="noBorder" name="sortKey" id="sortKeyArticleNumber" value="articleNumber" <?php if($sortKey == Reviewer::$SORT_BY_ARTICLE_NUMBER) { ?>checked="checked" <?php } ?>/>
		<label for="sortKeyArticleNumber">Article Number</label>	        
	      </td>
	      <td>
	        <input type="radio" class="noBorder" name="sortKey" id="sortKeyOverallAverage" value="overallAverage" <?php if($sortKey == Reviewer::$SORT_BY_OVERALL_GRADE) { ?>checked="checked" <?php } ?>/>
		<label for="sortKeyOverallAverage">Overall Grade</label>	        
	      </td>
	      <td>
	        <input type="radio" class="noBorder" name="sortKey" id="sortKeyAcceptance" value="acceptance" <?php if($sortKey == Reviewer::$SORT_BY_ACCEPTANCE_STATUS) { ?>checked="checked" <?php } ?>/>
		<label for="sortKeyAcceptance">Accept &amp; Reject Status</label>	        
	      </td>
              <td> 
                <input type="radio" class="noBorder" name="sortKey" id="sortKeyAcceptanceAndGrade" value="acceptance_and_grade" <?php if($sortKey == Reviewer::$SORT_BY_ACCEPTANCE_STATUS_AND_GRADE) { ?>checked="checked" <?php } ?>/>
                <label for="sortKeyAcceptanceAndGrade">Status &amp; Grade</label>               
              </td>
              <td> 
                <input type="radio" class="noBorder" name="sortKey" id="sortKeyLastModification" value="lastModificationDate" <?php if($sortKey == Reviewer::$SORT_BY_LAST_MODIFICATION) { ?>checked="checked" <?php } ?>/>
                <label for="sortKeyLastModification">Last Modification</label>               
              </td>

	    </tr>
	    <tr>
              <td>
	        <input type="radio" class="noBorder" name="viewType" id="extendedViewType" value="extendedView" <?php if($viewType == "extendedView") { ?>checked="checked" <?php } ?>/>
		<label for="extendedViewType">Extended View</label>
    	      </td>
	      <td>&nbsp;</td>
	      <td colspan="3">
                <input type="checkbox" class="noBorder" name="recent" id="recent" value="yes" <?php if($recent) { ?>checked="checked" <?php } ?>/>      
                <label for="recent">Show only articles modified since your last visit date</label>
	      </td>
	      <td colspan="2">
	        <input type="checkbox" class="noBorder" name="useSnapshot" id="useSnapshot" value="yes" <?php if($useSnapshot) { ?>checked="checked" <?php } ?>/>
		<label for="useSnapshot">Use snapshot grade</label>
	      </td>
 	    </tr>
	    <tr>
	      <td colspan="2">&nbsp;</td>
	      <td colspan="2">
	        <input type="checkbox" class="noBorder" name="reverseSort" id="reverseSort" value="yes" <?php if($reverseSort) { ?>checked="checked" <?php } ?>/>
		<label for="reverseSort">Reverse sort order</label>
	      </td>
	      <td colspan="3">&nbsp;</td>
            </tr>
	  </table>
	  <input type="hidden" name="previousPage" value=<?php print($pageToDisplay); ?> />
          <input type="submit" class="buttonLink bigButton" name="action" value="Update View" />
	</center>
      <?php if ($currentReviewer->getLastVisitDate() != 0) {?>
        <div class="floatRight">
          Last Visit: 
	  <em>
	    <?php 
	    if(Tools::use12HourFormat()) {
  	      print(gmdate("D, j M Y h:i a", $currentReviewer->getLastVisitDate()));
	    } else {	      
	      print(gmdate("D, j M Y H:i", $currentReviewer->getLastVisitDate()));
	    }
	    ?>
	  </em>
        </div>
      <?php } ?>
      </form>
      <div class="clear"></div>
    </div>
    <div class="floatRight">
    <form action="accept_and_reject.php" method="post">
      <input type="hidden" name="previousPage" value=<?php print($pageToDisplay); ?> />
      <input type="hidden" name="date" value=<?php print(gmdate("U")); ?> />	  
      <input type="submit" class="buttonLink" name="action" value="Set my Last Visit Date to the Current Time" />
    </form>
    </div>
    <div class="clear"></div>
    </div>

  <?php 

  /* Update Snapshot or Acceptances */

  if(Tools::readPost('action') == "Take a Snapshot of Current Averages") {
    print('<div class="OKmessage">New snapshot taken successfully.</div>');
  } 

  if($lastVisitDateSet) {
    print('<div class="OKmessage">Your last visit date has been successfully set to the current time.</div>');
  }

  if($acceptancesUpdated) {
    print('<div class="OKmessage">Update successfull.</div>');
  }


  /* from here, we have a $pageToDisplay initialized correctly */

  ?>
  <center>
  <?php Article::printAcceptanceSummaryTable(); ?>

  <form action="accept_and_reject.php" method="post">


  <?php 

  /* Get specific messages from the chair */

  $specificMessages = Meeting::getAllNonTrashedMessagesInDoubleArrayIndexedByArticleNumber();


  /* Get the range of article to display */

  $rangeOfArticleNumbers=array();
  $startKey = ($pageToDisplay-1)*$currentReviewer->getNumberOfArticlesPerPage();
  $nbrArticles = count($articleNumbers);
  for($i=0; ($i<$currentReviewer->getNumberOfArticlesPerPage()) && ($startKey+$i < $nbrArticles); $i++) {
    $rangeOfArticleNumbers[$i] = $articleNumbers[$startKey+$i];
  }

  /* Display buttons */

  $numberOfButtons = ceil($nbrArticles/$currentReviewer->getNumberOfArticlesPerPage());
  for($btNbr=0; $btNbr<$numberOfButtons; $btNbr++) {
    print('<div class="floatLeft">');
    $buttonLabel = 'Page '.($btNbr+1); 
    if($sortKey == Reviewer::$SORT_BY_ARTICLE_NUMBER) {
      $buttonLabel = $articleNumbers[$btNbr*$currentReviewer->getNumberOfArticlesPerPage()];
      $end = $articleNumbers[min(count($articleNumbers), ($btNbr+1)*$currentReviewer->getNumberOfArticlesPerPage())-1];
      if($end != $buttonLabel) {
	$buttonLabel .= ' - ' . $end;
      }
    }
    if(($pageToDisplay-1) == $btNbr) { 
      print('<input type="submit" class="buttonLink currentButton" name="page" value="' . $buttonLabel . '" />');
    } else {
      print('<input type="submit" class="buttonLink" name="page" value="' . $buttonLabel . '" />');
    } 
    print('</div>');
  }
  print('<div class="clear">&nbsp;</div>');


  if (count($rangeOfArticleNumbers) == 0) {
    print('<div class="OKmessage"><em>No articles to display</em></div>');
    return;
  }

  if($viewType == "simpleView") { ?>
    <table class="dottedTable">
      <tr>
	<th class="topRow" style="width: 5em;">&nbsp;</th>	
        <th class="topRow">&nbsp;</th>
	<th class="topRow">Art.<br/>Num.</th>
        <th class="leftAlign topRow">
	  <table>
	    <tr>
	      <th class="leftAlign topRow">Title</th>
	    </tr><tr>
	      <th class="leftAlign topRow"><em>Authors</em></th>
	    </tr><tr>
	      <th class="leftAlign topRow">Last Modification Date</th>
	    </tr>
	  </table>
        </th>
        <th class="topRow">Snapshot<br/>Grade</th>
        <th class="topRow">Average<br/>Grade</th>
	<th class="topRow">Completed<br/>Reviews</th>
        <th class="topRow">&nbsp;</th>
      </tr>
      <?php 
   }

  foreach($rangeOfArticleNumbers as $articleNumber) {
    $article = $allArticles[$articleNumber];
    if($viewType == "extendedView") {
    
    ?>
     <div class="paperBox" id="nbr<?php print($articleNumber); ?>">
      <div class="paperBoxTitle">
        <div class="paperBoxNumber">Article&nbsp;<?php print($articleNumber) ?></div>
	<div class="floatRight"><?php $article->printCompletedReviewsRatio(); ?></div>
          Title: <?php Tools::printHTML($article->getTitle()); ?><br/>
          <em>Authors: <?php Tools::printHTML($article->getAuthors()); ?></em>
      </div>
    <?php 

    if ($article->getIsCommitteeMember()){
      print('<div class="paperBoxDetailsCommittee">');
    } else {
      print('<div class="paperBoxDetails">');
    }
     /* Display the list of all the completed reviews concerning this article    */
    $assignements  = Assignement::getByArticleNumberAndAssignementStatus($articleNumber, Assignement::$ASSIGNED);
    $assignementsNotAssigned = Assignement::getNotAssignedNotVoidByArticleNumber($articleNumber); 
    print('<div class="floatRight">');
    print('<div class="rightAlign">');
    $acceptance = $article->getAcceptance();
    if($acceptance == Article::$ACCEPT) {?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber acceptArticle"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber acceptArticle"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$MAYBE_ACCEPT) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber maybeAcceptArticle"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber maybeAcceptArticle"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$DISCUSS) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber discussArticle"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber discussArticle"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$MAYBE_REJECT) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber maybeRejectArticle"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber maybeRejectArticle"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$REJECT) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber rejectArticle"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber rejectArticle"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$CUSTOM1) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber customAccept1"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber customAccept1"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$CUSTOM2) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber customAccept2"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber customAccept2"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$CUSTOM3) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber customAccept3"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber customAccept3"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$CUSTOM4) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber customAccept4"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber customAccept4"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$CUSTOM5) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber customAccept5"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber customAccept5"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$CUSTOM6) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber customAccept6"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber customAccept6"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else if($acceptance == Article::$CUSTOM7) { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber customAccept7"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber customAccept7"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    } else { ?>
        <table class="centeredTable"><tr><th>Snapshot</th><th>Average</th></tr>
        <tr><td><div class="bigNumber voidArticle"><?php print($article->roundAverageOverallGradeSnapshot(2)); ?></div></td><td><div class="bigNumber voidArticle"><?php print($article->roundAverageOverallGrade(2)); ?></div></td></tr></table>
<?php    }      
      ?>
      </div><div class="rightAlign"><?php $article->printAcceptanceSelect(); ?></div>
      </div>
      <div class="accept_reject_customcheckboxes"><?php $article->printCustomCheckboxes(true); ?></div>
      <?php if((count($assignements)+count($assignementsNotAssigned) == 0)) { ?>
	  No completed/assigned review at this time.
      <?php } else { ?> 
	  <table class="smallCentered">
	    <tr>
	      <th colspan="2">&nbsp;</th>
	      <?php ReviewReader::printReviewRowHead(); ?>
	    </tr>
	    <?php 
          if (count($assignements) != 0) {
	    print('<tr><td>&nbsp;</td>');
	    ReviewReader::printReviewSeparator('-- Assigned reviews --');
	    print('</tr>');
	    foreach($assignements as $assignement) {
	      $reviewerNumber = $assignement->getReviewerNumber();
	      $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
	      $reviewReader = new ReviewReader();
	      $reviewReader->createFromXMLFile($assignement);
	      ?>
	      <tr>
	     <?php if ($assignement->getReviewStatus() == Assignement::$VOID) {
  	       print('<td>&nbsp;</td>');
             } else { ?> 
		<td class="button">
		  <div class="popUp">
		     <a href="view_reviews_bis.php?articleNumber=<?php print($articleNumber); ?>#nbr<?php print($reviewerNumber); ?>" target="_blank">Details</a>
		    <div class="hidden">
		      <?php $reviewReader->printDetailsBoxPopUp(); ?>
		    </div>
		  </div>
		</td>
	     <?php }
		if ($assignement->getReviewStatus() == Assignement::$VOID) {
		  print('<td><div class="voidReview">No Review</div></td>');
		} else if ($assignement->getReviewStatus() == Assignement::$INPROGRESS) {
		  print('<td><div class="inProgressReview">In Progress</div></td>');
		} else {
		  print('<td><div class="completedReview">Completed</div></td>');
		}
		$reviewReader->printReviewRow(false);
		print('</tr>');
	    }
	  }
          if (count($assignementsNotAssigned) != 0) {
	    print('<tr><td>&nbsp;</td>');
	    ReviewReader::printReviewSeparator('-- Unsolicited reviews --');
	    print('</tr>');
	    foreach($assignementsNotAssigned as $assignement) {
	      $reviewerNumber = $assignement->getReviewerNumber();
	      $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
	      $reviewReader = new ReviewReader();
	      $reviewReader->createFromXMLFile($assignement);
	      ?>
	      <tr>
		<td class="button">
		  <div class="popUp">
		    <a href="view_reviews_bis.php?articleNumber=<?php print($articleNumber); ?>#nbr<?php print($reviewerNumber); ?>" target="_blank">Details</a>
		    <div class="hidden">
		      <?php $reviewReader->printDetailsBoxPopUp(); ?>
		    </div>
		  </div>
		</td>
	     <?php 
		if ($assignement->getReviewStatus() == Assignement::$INPROGRESS) {
		  print('<td><div class="inProgressReview">In Progress</div></td>');
		} else {
		  print('<td><div class="completedReview">Completed</div></td>');
		}
		$reviewReader->printReviewRow(false);
		print('</tr>');
	    }
	  } ?>
	  </table>
      <?php } ?>
      <div class="clear"></div>

      <?php 
	 /* Check if the current reviewer has made any personnal comments in his review of the article*/
	 $assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());
         $reviewReader = new ReviewReader();
	 $reviewReader->createFromXMLFile($assignement);
	 if (trim($reviewReader->getPersonalNotes()) != "") {
	   print('Personal Notes:<div class="chairComments">');
	   Tools::printHTMLbr($reviewReader->getPersonalNotes());
	   print('</div>');
	 }

         /* Display a button that gives to access to the details of the discussion */
	 ?>
      <table class="smallCentered"><tr><td class="button">
      <div class="popUp">
        <a href="discuss_article.php?articleNumber=<?php print($articleNumber); ?>#firstNotSeen" target="_blank">Reviews &amp; Discussion</a>
	 <div class="hidden">
	 <div class="paperBoxDetails">
	 <?php $article->printLastComments(8); ?>
	 </div>
	 </div>
      </div>
      </td>
      <td>
      <?php $article->printLastModificationDateAndNumberOfComments($currentReviewer); ?>
      </td></tr></table>
      <?php if(array_key_exists($articleNumber, $specificMessages) && !is_null($specificMessages[$articleNumber])) { 
        foreach($specificMessages[$articleNumber] as $message) { ?>
          <div class="messageBox">
            <?php 
            Tools::printHTMLbr($message->getContent());
            ?>
          </div>
        <?php } ?>
      <?php } ?>
      <?php /* End of paperBoxDetails */?>
    </div>

  </div>
 
       <?php /* END OF EXTENDED VIEW, BEGINING OF SIMPLE VIEW */
	} else {
	?>
	<tr>
	  <td>
	    <div class="popUp">
	      <a href="view_reviews_bis.php?articleNumber=<?php print($articleNumber); ?>" target="_blank">Details</a>
	      <div class="hidden"><?php 
	             if ($article->getIsCommitteeMember()){
		       print('<div class="paperBoxDetailsCommittee">');
		     } else {
		       print('<div class="paperBoxDetails">');
		     }
		     $article->printCustomCheckboxes(true);
		     $assignements = Assignement::getByArticleNumberAndAssignementStatus($articleNumber, Assignement::$ASSIGNED);
	             $assignementsNotAssigned = Assignement::getNotAssignedNotVoidByArticleNumber($articleNumber);?>
		  <?php if(count($assignements)+count($assignementsNotAssigned) == 0) { ?>
		    No completed/assigned review at this time.
		  <?php } else { ?>
		    <table class="smallCentered">
		      <tr>
		        <td>&nbsp;</td>
		        <?php ReviewReader::printReviewRowHead(); ?> 
		      </tr>
		      <?php 
                      if (count($assignements) != 0) {
                        print('<tr>');
                        ReviewReader::printReviewSeparator('-- Assigned reviews --');
                        print('</tr>');
  		        foreach($assignements as $assignement) {
			  $reviewReader->createFromXMLFile($assignement);
			?>
			<tr>
		          <?php $reviewReader->printReviewRow(true); ?>	
	                </tr>
	              <?php }
                      }
		      if (count($assignementsNotAssigned) != 0) {
			print('<tr>');
			ReviewReader::printReviewSeparator('-- Unsolicited reviews --'); 
			print('</tr>');
   		        foreach($assignementsNotAssigned as $assignement) {
			  $reviewReader->createFromXMLFile($assignement);
			  ?>
			  <tr>
		            <?php $reviewReader->printReviewRow(true); ?>	
	                  </tr>
	              <?php }
		      }
		      ?>
	            </table>
		    <?php 
  		    $assignement = Assignement::getByNumbers($articleNumber, $currentReviewer->getReviewerNumber());
   		    $reviewReader->createFromXMLFile($assignement);
                    if (trim($reviewReader->getPersonalNotes()) != "") {
		      print('<br />Personal Notes:<div class="chairComments">');
		      Tools::printHTMLbr($reviewReader->getPersonalNotes());
		      print('</div>');
		    }
		  } 
		  ?>
		</div>
	      </div>
	    </div>
	  </td>
          <td>
            <div class="popUp">
              <a href="discuss_article.php?articleNumber=<?php print($articleNumber); ?>#firstNotSeen" target="_blank">Rev.&nbsp;&amp;&nbsp;Disc.</a>
	      <div class="hidden">
	        <div class="paperBoxDetails">
	          <?php $article->printLastComments(8); ?>
	        </div>
	      </div>
            </div>
          </td>
	  <td><div class="bigNumber"><?php print($articleNumber); ?></div></td>
	  <td class="leftAlign">
	    <table>
	      <tr><td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),60); ?></td></tr>
	      <tr><td class="leftAlign"><em><?php Tools::printHTMLsubstr("Authors: " . $article->getAuthors(),60); ?></em></td></tr>
              <tr><td class="leftAlign"><?php $article->printLastModificationDateOnly($currentReviewer);?></td></tr>
	    </table>
	  </td>
	  <?php 
	  $acceptance = $article->getAcceptance();
          if($acceptance == Article::$ACCEPT) {
	    print('<td><div class="bigNumber acceptArticle">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber acceptArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$MAYBE_ACCEPT) {
	    print('<td><div class="bigNumber maybeAcceptArticle">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber maybeAcceptArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$DISCUSS) {
	    print('<td><div class="bigNumber discussArticle">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber discussArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$MAYBE_REJECT) {
	    print('<td><div class="bigNumber maybeRejectArticle">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber maybeRejectArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$REJECT) {
	    print('<td><div class="bigNumber rejectArticle">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber rejectArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$CUSTOM1) {
	    print('<td><div class="bigNumber customAccept1">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber customAccept1">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$CUSTOM2) {
	    print('<td><div class="bigNumber customAccept2">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber customAccept2">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$CUSTOM3) {
	    print('<td><div class="bigNumber customAccept3">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber customAccept3">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$CUSTOM4) {
	    print('<td><div class="bigNumber customAccept4">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber customAccept4">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$CUSTOM5) {
	    print('<td><div class="bigNumber customAccept5">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber customAccept5">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$CUSTOM6) {
	    print('<td><div class="bigNumber customAccept6">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber customAccept6">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else if($acceptance == Article::$CUSTOM7) {
	    print('<td><div class="bigNumber customAccept7">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber customAccept7">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  } else {
	    print('<td><div class="bigNumber voidArticle">' . $article->roundAverageOverallGradeSnapshot(2) . '</div></td>');
	    print('<td><div class="bigNumber voidArticle">' . $article->roundAverageOverallGrade(2) . '</div></td>');
	  }
          ?>
	  <td><?php 
	    $article->printCompletedReviewsRatio();
	  ?></td>
	  <td><?php $article->printAcceptanceSelect(); ?></td>
	</tr>
      <?php } ?>
    <?php }


  if($viewType == "simpleView") {
    print('</table>');
  } ?>
  <input type="hidden" name="previousPage" value=<?php print($pageToDisplay); ?> />
  <input type="hidden" name="date" value=<?php print(gmdate("U")); ?> />	  
  <input type="submit" class="buttonLink bigButton" name="action" value="Update Acceptances" />
  <input type="submit" class="buttonLink bigButton" name="action" value="Update Acceptances and Set Visit Date" />
  </form>
  </center>
  <form action="accept_and_reject.php" method="post">
    <input type="hidden" name="previousPage" value=<?php print($pageToDisplay); ?> />
    <div class="floatRight">
	<input type="submit" class="buttonLink" name="action" value="Take a Snapshot of Current Averages" />
    </div>
  </form>  

  <div class="bottomSpacer"></div>

<?php } ?>

<?php include('footer.php'); ?>
