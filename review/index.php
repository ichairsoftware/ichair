<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Main Page';
include('../utils/tools.php');
include('header.php');
if(is_null($currentReviewer)) {
print('<div class="ERRmessage">It seems that your account has not been configured yet. Please contact the iChair admin and ask him to do so.</div>');
}

if(!is_null($currentReviewer)) {

  Log::logAccessIndex($currentReviewer);

?>

<p>Welcome to the iChair Submission &amp; Review Software! This page is the
main page for the reviewers of <a href="<?php print(Tools::getConfig("conference/site")) ?>"><?php Tools::printHTML(Tools::getConfig('conference/name'));?></a>. 

<?php if(Tools::getCurrentPhase() != Tools::$DECISION) { ?>

<?php if($revHTML) { ?>

  It is important that you read the <a href="guidelines/guidelines.html" target="_blank">Review Guidelines</a>

  <?php if($revPDF && $revTXT) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a> and <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } else if($revPDF) { ?>
    (also available in <a href="guidelines/guidelines.pdf" target="_blank">[pdf]</a>)
  <?php } else if($revTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>

  before starting your reviews.

<?php } else if($revPDF) { ?>

  It is important that you read the <a href="guidelines/guidelines.pdf" target="_blank">Review Guidelines</a>

  <?php if($revTXT) { ?>
    (also available in <a href="guidelines/guidelines.txt" target="_blank">[text]</a>)
  <?php } ?>

  before starting your reviews.

<?php } else if($revTXT) { ?>

  It is important that you read the <a href="guidelines/guidelines.txt" target="_blank">Review Guidelines</a>
  before starting your reviews.

<?php } ?>
<?php } ?>


</p>


<?php 

  if($currentReviewerGroup != Reviewer::$OBSERVER_GROUP) {

    if(Tools::getCurrentPhase() == Tools::$SUBMISSION) {

      print('<p>We currently are in the <em>Submission/Transfer/Authorizations</em> phase. During this time, you can update your personal profile.</p>');
      if (Tools::useCategory()) {
	print('<p> While we are waiting for submissions, you can update your personal profile and choose which categories of articles you would prefer to review. Note that this choice will however have less influence than the individual article selection you will have the chance to make later on.</p>');
      } else {
	print('<p> While we are waiting for submissions, you can update your personal profile.</p>');
      }

    } else if(Tools::getCurrentPhase() == Tools::$ELECTION) {
      print('<p>We currently are in the <em>Preferred Articles Election</em> phase.</p>');
      if(Tools::useAbstract()) {
	print('<div class="floatRight"><form action="download_all_abstracts.php" method="post" />');
	print('<input type="submit" class="buttonLink" value="Download all article abstracts" />');
	print('</form></div>');
      }
      if(Tools::getConfig('review/zipForPreferred') && file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <div class="floatRight"><form action="download_all_zip.php" method="post">
           <input type="submit" class="buttonLink" value="Download a ZIP of all articles"/>
        </form></div>
      <?php }  
      print('<div class="clear"></div><center>');
      $currentReviewer->printAffinityTotalsTable();
      print('</center>');

    } else if(Tools::getCurrentPhase() == Tools::$ASSIGNATION) {
      if(file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <div class="floatRight"><form action="download_all_zip.php" method="post">
          <input type="submit" class="buttonLink" value="Download a ZIP of all articles"/>
        </form></div>
   <?php }
      print('<p>We currently are in the <em>Assignations</em> phase. For the moment the program chair is deciding which articles will be assigned to which reviewer.</p>');
      print('<div class="clear"></div>');
      $currentReviewer->printAffinityTotalsTable();

    } else if(Tools::getCurrentPhase() == Tools::$REVIEW) {
      if(file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <div class="floatRight"><form action="download_all_zip.php" method="post">
          <input type="submit" class="buttonLink" value="Download a ZIP of all articles"/>
        </form></div>
   <?php }
      if(!$currentReviewer->isAllowedToViewOtherReviews()) {
	print('<p>You currently are in the <em>Review</em> phase.</p>');
      } else {
	print('<p>You currently are in the <em>Review/Discussion/Decision</em> phase.</p>');
      }
      print('<div class="clear"></div>');
      print('<center>');
      $currentReviewer->printReviewTotalsTable();
      if($currentReviewer->isAllowedToViewOtherReviews()) {
        Article::printAuthorizedAcceptanceSummaryTable($currentReviewer);
      }
      print('</center>');
    } else if(Tools::getCurrentPhase() == Tools::$DECISION) {

      print('<p>We currently are in the <em>Decision</em> phase. During this time, the program chair is alone to choose the fate of the articles. You can no longer modify your reviews or comment on the articles.</p>');
      Article::printAcceptanceFinalResults();
    }

  } else {   

   if(Tools::getCurrentPhase() == Tools::$SUBMISSION) {
      print('<p>We currently are in the <em>Submission/Transfer/Authorizations</em> phase. While we are waiting for submissions, you can update your personal profile.</p>');

    } else if(Tools::getCurrentPhase() == Tools::$ELECTION) {
      print('<p>We currently are in the <em>Preferred Articles Election</em> phase.</p>');

    } else if(Tools::getCurrentPhase() == Tools::$ASSIGNATION) {
      if(file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <div class="floatRight"><form action="download_all_zip.php" method="post">
          <input type="submit" class="buttonLink" value="Download a ZIP of all articles"/>
        </form></div>
   <?php }
      print('<p>We currently are in the <em>Assignations</em> phase. For the moment the program chair is deciding which articles will be assigned to which reviewer.</p>');
      print('<div class="clear"></div>');

    } else if(Tools::getCurrentPhase() == Tools::$REVIEW) {
      if(file_exists(Tools::getConfig('server/reviewsPath').'all_articles.zip')) {?>
        <div class="floatRight"><form action="download_all_zip.php" method="post">
          <input type="submit" class="buttonLink" value="Download a ZIP of all articles"/>
        </form></div>
   <?php }
      print('<p>You currently are in the <em>Review/Discussion/Decision</em> phase.</p>');
      print('<div class="clear"></div>');
    } else if(Tools::getCurrentPhase() == Tools::$DECISION) {

      print('<p>We currently are in the <em>Decision</em> phase. During this time, the program chair is alone to choose the fate of the articles. You can no longer modify your reviews or comment on the articles.</p>');
      Article::printAcceptanceFinalResults();
    }
   }

?>

<?php if(Tools::getCurrentPhase() != Tools::$DECISION) { ?>

<h2><a href="meeting_show.php">Messages from Chair</a></h2>

<p>Check the <a href="meeting_show.php">Messages from Chair</a>  page regularly to consult comments published by the program chair either of general order or specific to an article.</p>

<h2><a href="profile.php">Personal Profile</a></h2>

<p>Use <a href="profile.php">Personal Profile</a> to make modification
to your full name, e-mail address, or to change your password.
</p>

<?php } ?>
<?php if(Tools::getCurrentPhase() == Tools::$ELECTION) { ?>

<h2><a href="affinity_list.php">Preferred Articles</a></h2>

<?php if ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP) { ?>

<p>Use <a href="affinity_list.php">Preferred Articles</a> to indicate
to the chair the paper you would like to review, those you would
rather not have to review, and those you just cannot review. For each
article, click on <em>Details</em> to get all the details about a
specific submissions and to download the corresponding file. 
</p>

<p>
Note that no change is taken into account until you click on the
<em>Update Preferences</em> button.
</p>

<?php } else { ?>

<p>Active reviewers can use the  <a href="affinity_list.php">Preferred Articles</a>
page to indicate to the chair the paper they would like to review.
As a &quot;neutral observer&quot;, you do not have the possibility to save
your choices.</p>

<?php } ?>

<?php } ?>

<?php if((Tools::getCurrentPhase() == Tools::$REVIEW) && ($currentReviewerGroup != Reviewer::$OBSERVER_GROUP)) { ?>

  <h2><a href="review.php">Your Reviews</a></h2>

  <p> Use <a href="review.php">Your Reviews</a> to see your assigned reviews
    and to submit reviews. Click on <em>Details</em> to obtain some
    additional information about a specific article. To submit your review,
    click on <em>Review Article</em> and fill the corresponding form. Don't
    forget to save your changes by clicking on the <em>Update Review</em>
    button. Reviews can remain invisble by checking a report as <em>In Progress</em>.
  </p>

  <p> If you do not want to fill the review form online, you also have
  the possibility to download an XML file corresponding to a given
  article, fill it in offline and then upload it later to validate
  your review.
  </p>

<?php } ?>

<?php if((Tools::getCurrentPhase() == Tools::$REVIEW) && ($currentReviewer->isAllowedToViewOtherReviews()) ) { ?>

  <h2><a href="discuss.php">Reviews &amp; Discussions</a></h2>

  <p> Use <a href="discuss.php">Reviews &amp; Discussions</a> to consult the
    reviews and discussions made by all reviewers. For each article, you can
    click on <em>Details</em> to get more information about a specific
    review. <em>Reviews &amp; Comments</em> give access to the discussion
    about the article and to a complete list of reviews about this article.
  </p>

<?php } ?>

<?php } ?>
<?php include('w3c.php'); ?>
<?php include('footer.php'); ?>
