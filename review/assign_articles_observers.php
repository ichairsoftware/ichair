<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include '../utils/tools.php';
$reviewerNumber = Tools::readPost('reviewerNumber');
/* Do not print the reviewer name here, as we cannot test if the currentReviewerGroup is CHAIR_GROUP or not */
$page_title='Assign Articles to a Reviewer';

include 'header.php';

if($currentReviewerGroup == Reviewer::$CHAIR_GROUP) {

  $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
  $nextReviewerNumber = $reviewer->getNextPassiveReviewer();

  /* processing the POST variable */

  $reviewerToUpdate = Tools::readPost('reviewerToUpdate');
  /* If there is a reviewer to update, update each assignement */
  if($reviewerToUpdate != "") {
    $articleNumbers = Article::getAllArticleNumbers();
    foreach($articleNumbers as $articleNumber) {
      $assignementStatus = Tools::readPost("" . $articleNumber);
      if($assignementStatus != "") {
	/* I there no assignement yet, create it */
	$assignement = Assignement::getByNumbers($articleNumber, $reviewerToUpdate);
	if(is_null($assignement)) {
	  $assignement = new Assignement();
	  $assignement->createFromScratch($articleNumber, $reviewerToUpdate);
	}
	$assignement->setAssignementStatus($assignementStatus);
	Log::logAssignementReviewer($reviewer, $currentReviewer);
      }
    }
    $reviewerToUpdateName = Reviewer::getByReviewerNumber($reviewerToUpdate)->getFullName();
    ?>
    <div class="OKmessage">Permissions for Obesrver <?php Tools::printHTML($reviewerToUpdateName); ?> have been saved successfully.<?php 
  }
  /* If we just updated the last reviewer... */

  if (($reviewerToUpdate != "") && (Tools::readPost('buttonName') == "Save Changes for this Last Observer")) {?>
    <br/>This was the last observer. You can go back to the list.</div>
    <div class="floatRight">
       <form action="assign_articles_list.php" method="post">
         <input type="submit" class="buttonLink bigButton" value="Go Back to the List"/>
       </form>
    </div><?php 
  } else {
    if ($reviewerToUpdate != "") {
      print('</div>');
    }
    /* Display a white box with all the informations about the reviewer to whom the chair is assigning papers */
    ?>

    <div class="paperBox">
      <div class="paperBoxDetails">
        <?php $reviewer->printLong(); ?>
      </div>
    </div>
 
    <?php /* Displaying information */ ?>

    <center>

 <form action="assign_articles_list.php#nbr<?php print($reviewerNumber);?>" method="post">
       <input type="submit" class="buttonLink bigButton" value="Go Back to the List" />
 </form>

      <form action="assign_articles_observers.php" method="post">
        <input type="hidden" name="reviewerToUpdate" value="<?php print($reviewerNumber);?>" />

	<?php 
        $assignedArticles = $reviewer->getArticlesByStatus(ASSIGNEMENT::$ASSIGNED);
        $authorizedArticles = $reviewer->getArticlesByStatus(ASSIGNEMENT::$AUTHORIZED);
	$blockedArticles = $reviewer->getArticlesByStatus(ASSIGNEMENT::$BLOCKED);
        ?>

	<table class="usersTable">
	  <tr>
	    <th colspan="3">&nbsp;</th>
	  <?php if (count($assignedArticles) != 0){?>
	    <th colspan="3">Status</th>
          <?php } else {?>
	    <th colspan="2">Status</th>
          <?php } ?>
	  </tr>
          <tr>
	    <th>Article<br/>Number</th>
	    <th>&nbsp;</th>
	    <th class="leftAlign">
	      <table>
	        <tr><th class="leftAlign">Title</th></tr>
	        <tr><th class="leftAlign"><em>Authors</em></th></tr>
	      </table>
	    </th>
	  <?php if (count($assignedArticles) != 0){?>
            <th><div class="bigNumber completedReview"><?php 
		print(count($assignedArticles));
	        if ($reviewer->getNumberOfArticles() != "") {
		  print('/'.$reviewer->getNumberOfArticles());
		}?></div></th>
          <?php } ?>
            <th><div class="bigNumber inProgressReview"><?php print(count($authorizedArticles)); ?></div></th>
            <th><div class="bigNumber voidReview"><?php print(count($blockedArticles)); ?></div></th>
          </tr>
	  <?php 
	  foreach($assignedArticles as $articleNumber) {
	    $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	    $article = Article::getByArticleNumber($articleNumber);
	    ?> 
	    <tr>
            <td><div class="bigNumber"><?php print($article->getArticleNumber());?></td>
	    <td class="button">
	    <div class="popUp">
	      <a href="article_details_chair.php?articleNumber=<?php print($articleNumber); ?>" target="_blank">Details</a>
	      <div class="hidden">
	        <?php $article->printDetailsBoxForChairPopUp();?>
	      </div>
	    </div>
	    </td>
            <td class="leftAlign">
	      <table>
	        <tr><td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),60) ?></td></tr>
	        <tr><td class="leftAlign"><em><?php Tools::printHTMLsubstr("Authors: ".$article->getAuthors(),60) ?></em></td></tr>
	      </table>
	    </td>
            <?php 
	    print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="assign' .$articleNumber .'" value="'. Assignement::$ASSIGNED .'" checked="checked" /><label for="assign' .$articleNumber ."\">Assigned</label></td>\n");
	    print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="authorize' . $articleNumber .'" value="'. Assignement::$AUTHORIZED .'" /><label for="authorize' . $articleNumber . "\">Authorized</label></td>\n");
	    print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="block' . $articleNumber .'" value="'. Assignement::$BLOCKED .'" /><label for="block' . $articleNumber . "\">Blocked</label></td>\n");
	  }
	foreach($authorizedArticles as $articleNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $article = Article::getByArticleNumber($articleNumber);
	    ?> 
	    <tr>
	    <td><div class="bigNumber"><?php print($article->getArticleNumber());?></td>
	    <td class="button">
	    <div class="popUp">
	      <a href="article_details_chair.php?articleNumber=<?php print($articleNumber); ?>" target="_blank">Details</a>
	      <div class="hidden">
	        <?php $article->printDetailsBoxForChairPopUp();?>
	      </div>
	    </div>
	    </td>
            <td class="leftAlign">
	      <table>
	        <tr><td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),60) ?></td></tr>
	        <tr><td class="leftAlign"><em><?php Tools::printHTMLsubstr("Authors: ".$article->getAuthors(),60) ?></em></td></tr>
	      </table>
	    </td>
            <?php 
	    if (count($assignedArticles) != 0){
	      print('<td>&nbsp;</td>');
            }
	    print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="authorize' . $articleNumber .'" value="'. Assignement::$AUTHORIZED .'" checked="checked" /><label for="authorize' . $articleNumber . "\">Authorized</label></td>\n");
	    print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="block' . $articleNumber .'" value="'. Assignement::$BLOCKED .'" /><label for="block' . $articleNumber . "\">Blocked</label></td>\n");
	}
	foreach($blockedArticles as $articleNumber) {
	  $assignement = Assignement::getByNumbers($articleNumber, $reviewerNumber);
	  $article = Article::getByArticleNumber($articleNumber);
	    ?> 
	    <tr>
	    <td><div class="bigNumber"><?php print($article->getArticleNumber());?></td>
	    <td class="button">
	    <div class="popUp">
	      <a href="article_details_chair.php?articleNumber=<?php print($articleNumber); ?>" target="_blank">Details</a>
	      <div class="hidden">
	        <?php $article->printDetailsBoxForChairPopUp();?>
	      </div>
	    </div>
	    </td>
            <td class="leftAlign">
	      <table>
	        <tr><td class="leftAlign"><?php Tools::printHTMLsubstr($article->getTitle(),60) ?></td></tr>
	        <tr><td class="leftAlign"><em><?php Tools::printHTMLsubstr("Authors: ".$article->getAuthors(),60) ?></em></td></tr>
	      </table>
	    </td>
            <?php 
	    if (count($assignedArticles) != 0){
	      print('<td>&nbsp;</td>');
            }
	    print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="authorize' . $articleNumber .'" value="'. Assignement::$AUTHORIZED .'" /><label for="authorize' . $articleNumber . "\">Authorized</label></td>\n");
	    print('<td><input class="noBorder" type="radio" name="' . $articleNumber . '" id="block' . $articleNumber .'" value="'. Assignement::$BLOCKED .'" checked="checked" /><label for="block' . $articleNumber . "\">Blocked</label></td>\n");
	}
	?>
      </table>   
      
     

   <?php  if (!is_null($nextReviewerNumber)) {  ?>
      <input type="hidden" name="reviewerNumber" value="<?php print($nextReviewerNumber);?>" />
      <input type="submit" name="buttonName" class="buttonLink bigButton" value="Save Changes and Go To Next Observer" />
   <?php } else { ?>
      <input type="hidden" name="reviewerNumber" value="<?php print($reviewerNumber);?>" />
      <input type="submit" name="buttonName" class="buttonLink bigButton" value="Save Changes for this Last Observer" />
   <?php  }  ?>
  
    </form>
  </center>

<div class="clear bottomSpacer"></div>

<?php } }?>

<?php include('footer.php'); ?>
