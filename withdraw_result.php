<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Withdrawal Result';
include 'utils/tools.php';
if(!Tools::isConfigured()) {return;}
include 'header.php';

/* Check that the author is still on time... */

if(Tools::serverIsShutdown()) {
  Tools::printServerShutdownMessage();
} else {

/* Create a new object submission */
$id = Tools::readPost('id');
$submission = new Submission();
$submission = Submission::getByID($id);

/* Set the withdrawn flag to true, send an email, and print a message*/
Log::logWithdrawal($submission); 
$submission->setWithdrawn();
$submission->sendWithdrawalMail(); 
?>
<div class="OKmessage">
Your file was withdrawn successfully. This information was also emailed to the contact author.
</div>
<?php 
}
?>

</body>
</html>

