<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
if(file_exists("header_custom.php")) {
  include("header_custom.php");
} else {
header("Content-type: text/html; charset=iso-8859-1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=iso-8859-1" />
  <title><?php print(htmlentities(Tools::getConfig("conference/name"), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')." - ".$page_title); ?></title>
  <link href="css/main_<?php print(Tools::getConfig("miscellaneous/submissionSkin"));?>.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="css/print_<?php print(Tools::getConfig("miscellaneous/submissionSkin"));?>.css" rel="stylesheet" type="text/css" media="print" />
  <link rel="SHORTCUT ICON" type="image/png" href="images/icon.png" />
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/authors.js"></script>  
</head>
<body>

<?php 

$cfpHTML = Tools::hasCallForPaperHTML(".");
$cfpPDF  = Tools::hasCallForPaperPDF(".");
$cfpTXT  = Tools::hasCallForPaperTXT(".");

$subHTML = Tools::hasSubmissionGuidelinesHTML(".");
$subPDF  = Tools::hasSubmissionGuidelinesPDF(".");
$subTXT  = Tools::hasSubmissionGuidelinesTXT(".");

?>


<div class="navigate">

  <div class="navigateTitle">
    <a href="<?php print(Tools::getConfig("conference/site")) ?>"><?php Tools::printHTML(Tools::getConfig("conference/name")) ?></a>
  </div>
  <div class="navigateLinks">
    <p><a href="index.php">Main Page</a></p>

    <?php if($cfpHTML) { ?>
      <p class="guidelines"><a class="guidelines" href="cfp/cfp.html" target="_blank">Call for Papers</a>
      <?php if($cfpPDF) { ?>
        <a class="noblock" href="cfp/cfp.pdf" target="_blank">[pdf]</a>
      <?php } 
         if($cfpTXT) { ?>
        <a class="noblock" href="cfp/cfp.txt" target="_blank">[text]</a>
      <?php } ?>
      </p>
    <?php } else if($cfpPDF) { ?>
      <p class="guidelines"><a class="guidelines" href="cfp/cfp.pdf" target="_blank">Call for Papers</a>
      <?php if($cfpTXT) { ?>
        <a class="noblock" href="cfp/cfp.txt" target="_blank">[text]</a>
      <?php } ?>
      </p>
    <?php } else if($cfpTXT) { ?>
      <p><a href="cfp/cfp.txt" target="_blank">Call for Papers</a></p>
    <?php } ?>

    <?php if($subHTML) { ?>
      <p class="guidelines"><a class="guidelines" href="guidelines/guidelines.html" target="_blank">Guidelines</a>
      <?php if($subPDF) { ?>
        <a class="noblock" href="guidelines/guidelines.pdf" target="_blank">[pdf]</a>
      <?php } 
         if($subTXT) { ?>
        <a class="noblock" href="guidelines/guidelines.txt" target="_blank">[text]</a>
      <?php } ?>
      </p>
    <?php } else if($subPDF) { ?>
      <p class="guidelines"><a class="guidelines" href="guidelines/guidelines.pdf" target="_blank">Guidelines</a>
      <?php if($subTXT) { ?>
        <a class="noblock" href="guidelines/guidelines.txt" target="_blank">[text]</a>
      <?php } ?>
      </p>
    <?php } else if($subTXT) { ?>
      <p><a href="guidelines/guidelines.txt" target="_blank">Guidelines</a></p>
    <?php } ?>

    <p class="topLine"><a href="submit.php">Submission Form</a></p>
    <p><a href="revise.php">Revision Form</a></p>
    <p><a href="withdraw.php">Withdrawal Form</a></p>
    <p class="topLine"><a href="about.php">About iChair</a></p>
  </div>

  <div class="navigateTitle">
    <a href="http://www.time.gov/timezone.cgi?UTC/s/0/java" target="_new">Current Time</a>
  </div>
  <div class="navigateTimes">
    <div class="timeTitleTop">UTC Time</div>
    <?php if(Tools::use12HourFormat()) { ?>
      <div class="time"><?php print(gmdate("j M y - h:i&#160;a")); ?></div>
    <?php } else {?>
      <div class="time"><?php print(gmdate("j M y - H:i")); ?></div>      
    <?php } ?>
  </div>

  <div class="navigateTitle">
    Submission deadline
  </div>
  <div class="navigateTimes">
    <div class="timeTitleTop">UTC Time</div>
    <div class="time">
      <?php 
         $U = Tools::getUFromDateAndTime(Tools::getConfig("server/deadlineDate"), Tools::getConfig("server/deadlineTime"));
	  if(Tools::use12HourFormat()) {
	    print(gmdate("j M y - h:i&#160;a", $U)); 
	  } else {
	    print(gmdate("j M y - H:i", $U)); 
	  }
      ?>
    </div>
    <div class="timeTitle">Time left</div>
    <div class="time">
    <?php 
    $timeLeft = $U - gmdate("U");
    if($timeLeft > 0) {
      print(Tools::stringTimeLeft($timeLeft));
    } else {
      print("<div class=\"timeUp\">Time is up!</div>");
    }
    ?>
    </div>
  </div>

  <div class="navigateImages">
  <?php 
  Tools::printLogos();
  ?>
  </div>

</div>

<h1><?php print(htmlentities(Tools::getConfig("conference/name"), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')." - ".$page_title); ?></h1>
<?php 
}
?>
