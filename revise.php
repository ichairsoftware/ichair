<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Revision ID Check';
include 'utils/tools.php';
if(!Tools::isConfigured()) {return;}
include 'header.php';

/* Check that the author is still on time... */

if(Tools::serverIsShutdown()) {
  Tools::printServerShutdownMessage();
} else {

?>

Please type in the ID of the paper you want to revise (this is not the MD5 hash of your file). This ID was emailed to the contact author when the paper was first submitted.

<form action="revise_form.php" method="post" enctype="multipart/form-data">  
<center> ID:&nbsp;<input class="id" name="id" type="text" size=35 /><br>
<input type="submit" class="buttonLink bigButton" value="Proceed to Revision Form" />
</center>
</form>

<?php 
}
?>

<?php include("footer.php"); ?>
