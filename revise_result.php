<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Revision Result';
include 'utils/tools.php';
if(!Tools::isConfigured()) {return;}
include 'header.php';

/* Check that the author is still on time... */

if(Tools::serverIsShutdown()) {

    Tools::printServerShutdownMessage();

} else {

    /* From the id passed in parameter, construct a submission object */
    $id = $_GET['id'];
    if(Tools::isAnId($id)) {
	
	$submission = Submission::getByID($id);
	if(is_null($submission)) {

	    /* If the id is wrong, it probably means that the user tried one at random -> make him wait... */
	    sleep(5);
	    print('<div class="ERRmessage">We could not find your article in our database. Please contact <em>'.
		  htmlentities(Tools::getConfig('mail/admin'), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') .
		  '</em></div>');

	} else {

	    Tools::printHTMLbr("Your submission was revised successfully. The information below was also emailed to the contact author.");
	    print('<div class="OKmessage">');
	    $submission->printInfo();				     
	    $submission->printLastVersionBr();
	    ?>

	    </div>

	    <?php if(Tools::usePreview()) {?>
	      <h2>Submission preview</h2>
	      Below is a preview of the first page of the document you uploaded. Please be sure to also check the MD5 of your file.
	      <div class="previewImage"><img src="imgpreview.php?id=<?php print $submission->getId() ?>&version=<?php print $submission->getLastVersionNumber()?>" /></div>
	    <?php }?>

	    <?php   
        } 
    }
}

?>
</body>
</html>

