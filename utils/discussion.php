<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 

class Discussion {

  function __construct() {}

  static function openDB() {
    $dbFile = Tools::getConfig('server/reviewsPath') . "discussions.db";
    if (!file_exists($dbFile)) {
      Discussion::createDB();
    }
    return new SQLite3($dbFile);
  }

  static function createDB() {
    $db = new SQLite3(Tools::getConfig('server/reviewsPath') . "discussions.db");
    $db->exec("CREATE TABLE discussions( articleNumber, reviewerName, date, comment )");
  }

  static function addComment($articleNumber, $reviewerNumber, $comment) {
    $reviewer = Reviewer::getByReviewerNumber($reviewerNumber);
    $date = gmdate("U");
    $db = Discussion::openDB();
    $db->exec('INSERT INTO discussions VALUES ("'
	       . $articleNumber . '","'
	       . base64_encode($reviewer->getFullName()) . '","'
	       . $date . '","'
	       . base64_encode($comment) . '")');
  }

  static function getQueryResultByArticleNumber($articleNumber) {
    $db = Discussion::openDB();
    return $db->query('SELECT * FROM discussions WHERE articleNumber="'. $articleNumber .'" ORDER BY date');
  }


  /*
   * Print functions
   */

  static function printDiscussionElement($db_row, $firstNotSeenAlreadySet, $date) {
    if((!$firstNotSeenAlreadySet) && ($date < $db_row['date'])) {
      $firstNotSeenAlreadySet = true;
      print('<div class="paperBox" id="firstNotSeen">');
    } else {
      print('<div class="paperBox">');
    }
    print('<div class="paperBoxTitle">');
    if(Tools::use12HourFormat()) {
      print('<div class="floatRight">Date: ' . gmdate("D, j M Y h:i a", $db_row['date']) . '</div>');
    } else {
      print('<div class="floatRight">Date: ' . gmdate("D, j M Y H:i", $db_row['date']) . '</div>');
    }
    print('Comment by ' . base64_decode($db_row['reviewerName']));
    print('</div>');
    if($firstNotSeenAlreadySet) {
      print('<div class="paperBoxDetails newComment">');
    } else {
      print('<div class="paperBoxDetails">');
    }
    Tools::printHTMLbr(base64_decode($db_row['comment']));
    print('</div>');
    print('</div>');
    return $firstNotSeenAlreadySet;
  }


  
}

?>
