<xsl:stylesheet version = '1.0'
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:template match="/">
  <form action="configuration_result.php" method="post">
    <h2>Current Phase Selection</h2>
    <xsl:apply-templates select="xml/phase"/>
    <h2>Chair Menu</h2>
    <xsl:apply-templates select="xml/restrictedChairMenu"/>
    <br />
    <center>
      <input type="submit" class="buttonLink bigButton" value="Update Configuration File"/>
    </center>
  </form>
</xsl:template>

<xsl:template match="phase">
  <table>

    <tr>
      <td>
        <input type="radio" class="noBorder" name="phase" value="submission" id="submissionPhase">
	  <xsl:if test="current='submission'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
	  </xsl:if>
	</input>
      </td>
      <td>
        <label for="submissionPhase"><em>Submission/Transfer/Authorizations</em></label>
      </td>
      </tr>
      <tr>
        <td></td>
        <td>
	  This phase should be active during the submission process and
	  during the time when the chair transfers the submissions (i.e.,
	  creates the articles database) using <a
	  href="transfer.php">Load Submissions for Review</a>. It is also in
	  this phase that the chair should select which articles will be
	  accessible to which reviewer (i.e. block a reviewer from reviewing
	  his own article), using either <a
	  href="assign_reviewers_list.php">Assign Reviewers to Articles</a>
	  or <a href="assign_articles_list.php"> Assign Articles to
	  Reviewer</a>. During this phase, the reviewers only have access to
	  their <a href="profile.php">Personal Profile</a>.
        </td>
      </tr>

      <tr>
      <td>
        <input type="radio" class="noBorder" name="phase" value="election" id="electionPhase">
	  <xsl:if test="current='election'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
	  </xsl:if>
	</input>
      </td>
      <td>
        <label for="electionPhase"><em>Preferred Articles Election</em></label>
      </td>
      </tr>
      <tr>
      <td></td>
      <td>
        This phase should be active during the time when the reviewers choose which articles they would prefer to review, 
	which articles they would rather not review, and which articles they just cannot/don't want to review. This is 
	done using <a href="affinity_list.php">Preferred Articles</a>. At the time this phase starts, the chair can notify 
	all the reviewers by using <a href="mass_mailing.php">Mass Mailing</a>. When all the reviewers have chosen their 
	preferred articles (or when the chair considers that they had enough time to do so), the next phase can start.
      </td>
      </tr>
      <tr>
      <td>
        <input type="radio" class="noBorder" name="phase" value="assignation" id="assignationPhase">
	  <xsl:if test="current='assignation'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
	  </xsl:if>
	</input>
      </td>
      <td>
        <label for="assignationPhase"><em>Assignations</em></label>
      </td>
      </tr>
      <tr>
      <td></td>
      <td>
        This phase should be active during the time when the chair assigns articles to each reviewer. This can be done by using either 
	<a href="assign_reviewers_list.php">Assign Reviewers to Articles</a>, <a href="assign_articles_list.php">
	Assign Articles to Reviewers</a>, or <a href="automatic_assignation.php">Automatic Assignation</a>. During this time, 
	the reviewers only have access to their <a href="profile.php">Personal Profile</a>. Once each articles has been assigned enough reviewers, 
	the next phase can start.
      </td>
      </tr>

      <tr>
      <td>
        <input type="radio" class="noBorder" name="phase" value="review" id="reviewPhase">
	  <xsl:if test="current='review'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
	  </xsl:if>
	</input>
      </td>
      <td>
        <label for="reviewPhase"><em>Review/Discussion/Decision</em></label>
      </td>
      </tr>
      <tr>
      <td></td>
      <td>
        This phase should be active during the time when the reviewers
	review the articles assigned to them, using <a
	href="review.php">Your Reviews</a> and discuss about these reviews,
	using <a href="discuss.php">Reviews &amp; Discussions</a>. At the
	beginning of this phase, none of the reviewers have access to the
	reviews of other reviewers, nor to the discussion section. The chair
	can grant access to these using the <a
	href="supervise_reviewers.php">Reviewers Access Privileges</a> menu. At the
	end of this phase, using the <a href="accept_and_reject.php">Accept
	&amp; Reject</a> page, the chair shall decide which articles are
	accepted to the conference.<br />
        <input type="checkbox" class="noBorder" name="hideTime" value="yes" id="hideTime">
          <xsl:if test="hideTime='yes'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
	<label for="hideTime">Hide the <em>Review Deadline</em> time box.</label>
      </td>
    </tr>  

      <tr>
      <td>
        <input type="radio" class="noBorder" name="phase" value="decision" id="decisionPhase">
          <xsl:if test="current='decision'">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
      </td>
      <td>
        <label for="decisionPhase"><em>Decision only</em></label>
      </td>
      </tr>
      <tr>
      <td></td> 
      <td> 
	During this final phase the reviewers can no longer modify their
        reviews or continue discussing on the articles. The chair is left
        alone to decide the final fate of the articles. Using the <a
        href="accept_and_reject.php">Accept &amp; Reject</a> page should be
        enough then.
     </td>
    </tr>  
  </table>
</xsl:template>

<xsl:template match="restrictedChairMenu">
  <table>
    <tr>
      <td>
        <label for="restrictMenu">Restrict the items visibility in the <em>Chair Menu</em> to the ones useful in the current phase</label>
      </td>
      <td>
        <input type="checkbox" class="noBorder" name="restrictedChairMenu" value="yes" id="restrictMenu">
	  <xsl:if test=".='yes'">
	    <xsl:attribute name="checked">checked</xsl:attribute>
	  </xsl:if>
	</input>
      </td>
    </tr>
  </table>  
</xsl:template>

</xsl:stylesheet>

