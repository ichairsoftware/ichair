<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
include 'submission.php';
include 'reviewer.php';
include 'article.php';
include 'assignement.php';
include 'log.php';
include 'discussion.php';
include 'review_reader.php';
include 'meeting.php';
include 'mailTemplate.php';

class Tools {

    /* Min length of an Id in terms of characters. Each character is coded on 5 bits */
    public static $ID_LENGTH = 12;

  /* These are the four phases we can be in */
  /* Depending on the phase, some menu will be visible to reviewers, some won't */
  public static $SUBMISSION  = "submission";
  public static $ELECTION    = "election";
  public static $ASSIGNATION = "assignation";
  public static $REVIEW      = "review";  
  public static $DECISION    = "decision";

  public static $config; /* Initialized at the bottom of this file */
  public static $chairConfig; /* Initialized at the bottom of this file */

  /* Translation table 5 bits => [0-9a-v]. Used in the paper id computation. */

  public static $idTrans = array(
      "00000:" => "0", "00001:" => "1", "00010:" => "2", "00011:" => "3", "00100:" => "4", "00101:" => "5", "00110:" => "6", "00111:" => "7",
      "01000:" => "8", "01001:" => "9", "01010:" => "A", "01011:" => "B", "01100:" => "C", "01101:" => "D", "01110:" => "E", "01111:" => "F",
      "10000:" => "G", "10001:" => "H", "10010:" => "I", "10011:" => "J", "10100:" => "K", "10101:" => "L", "10110:" => "M", "10111:" => "N",
      "11000:" => "O", "11001:" => "P", "11010:" => "Q", "11011:" => "R", "11100:" => "S", "11101:" => "T", "11110:" => "U", "11111:" => "V"
      );

  /* continent prefixes: AF = Africa, AM = America, AS = Asia, EU = Europe, OC = Oceania, OT = other */

  public static $COUNTRY_LIST = array('' => 'Please select a country',
				      'AS_AF' => 'Afganistan',
				      'EU_AX' => 'Åland Islands',
				      'EU_AL' => 'Albania',
				      'AF_DZ' => 'Algeria',
				      'OC_AS' => 'American Samoa',
				      'EU_AD' => 'Andorra', 
				      'AF_AO' => 'Angola',
				      'AM_AI' => 'Anguilla',
				      'OT_AQ' => 'Antarctica',
				      'AM_AG' => 'Antigua and Barbuda', 
				      'AM_AR' => 'Argentina', 
				      'AS_AM' => 'Armenia', 
				      'AM_AW' => 'Aruba', 
				      'OC_AU' => 'Australia', 
				      'EU_AT' => 'Austria', 
				      'AS_AZ' => 'Azerbaijan',
				      'AM_BS' => 'Bahamas', 
				      'AS_BH' => 'Bahrain', 
				      'AS_BD' => 'Bangladesh',
				      'AM_BB' => 'Barbados',
				      'EU_BY' => 'Belarus', 
				      'EU_BE' => 'Belgium', 
				      'AM_BZ' => 'Belize',
				      'AF_BJ' => 'Benin', 
				      'AM_BM' => 'Bermuda', 
				      'AS_BT' => 'Bhutan',
				      'AM_BO' => 'Bolivia', 
				      'AM_BQ' => 'Bonaire, Sint Eustatius and Saba',
				      'EU_BA' => 'Bosnia and Herzegovina',
				      'AF_BW' => 'Botswana',
				      'OT_BV' => 'Bouvet Island', 
				      'AM_BR' => 'Brazil',
				      'AS_IO' => 'British Indian Ocean Territory',
				      'AS_BN' => 'Brunei', 
				      'EU_BG' => 'Bulgaria',
				      'AF_BF' => 'Burkina Faso',
				      'AF_BI' => 'Burundi', 
				      'AS_KH' => 'Cambodia',
				      'AF_CM' => 'Cameroon',
				      'AM_CA' => 'Canada',
				      'AF_CV' => 'Cape Verde',
				      'AM_KY' => 'Cayman Islands',
				      'AF_CF' => 'Central African Republic',
				      'AF_TD' => 'Chad',
				      'AM_CL' => 'Chile', 
				      'AS_CN' => 'China',
				      'AS_CX' => 'Christmas Island',    
				      'AS_CC' => 'Cocos (Keeling) Islands', 
				      'AM_CO' => 'Colombia',
				      'AF_KM' => 'Comoros', 
				      'AF_CG' => 'Congo', 
				      'AF_CD' => 'Congo, the Democratic Republic of the', 
				      'OC_CK' => 'Cook Islands',
				      'AM_CR' => 'Costa Rica',
				      'AF_CI' => 'Cote d\'Ivoire', 
				      'EU_HR' => 'Croatia (Hrvatska)',
				      'AM_CU' => 'Cuba',
				      'AS_CY' => 'Cyprus',
				      'EU_CZ' => 'Czech Republic',
				      'EU_DK' => 'Denmark', 
				      'AF_DJ' => 'Djibouti',
				      'AM_DM' => 'Dominica',
				      'AM_DO' => 'Dominican Republic',
				      'AS_TP' => 'East Timor',
				      'AM_EC' => 'Ecuador', 
				      'AF_EG' => 'Egypt', 
				      'AM_SV' => 'El Salvador', 
				      'AF_GQ' => 'Equatorial Guinea', 
				      'AF_ER' => 'Eritrea', 
				      'EU_EE' => 'Estonia', 
				      'AF_SZ' => 'Eswatini',
				      'AF_ET' => 'Ethiopia',
				      'AM_FK' => 'Falkland Islands (Malvinas)', 
				      'EU_FO' => 'Faroe Islands', 
				      'OC_FJ' => 'Fiji',
				      'EU_FI' => 'Finland',
				      'EU_FR' => 'France',
				      'AM_GF' => 'French Guiana', 
				      'OC_PF' => 'French Polynesia',
				      'OT_TF' => 'French Southern Territories', 
				      'AF_GA' => 'Gabon', 
				      'AF_GM' => 'Gambia',
				      'AS_GE' => 'Georgia', 
				      'EU_DE' => 'Germany', 
				      'AF_GH' => 'Ghana', 
				      'EU_GI' => 'Gibraltar', 
				      'EU_GR' => 'Greece',
				      'AM_GL' => 'Greenland', 
				      'AM_GD' => 'Grenada', 
				      'AM_GP' => 'Guadeloupe',
				      'OC_GU' => 'Guam',
				      'AM_GT' => 'Guatemala', 
				      'EU_GG' => 'Guernsey',
				      'AF_GN' => 'Guinea',
				      'AF_GW' => 'Guinea-Bissau', 
				      'AM_GY' => 'Guyana',
				      'AM_HT' => 'Haiti', 
				      'OT_HM' => 'Heard and Mc Donald Islands', 
				      'AM_HN' => 'Honduras',
				      'AS_HK' => 'Hong Kong', 
				      'EU_HU' => 'Hungary', 
				      'EU_IS' => 'Iceland', 
				      'AS_IN' => 'India', 
				      'AS_ID' => 'Indonesia', 
				      'AS_IR' => 'Iran (Islamic Republic of)',
				      'AS_IQ' => 'Iraq',
				      'EU_IE' => 'Ireland', 
				      'AS_IL' => 'Israel',
				      'EU_IT' => 'Italy', 
				      'AM_JM' => 'Jamaica', 
				      'AS_JP' => 'Japan',
				      'EU_JE' => 'Jersey',
				      'AS_JO' => 'Jordan',
				      'AS_KZ' => 'Kazakhstan',
				      'AF_KE' => 'Kenya', 
				      'OC_KI' => 'Kiribati',
				      'AS_KP' => 'Korea, Democratic People\'s Republic of',
				      'AS_KR' => 'Korea, Republic of',
				      'EU_KO' => 'Kosovo',
				      'AS_KW' => 'Kuwait',
				      'AS_KG' => 'Kyrgyzstan',
				      'AS_LA' => 'Laos',
				      'EU_LV' => 'Latvia',
				      'AS_LB' => 'Lebanon',
				      'AF_LS' => 'Lesotho', 
				      'AF_LR' => 'Liberia', 
				      'AF_LY' => 'Libya',
				      'EU_LI' => 'Liechtenstein', 
				      'EU_LT' => 'Lithuania',
				      'EU_LU' => 'Luxembourg',
				      'AS_MO' => 'Macau', 
				      'EU_MK' => 'Macedonia, The Former Yugoslav Republic of',
				      'AF_MG' => 'Madagascar',
				      'AF_MW' => 'Malawi',
				      'AS_MY' => 'Malaysia',
				      'AS_MV' => 'Maldives',
				      'AF_ML' => 'Mali',
				      'EU_MT' => 'Malta',
				      'OC_MH' => 'Marshall Islands',
				      'AM_MQ' => 'Martinique',
				      'AF_MR' => 'Mauritania',
				      'AF_MU' => 'Mauritius',
				      'AF_YT' => 'Mayotte', 
				      'AM_MX' => 'Mexico',
				      'OC_FM' => 'Micronesia, Federated States of',
				      'EU_MD' => 'Moldova, Republic of',
				      'EU_MC' => 'Monaco',
				      'AS_MN' => 'Mongolia',
				      'EU_MTG'=> 'Montenegro',
				      'AM_MS' => 'Montserrat',
				      'AF_MA' => 'Morocco',
				      'AF_MZ' => 'Mozambique',
				      'AS_MM' => 'Myanmar',
				      'AF_NA' => 'Namibia',
				      'OC_NR' => 'Nauru', 
				      'AS_NP' => 'Nepal', 
				      'EU_NL' => 'Netherlands',
				      'AM_AN' => 'Netherlands Antilles',
				      'OC_NC' => 'New Caledonia',
				      'OC_NZ' => 'New Zealand', 
				      'AM_NI' => 'Nicaragua', 
				      'AF_NE' => 'Niger', 
				      'AF_NG' => 'Nigeria', 
				      'OC_NU' => 'Niue',
				      'OC_NF' => 'Norfolk Island',
				      'OC_MP' => 'Northern Mariana Islands',
				      'EU_NO' => 'Norway',
				      'AS_OM' => 'Oman',
				      'AS_PK' => 'Pakistan',
				      'OC_PW' => 'Palau',
				      'AM_PA' => 'Panama',
				      'OC_PG' => 'Papua New Guinea',
				      'AM_PY' => 'Paraguay',
				      'AM_PE' => 'Peru',
				      'AS_PH' => 'Philippines',
				      'OC_PN' => 'Pitcairn Islands',
				      'EU_PL' => 'Poland',
				      'EU_PT' => 'Portugal',
				      'AM_PR' => 'Puerto Rico',
				      'AS_QA' => 'Qatar',
				      'AF_RE' => 'Reunion',
				      'EU_RO' => 'Romania',
				      'EU_RU' => 'Russian Federation',
				      'AF_RW' => 'Rwanda',
				      'AM_BL' => 'Saint Barthélemy',
				      'AF_SH' => 'Saint Helena, Ascension and Tristan da Cunha',
				      'AM_KN' => 'Saint Kitts and Nevis', 
				      'AM_LC' => 'Saint Lucia', 
				      'AM_MF' => 'Saint Martin',
				      'AM_PM' => 'Saint Pierre and Miquelon',
				      'AM_VC' => 'Saint Vincent and the Grenadines',
				      'OC_WS' => 'Samoa', 
				      'EU_SM' => 'San Marino',
				      'AF_ST' => 'Sao Tome and Principe',
				      'AS_SA' => 'Saudi Arabia',
				      'AF_SN' => 'Senegal',
				      'AF_SC' => 'Seychelles',
				      'EU_SE' => 'Serbia',
				      'AF_SL' => 'Sierra Leone',
				      'AS_SG' => 'Singapore', 
				      'EU_SK' => 'Slovakia (Slovak Republic)',
				      'EU_SI' => 'Slovenia',
				      'OC_SB' => 'Solomon Islands',
				      'AF_SO' => 'Somalia', 
				      'AF_ZA' => 'South Africa',
				      'OT_GS' => 'South Georgia and the South Sandwich Islands',
                      'AF_SS'=> 'South Sudan',
				      'EU_ES' => 'Spain',
				      'AS_LK' => 'Sri Lanka',
				      'AF_SH' => 'St. Helena',
				      'AM_PM' => 'St. Pierre and Miquelon', 
				      'AF_SD' => 'Sudan', 
				      'AM_SR' => 'Suriname',
				      'EU_SJ' => 'Svalbard and Jan Mayen Islands',
				      'EU_SW' => 'Sweden',
				      'EU_CH' => 'Switzerland', 
				      'AS_SY' => 'Syria',
				      'AS_TW' => 'Taiwan, Province of China',
				      'AS_TJ' => 'Tajikistan',
				      'AF_TZ' => 'Tanzania, United Republic of',
				      'AS_TH' => 'Thailand',
				      'AS_TL' => 'Timor-Leste',
				      'AF_TG' => 'Togo',
				      'OC_TK' => 'Tokelau',
				      'OC_TO' => 'Tonga', 
				      'AM_TT' => 'Trinidad and Tobago', 
				      'AF_TN' => 'Tunisia', 
				      'AS_TR' => 'Turkey',
				      'AS_TM' => 'Turkmenistan',
				      'AM_TC' => 'Turks and Caicos Islands',
				      'OC_TV' => 'Tuvalu',
				      'AF_UG' => 'Uganda',
				      'EU_UA' => 'Ukraine',
				      'AS_AE' => 'United Arab Emirates',
				      'EU_GB' => 'United Kingdom',
				      'AM_US' => 'United States of America',
				      'AM_UM' => 'United States Minor Outlying Islands',
				      'AM_UY' => 'Uruguay', 
				      'AS_UZ' => 'Uzbekistan',
				      'EU_VT' => 'Vatican City', 
				      'OC_VU' => 'Vanuatu', 
				      'AM_VE' => 'Venezuela',
				      'AS_VN' => 'Vietnam',
				      'AM_VI' => 'Virgin Islands', 
				      'OC_WF' => 'Wallis and Futuna Islands', 
				      'AF_EH' => 'Western Sahara',
				      'AS_YE' => 'Yemen', 
				      'AF_ZM' => 'Zambia',
				      'AF_ZW' => 'Zimbabwe');
    
  static function printCountrySelect($selected="") {
    print('<select name="country[]">');
    foreach(Tools::$COUNTRY_LIST as $symb => $country) {
      if ($selected == $symb) {
        print('<option value="' . $symb . '" selected="selected">' . $country . '</option>');
      } else {
        print('<option value="' . $symb . '">' . $country . '</option>');
      }
    }
    print('</select>');
  }
    
  static function printContinentDivs($countries, $print = True) {
    $cont_count = array('AF' => 0, 'AM' => 0, 'AS' => 0, 'EU' => 0, 'OC' => 0, 'OT' => 0);
    foreach($countries as $country) {
      $cont_count[substr($country,0,2)]++;
    }
    arsort($cont_count);
    reset($cont_count);
    $output = '<div class="continent_' . key($cont_count) . '">';
    next($cont_count);
    if (current($cont_count) == 0) {
      $output .= '<div class="continent_last"><div>';
    } else {
      $output .= '<div class="continent_' . key($cont_count) . '">';
      next($cont_count);
      if (current($cont_count) == 0) {
        $output .= '<div class="continent_last">';
      } else {
        $output .= '<div class="continent_last continent_' . key($cont_count) . '">';
      }
    }
    if ($print) {
      print($output);
    }
    return $output;
  }

  static function stringTimeLeft($timeLeft) {
    
    $days = floor($timeLeft / 86400);
    $hours = floor(($timeLeft % 86400) / 3600);
    $minutes = floor(($timeLeft % 3600) / 60);
    
    return $days . " days " . sprintf("%02d", $hours) . ":" . sprintf("%02d", $minutes);
    
  }

  /*
   * We use this function to print the strings of
   * the $_POST variables without " & / or another
   * weird character...x
   */

  static function printHTMLbr($string) {
    print nl2br(htmlentities($string, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1'));
  }

  static function printHTML($string) {
    print htmlentities($string, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1');
  }
  
  static function printHTMLreport($string, $length) {
    if(strlen($string) > $length) {
      $substring = substr($string,0,$length);
      print(htmlentities($substring, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '<center>-- Report Truncated --</center>');
    } else {
      Tools::printHTML($string);
    }
  }

  static function printHTMLsubstr($string, $length) {
    if(strlen($string) > $length) {
      $substring = substr($string,0,$length-3) . " (...)";
      print('<span title="'. htmlentities($string, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') .'">' . htmlentities($substring, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</span>');
    } else {
      print htmlentities($string, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1');
    }
  }

  static function HTMLsubstr($string, $length) {
    if(strlen($string) > $length) {
      $substring = substr($string,0,$length-3) . " (...)";
      return '<span title="'. htmlentities($string, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') .'">' . htmlentities($substring, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</span>';
    } else {
      return htmlentities($string, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1');
    }
  }

  static function filterControlChars($string) {
    return preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', $string);
  }

  static function readPost($postString, $index = null) {
    if (!array_key_exists($postString, $_POST)) {
      return null;
    }
    if ($index !== null) {
      return str_replace("\r\n","\n",$_POST[$postString][$index]);
    } else {
      return str_replace("\r\n","\n",$_POST[$postString]);
    }
  }

  static function UTF8readPost($postString) {
    if (!array_key_exists($postString, $_POST)) {
      return null;
    }
    return utf8_encode($_POST[$postString]);
  }

  static function printCategories($selected, $name){
    $domxPath = new DOMXpath(Tools::$config);
    $categoryGroups = $domxPath->query("/xml/submission/categories/categoryGroup");
    print('<select name="' . $name . '" size="1">');
    if ($selected == ""){
      print('<option></option>');
    }
    foreach($categoryGroups as $categoryGroup) {
      Tools::printCategoryGroup($categoryGroup, $selected);
    }
    $standAloneCategories = $domxPath->query("/xml/submission/categories/standAloneCategory");
    foreach($standAloneCategories as $standAloneCategory) {
      Tools::printCategory($standAloneCategory, $selected);
    }
    print('</select>');

  }

  private static function printCategoryGroup($categoryGroup, $selected) {
    $name = $categoryGroup->getElementsByTagName("name")->item(0)->textContent;
    print("<optgroup label=\"" . $name . "\">");
    foreach($categoryGroup->getElementsByTagName("category") as $category) {
      Tools::printCategory($category, $selected);
    }
    print("</optgroup>");
  }

  private static function printCategory($category, $selected) {
    $id = Tools::getCategoryId($category);
    if ($id == $selected){
      print("<option value=\"" . $id . "\" selected>" . $category->textContent . "</option>");
    } else {
      print("<option value=\"" . $id . "\">" . $category->textContent . "</option>");
    }
  }

  private static function getCategoryId($XMLcategory) {
    if ($XMLcategory->attributes) {
       return $XMLcategory->attributes->getNamedItem("id")->value;
    } else {
       return $XMLcategory->textContent;
    }                    
  }

  /* returns an Array of all the categories of the form id => name */
  static function getAllCategories() {
    $allCategories = array();
    $domxPath = new DOMXpath(Tools::$config);
    $categories = $domxPath->query("/xml/submission/categories/categoryGroup/category");
    $i = 0;
    foreach($categories as $category) {
      $allCategories[Tools::getCategoryId($category)] = $category->textContent;
      $i++;
    }
    $categories = $domxPath->query("/xml/submission/categories/standAloneCategory");
    foreach($categories as $category) {
      $allCategories[Tools::getCategoryId($category)] = $category->textContent;
      $i++;
    }
    return $allCategories;
  }

  static function getCategoryNameById($id) {
    $domxPath = new DOMXpath(Tools::$config);
    $categories = $domxPath->query("/xml/submission/categories/categoryGroup/category");
    foreach($categories as $category) {
      if (Tools::getCategoryId($category) == $id) {
        return $category->textContent;
      }
    }
    $categories = $domxPath->query("/xml/submission/categories/standAloneCategory");
    foreach($categories as $category) {
      if (Tools::getCategoryId($category) == $id) {
        return $category->textContent;
      }
    } 
    return $id;                            
  }

  static function getCategoryIdByHtmlName($name) {
    $domxPath = new DOMXpath(Tools::$config);
    $categories = $domxPath->query("/xml/submission/categories/categoryGroup/category");
    foreach($categories as $category) {
      if (htmlentities($category->textContent, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') == $name) {
        return Tools::getCategoryId($category);
      }
    }
    $categories = $domxPath->query("/xml/submission/categories/standAloneCategory");
    foreach($categories as $category) {
      if (htmlentities($category->textContent, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') == $name) {
        return Tools::getCategoryId($category);
      }
    } 
    return $name;
  }

  static function printLogos() {
    $domxPath = new DOMXpath(Tools::$config);
    $domNodes = $domxPath->query("/xml/conference/logos/logo");
    for($i = 0; $i < $domNodes->length; $i++) {
      $img = $domNodes->item($i)->getElementsByTagName("img")->item(0)->textContent;
      if($img != "") {
	$link = $domNodes->item($i)->getElementsByTagName("link")->item(0)->textContent;
	print("  <a href=\"" . $link . "\"><img src=\"" . $img . "\" alt=\"\" /></a><br />\n");
      }
    }
  }

  /* This function assumes that there is only  ONE element corresponding the this xpath -> only for text values */

  static function getConfig($xPath) {
    $domxPath = new DOMXpath(Tools::$config);
    $domNode = $domxPath->query("/xml/" . $xPath)->item(0);
    if (!is_null($domNode) && !is_null($domNode->firstChild)) {
      return utf8_decode($domNode->firstChild->nodeValue);
    }
    return null;
  }

  static function getChairConfig($xPath) {
    if(!is_null(Tools::$chairConfig)) {
      $domxPath = new DOMXpath(Tools::$chairConfig);
      $domNode = $domxPath->query("/xml/" . $xPath)->item(0);
      return utf8_decode($domNode->firstChild->nodeValue);
    }
    return null;
  }

  static function testFileType() {
    $file = fopen($_FILES['file']['tmp_name'], "r");
    /* to do -> test if the open procedure was succesfull */
    $header = fread($file, 5);
    fclose($file);
    switch ($header) {
    case "%!PS-":
      if (preg_match("/\.[pP][sS]$/", $_FILES['file']['name'])) {
        return array("valid" => True, "type" => "PostScript");
      } else {
        return array("valid" => False, "type" => "unknown");
      }
    case "%PDF-":
      if (preg_match("/\.[pP][dD][fF]$/", $_FILES['file']['name'])) {
        return array("valid" => True, "type" => "PDF");
      } else {
        return array("valid" => False, "type" => "unknown");
      }
    case "PKCDT":
      return array("valid" => False, "type" => "compressed Zip");
    case "Rar!Z":
      return array("valid" => False, "type" => "compressed Rar");
    default:
      return array("valid" => False, "type" => "unknown");
    }
  }

  static function serverIsShutdown() {
    $time = Tools::getConfig("server/shutdownTime");
    $date = Tools::getConfig("server/shutdownDate");
    $timeLeft = Tools::getUFromDateAndTime($date, $time) - gmdate("U");
    if($timeLeft <= 0) {
      return TRUE;
    }
    return FALSE;
  }


  static function getUFromDateAndTime($date, $time) {

    $hours = substr($time,0,2);
    $minutes = substr($time,3,2);
    $day = substr($date,0,2);
    $month = substr($date,3,2);
    $year = substr($date,6,4);
    return gmmktime($hours,$minutes,0,$month,$day,$year);

  }



  static function printServerShutdownMessage() {
    print("<div class=\"ERRmessage\">" . nl2br(htmlentities(Tools::getConfig("server/shutdownMessage"), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')) . "</div>");
  }

  /* used in the admin configration */

  static function updateConfig() {
    Tools::$config->load("../utils/config.xml");
  }

  /* create a domDocument with the given review fields */

  static function createXMLReview($useDate,
			   $articleNumberValue, 
			   $articleTitleValue, $authorsValue, 
			   $reviewerValue, $subreviewerValue, 
			   $overallGradeValue, $confidenceLevelValue, $technicalQualityValue, $editorialQualityValue, $suitabilityValue, $bestPaperValue,
			   $toProgramCommitteeValue, $toAuthorsValue, $personalNotesValue) {

    $reviewDocument = new DOMDocument('1.0');
    $reviewDocument->formatOutput = true;
    $reviewDocument->encoding = "iso-8859-1";

    /******************************************/
    /* Comment: cannot use certain characters */
    /******************************************/

    $commentText='
     Note that the characters &, <, and > are forbidden in XML.
     Please use their HTML equivalent, i.e., "&amp;", "&lt;", and "&gt;".
     - Be sure not to modify the <articleNumber> and <articleTitle> fields.
       They will be required when uploading your review file.
     - The <subreviewer> field should contain the full name of the
       subreviewer for this article. Leave blank if none.
';
    
    $commentText .= '     - The <overallGrade> is chosen between ' . Tools::getConfig("review/overallGradeMin") . ' and ' . Tools::getConfig("review/overallGradeMax") . '.
';
    $gradeSemantic = Tools::getGradeSemanticsString("overallGrade");
    if($gradeSemantic != "") {
      $commentText .= '         ' . str_replace(";", "\n        ", $gradeSemantic) . "\n";
    }

    if(Tools::getConfig("review/useConfidenceLevel") == "yes") {
      $commentText .= '     - The <confidenceLevel> is chosen between ' . Tools::getConfig("review/confidenceLevelMin") . ' and ' . Tools::getConfig("review/confidenceLevelMax") . '.
';
      $gradeSemantic = Tools::getGradeSemanticsString("confidenceLevel");
      if($gradeSemantic != "") {
	$commentText .= '         ' . str_replace(";", "\n        ", $gradeSemantic) . "\n";
      }
    }

    if(Tools::getConfig("review/useTechnicalQuality") == "yes") {
      $commentText .= '     - The <technicalQuality> is chosen between ' . Tools::getConfig("review/technicalQualityMin") . ' and ' . Tools::getConfig("review/technicalQualityMax") . '.
';
      $gradeSemantic = Tools::getGradeSemanticsString("technicalQuality");
      if($gradeSemantic != "") {
	$commentText .= '         ' . str_replace(";", "\n        ", $gradeSemantic) . "\n";
      }
    }

    if(Tools::getConfig("review/useEditorialQuality") == "yes") {
      $commentText .= '     - The <editorialQuality> is chosen between ' . Tools::getConfig("review/editorialQualityMin") . ' and ' . Tools::getConfig("review/editorialQualityMax") . '.
';
      $gradeSemantic = Tools::getGradeSemanticsString("editorialQuality");
      if($gradeSemantic != "") {
	$commentText .= '         ' . str_replace(";", "\n        ", $gradeSemantic) . "\n";
      }
    }

    if(Tools::getConfig("review/useSuitability") == "yes") {
      $commentText .= '     - The <suitability> grade is chosen between ' . Tools::getConfig("review/suitabilityMin") . ' and ' . Tools::getConfig("review/suitabilityMax") . '.
';
      $gradeSemantic = Tools::getGradeSemanticsString("suitability");
      if($gradeSemantic != "") {
	$commentText .= '         ' . str_replace(";", "\n        ", $gradeSemantic) . "\n";
      }
    }

    if(Tools::getConfig("review/useBestPaper") == "yes") {
      $commentText .= '     - The <bestPaper> grade is chosen between ' . Tools::getConfig("review/bestPaperMin") . ' and ' . Tools::getConfig("review/bestPaperMax") . '.
';
      $gradeSemantic = Tools::getGradeSemanticsString("bestPaper");
      if($gradeSemantic != "") {
	$commentText .= '         ' . str_replace(";", "\n        ", $gradeSemantic) . "\n";
      }
    }


    $domComment = $reviewDocument->createComment($commentText);
    $reviewDocument->appendChild($domComment);
    
    /*************/
    /* main node */
    /*************/

    $xml = $reviewDocument->createElement('xml'); 
    $xml = $reviewDocument->appendChild($xml);
    
    /***********************************/
    /* Used Date when the xml is saved */
    /***********************************/

    if($useDate) {

      $date = $reviewDocument->createElement('date'); 
      $date = $xml->appendChild($date);
      
      $dateValue = gmdate("Ymd-His");
      $value = $reviewDocument->createTextNode(utf8_encode($dateValue));
      $date->appendChild($value);      

    }

    /*****************************/
    /* Section about the article */
    /*****************************/
        
    $articleNumber = $reviewDocument->createElement('articleNumber'); 
    $articleNumber = $xml->appendChild($articleNumber);
    
    $value = $reviewDocument->createTextNode(utf8_encode(sprintf("%04d",$articleNumberValue)));
    $articleNumber->appendChild($value);
    
    $title = $reviewDocument->createElement('articleTitle'); 
    $title = $xml->appendChild($title);
    
    $value = $reviewDocument->createTextNode(utf8_encode($articleTitleValue));
    $title->appendChild($value);
    
    if(Tools::getConfig('review/anonymousAuthors') == "") {
      $authors = $reviewDocument->createElement('articleAuthors');
      $authors = $xml->appendChild($authors);
      
      $value = $reviewDocument->createTextNode(utf8_encode($authorsValue));
      $authors->appendChild($value);
      
    }
    
    /******************************/
    /* Section about the Reviewer */
    /******************************/
    
    $fullName = $reviewDocument->createElement('reviewer');
    $fullName = $xml->appendChild($fullName);
    
    $value = $reviewDocument->createTextNode(utf8_encode($reviewerValue));
    $fullName->appendChild($value);
    
    $subreferee = $reviewDocument->createElement('subreviewer');
    $subreferee = $xml->appendChild($subreferee);
    
    $value = $reviewDocument->createTextNode(utf8_encode($subreviewerValue));
    $subreferee->appendChild($value);
    
    /***********************************/
    /* Section about the Review itself */
    /***********************************/
        
    $overallGrade = $reviewDocument->createElement('overallGrade');
    $overallGrade = $xml->appendChild($overallGrade);
    
    $value = $reviewDocument->createTextNode(utf8_encode($overallGradeValue));
    $overallGrade->appendChild($value);
      
    if(Tools::getConfig("review/useConfidenceLevel") == "yes") {
      
      $confidenceLevel = $reviewDocument->createElement('confidenceLevel');
      $confidenceLevel = $xml->appendChild($confidenceLevel);
      
      $value = $reviewDocument->createTextNode(utf8_encode($confidenceLevelValue));
      $confidenceLevel->appendChild($value);
      
    }
    
    if(Tools::getConfig("review/useTechnicalQuality") == "yes") {
      
      $technicalQuality = $reviewDocument->createElement('technicalQuality');
      $technicalQuality = $xml->appendChild($technicalQuality);
      
      $value = $reviewDocument->createTextNode(utf8_encode($technicalQualityValue));
      $technicalQuality->appendChild($value);
      
    }
    
    if(Tools::getConfig("review/useEditorialQuality") == "yes") {
      
      $editorialQuality = $reviewDocument->createElement('editorialQuality');
      $editorialQuality = $xml->appendChild($editorialQuality);
      
      $value = $reviewDocument->createTextNode(utf8_encode($editorialQualityValue));
      $editorialQuality->appendChild($value);
      
    }
    
    if(Tools::getConfig("review/useSuitability") == "yes") {
      
      $suitability = $reviewDocument->createElement('suitability');
      $suitability = $xml->appendChild($suitability);
      
      $value = $reviewDocument->createTextNode(utf8_encode($suitabilityValue));
      $suitability->appendChild($value);
      
    }
    
    if(Tools::getConfig("review/useBestPaper") == "yes") {
      
      $bestPaper = $reviewDocument->createElement('bestPaper');
      $bestPaper = $xml->appendChild($bestPaper);
      
      $value = $reviewDocument->createTextNode(utf8_encode($bestPaperValue));
      $bestPaper->appendChild($value);
      
    }
    
    if(Tools::getConfig("review/useToProgramCommittee") == "yes") {
      
      $toProgramCommittee = $reviewDocument->createElement('toProgramCommittee');
      $toProgramCommittee = $xml->appendChild($toProgramCommittee);
      
      $value = $reviewDocument->createTextNode(utf8_encode($toProgramCommitteeValue));
      $toProgramCommittee->appendChild($value);
      
    }
    
    if(Tools::getConfig("review/useToAuthors") == "yes") {
      
      $toAuthors = $reviewDocument->createElement('toAuthors');
      $toAuthors = $xml->appendChild($toAuthors);
      
      $value = $reviewDocument->createTextNode(utf8_encode($toAuthorsValue));
      $toAuthors->appendChild($value);
      
    }
    
    $personalNotes = $reviewDocument->createElement('personalNotes');
    $personalNotes = $xml->appendChild($personalNotes);
    
    $value = $reviewDocument->createTextNode(utf8_encode($personalNotesValue));
    $personalNotes->appendChild($value);
    
    return $reviewDocument;

  }
  

  static function getGradeSemanticsString($name) {
    $semantics = Tools::getGradeSemantics($name);
    $semanticString = "";
    $max = Tools::getConfig('review/'.$name.'Max');
    $min = Tools::getConfig('review/'.$name.'Min');
    for($grade = $min; $grade <= $max; $grade++) {
      if($semantics[$grade] != "") {
	$semanticString .= "; " . $grade . $semantics[$grade];
      }
    }
    return substr($semanticString, 2);
  }
  



  /* the following functions return a boolean depending on that fact that conferences uses abstact, keywords,... or not */
  
  static function useGlobalBCC() {
    return (Tools::getConfig("mail/useGlobalBCC") == "yes");
  }

  static function useTagArticles() {
    return (Tools::getConfig("review/tagArticles") == "yes");
  }

  static function useCustomAccept1() {
    return (Tools::getConfig("review/useCustomAccept1") == "yes");
  }

  static function getCustomAccept1() {
    return Tools::getConfig("review/customAccept1");
  }

  static function useCustomAccept2() {
    return (Tools::getConfig("review/useCustomAccept2") == "yes");
  }

  static function getCustomAccept2() {
    return Tools::getConfig("review/customAccept2");
  }

  static function useCustomAccept3() {
    return (Tools::getConfig("review/useCustomAccept3") == "yes");
  }

  static function getCustomAccept3() {
    return Tools::getConfig("review/customAccept3");
  }

  static function useCustomAccept4() {
    return (Tools::getConfig("review/useCustomAccept4") == "yes");
  }

  static function getCustomAccept4() {
    return Tools::getConfig("review/customAccept4");
  }

  static function useCustomAccept5() {
    return (Tools::getConfig("review/useCustomAccept5") == "yes");
  }

  static function getCustomAccept5() {
    return Tools::getConfig("review/customAccept5");
  }

  static function useCustomAccept6() {
    return (Tools::getConfig("review/useCustomAccept6") == "yes");
  }

  static function getCustomAccept6() {
    return Tools::getConfig("review/customAccept6");
  }

  static function useCustomAccept7() {
    return (Tools::getConfig("review/useCustomAccept7") == "yes");
  }

  static function getCustomAccept7() {
    return Tools::getConfig("review/customAccept7");
  }

  static function useAffiliations() {
    return (Tools::getConfig("submission/useAffiliations") == "yes");
  }
  
  static function useCustomCheck1() {
    return (Tools::getConfig("submission/useCustomCheck1") == "yes");
  }

  static function useCustomCheck2() {
    return (Tools::getConfig("submission/useCustomCheck2") == "yes");
  }

  static function useCustomCheck3() {
    return (Tools::getConfig("submission/useCustomCheck3") == "yes");
  }

  static function getCustomCheck1($long = false) {
    if ($long) {
      return Tools::getConfig("submission/longCustomCheck1");
    } else {
      return Tools::getConfig("submission/shortCustomCheck1");
    }
  }

  static function getCustomCheck2($long = false) {
    if ($long) {
      return Tools::getConfig("submission/longCustomCheck2");
    } else {
      return Tools::getConfig("submission/shortCustomCheck2");
    }
  }

  static function getCustomCheck3($long = false) {
    if ($long) {
      return Tools::getConfig("submission/longCustomCheck3");
    } else {
      return Tools::getConfig("submission/shortCustomCheck3");
    }
  }

  static function chaironlyCustomCheck1() {
    return (Tools::getConfig("submission/chaironlyCustomCheck1") == "yes");
  }

  static function chaironlyCustomCheck2() {
    return (Tools::getConfig("submission/chaironlyCustomCheck2") == "yes");
  }

  static function chaironlyCustomCheck3() {
    return (Tools::getConfig("submission/chaironlyCustomCheck3") == "yes");
  }

  static function useAbstract() {
    return (Tools::getConfig("submission/useAbstract") == "yes");
  }
  
  static function useCategory() {
    return ((Tools::getConfig("submission/useCategory") == "yes") && (count(Tools::getAllCategories()) != 0));
  }
  
  static function useKeywords() {
    return (Tools::getConfig("submission/useKeywords") == "yes");
  }

  static function useCountry() {
    return (Tools::getConfig("submission/useCountry") == "yes");
  }

  static function authorsAreAnonymous() {
    return (Tools::getConfig("review/anonymousAuthors") == "yes");
  }

  static function reviewersAreAnonymous() {
    return (Tools::getConfig("review/anonymousReviewers") == "yes");
  }
  
  static function usePreview() {
    return (Tools::getConfig("submission/usePreview") == "yes");
  }

  static function useZipReviews() {
    return (Tools::getConfig("server/zipPath") != "");
  }
  
  static function useConfidenceLevel() {
    return (Tools::getConfig("review/useConfidenceLevel") == "yes");
  }

  static function useTechnicalQuality() {
    return (Tools::getConfig("review/useTechnicalQuality") == "yes");
  }

  static function useEditorialQuality() {
    return (Tools::getConfig("review/useEditorialQuality") == "yes");
  }

  static function useSuitability() {
    return (Tools::getConfig("review/useSuitability") == "yes");
  }

  static function useBestPaper() {
    return (Tools::getConfig("review/useBestPaper") == "yes");
  }

  static function useToAuthors() {
    return (Tools::getConfig("review/useToAuthors") == "yes");
  }

  static function useToProgramCommittee() {
    return (Tools::getConfig("review/useToProgramCommittee") == "yes");
  }
  
  static function isValidEmail($mail) {
    $emaillist = explode(",", trim($mail, ", \t\n\r\0\x0B"));
    foreach ($emaillist as $email) {
      if (!preg_match("/^([a-zA-Z0-9!#$%&*+\-\/\=^_{|}~]+\.)*[a-zA-Z0-9!#$%&*+\-\/\=^_{|}~]+@.+\..+/" ,trim($email))) {
        return False;
      }
    }
    return True;
  }
  
  static function xmlentities($string) {
    return str_replace( array ('"', "'", '<', '>', '\u2019'), array ('&quot;', '&apos;' , '&lt;' , '&gt;', '&apos;'), $string );
  }
  
  /* this function is used by the admin to check if the min/max possible grades for a review a correct grades */
  static function checkMinMaxGrades($min, $max, $type) {
    $status = "";
    if(!preg_match("/^[0-9]+$/",$min)) {
      $status .= "The minimum " . $type . " grade you entered is not a non-negative integer.\n";
    } else if(!preg_match("/^[0-9]+$/",$max)) {
      $status .= "The maximum " . $type . " grade you entered is not a non-negative integer.\n";
    } else if($min > $max) {
      $status .= "The minimum " . $type . " is greater than the maximum " . $type . ".\n";
    } 
    return $status;
  }

  /* check if a grade is a grade (!) and that is lies between the two bounds */
  static function checkGrade($grade, $min, $max, $type) {
    $status = "";
    if($grade == "") {
      return $status;
    }
    if(!preg_match("/^[0-9]+$/",$grade)) {
      $status .= "The " . $type . " \"" . $grade . "\" you entered is not a integer.\n";
      return $status;
    }
    if($grade > $max) {
      $status .= "The " . $type . " you entered \"".$grade."\" is larger than the maximum value " . $max . ".\n";
    }
    if($grade < $min) {
      $status .= "The " . $type . " you entered \"".$grade."\" is smaller than the minimum value " . $min . ".\n";
    }
    return $status;
  }

  /* Check the semantics of the grades entered by the administrator */
  static function checkGradeSematics($gradeSemantics, $type) {
    $singleGradeSematics = explode(";", $gradeSemantics);
    foreach($singleGradeSematics as $singleGradeSematic) {
      $singleGradeSematic = trim($singleGradeSematic);
      if(($singleGradeSematic != "") && !preg_match("/^[0-9]+\s*-[^<>&]+$/", $singleGradeSematic)) {
	return "Syntax error in the " . $type . " semantics. Please refer to the manual for further details.\n";
      }
    }
    return "";
  }


  /* Return the current phase. Usefull to know which menu shall be accessible to the reviewers */
  
  static function getCurrentPhase() {
    $domxPath = new DOMXpath(Tools::$chairConfig);
    $domNode = $domxPath->query("/xml/phase/current")->item(0);
    return utf8_decode($domNode->firstChild->nodeValue);
  }

  static function restrictChairMenu() {
    $domxPath = new DOMXpath(Tools::$chairConfig);
    $domNode = $domxPath->query("/xml/restrictedChairMenu")->item(0);
    if (!is_null($domNode) && !is_null($domNode->firstChild)) {
      return (utf8_decode($domNode->firstChild->nodeValue) == "yes");
    }
    return null;
  }

  /* Tells if the time's up time box should be displayed or not */

  static function hideTimeBox() {
    $domxPath = new DOMXpath(Tools::$chairConfig);
    $domNode = $domxPath->query("/xml/phase/hideTime")->item(0);
    if (!is_null($domNode) && !is_null($domNode->firstChild)) {
      return (utf8_decode($domNode->firstChild->nodeValue) == "yes");
    }
    return null;
  }

  static function createDefaultReviewsConfigFile($configFileName) {
         
    $newConfig = new DOMDocument();
    $newConfig->formatOutput = true;
    $newConfig->encoding = "iso-8859-1";
    
    $xml = $newConfig->createElement('xml'); 
    $xml = $newConfig->appendChild($xml);
    
    /* phase selection */
    
    $phase = $newConfig->createElement('phase');
    $phase = $xml->appendChild($phase);
    
    /* phase value */

    $current = $newConfig->createElement('current');
    $current = $phase->appendChild($current);

    $value = $newConfig->createTextNode(Tools::$SUBMISSION);
    $current->appendChild($value);
    
    /* Hide Review Deadline time box */
    
    $hideTime = $newConfig->createElement('hideTime');
    $hideTime = $phase->appendChild($hideTime);
    
    $value = $newConfig->createTextNode('');
    $hideTime->appendChild($value);

    /* restrictedChairMenu */
    
    $restrictedChairMenu = $newConfig->createElement('restrictedChairMenu');
    $restrictedChairMenu = $xml->appendChild($restrictedChairMenu);
    
    $value = $newConfig->createTextNode('');
    $restrictedChairMenu->appendChild($value);
        
    $newConfig->save($configFileName);
    
  }

  static function printSelectGrade($name, $min, $max ,$selected) {
    $semantics = Tools::getGradeSemantics($name);
    print('<select name="' . htmlentities($name, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '" size="1" class="gradeSelect"><option></option>');
    for ($val = $min; $val <= $max; $val++) {
      $semantic = $semantics[$val];
      if ($val == $selected){
	print('<option selected>' . $val . $semantic . '</option>');
      } else {
	print('<option>' . $val . $semantic . '</option>');
      }
    }
    print('</select>');
  }

  static function getGradeSemantics($name) {
    $semantics = array();
    $semanticStrings = explode(";", Tools::getConfig('review/'.$name.'Semantics'));
    foreach($semanticStrings as $semanticString) {
      $semanticString = trim($semanticString);
      preg_match("/^([0-9]+)\s*-(.+)$/", $semanticString, $grade);
      $semantics[$grade[1]] = " - " . trim($grade[2]);
    }
    return $semantics;
  }

    /* This function generates id's, which are strings made of [0-9a-v] (36 characters => 5 bits/character). */
    /* The length of the wanted string is given as a parameter. */
    /* Since 30 bits should be far enough for an id (as we always make sure that an id hasn't been given yet, before */
    /* we actually give it to a new submission), 6 characters should be enough. */

    static function generateIdString($length) {

	/* Generate a random hex string of length just >= 5*$length bits */
	$bitlength = 5 * $length;
	$hexString = "";
	$binString = "";
	do {
	    $hexString .= '' . md5(rand(0,100000).time().rand(0,100000).getmypid().rand(0,100000).$hexString);
	    $bitlength -= 128;
	}
	while($bitlength > 0);

	/* Turn the hex string into a binary string */
	foreach(str_split($hexString) as $hex) {
	    $binString = $binString . sprintf("%04d", base_convert($hex,16,2));
	}

	/* Keep 5*$length binary digits*/
	$binString = substr($binString, 0, (5 * $length));

	/* Ad the character ':' each 5 binary character */
	$binString = chunk_split($binString, 5, ":");

	/* Translate the string to an id */
	$id = strtr($binString, Tools::$idTrans);

	return $id;

    }

    /* In several place, we test if a string looks like a correct ID -> use this function */
    static function isAnId($id) {
	if(strlen($id) != Tools::$ID_LENGTH) {
	    return false;
	}
	if(!preg_match("/[0-9A-V]{". Tools::$ID_LENGTH .",}/", $id)) {
	    return false;
	}
	return true;
    }

  /* this functions (used in the admin section) returns a non-empty string whenever there is something wrong with */
  /* the configuration. */

  static function getAdminConfigStatus() {
    $status = "";
    if(!file_exists(Tools::getConfig("server/reviewsPath"))) {
      $status .= "Configuration error: the reviews path is incorrect."; 
      $status .= "Please go the <a href=\"configuration.php\">Configuration page</a> and enter a valid Reviews path.\n";
    } else if(is_file(Tools::getConfig("server/reviewsPath"))) {
      $status .= "Configuration error: the reviews path is not a directory.";
      $status .= " Please go the <a href=\"configuration.php\">Configuration page</a> and enter a valid Reviews path.\n";
    }
    return $status;
  }

  static function isConfigured() {
    return (file_exists("utils/config.xml") || file_exists("../utils/config.xml"));
  }


  static function adminHasNoPassword() {
    if(!array_key_exists('PHP_AUTH_USER', $_SERVER) || $_SERVER['PHP_AUTH_USER'] == "") {
      print('<div class="ERRmessage">It seems that no password has been set for the administrator!'.
	    ' Use the <a href="admin_password.php">Admin Password</a> page to set it. If you already'.
	    ' did so, there must a problem with your apache configuration. Please refer to the'.
	    ' documentation for more details.</div>');
      return true;
    }
    return false;
  }
  

  /* Replace [authors],... by their correct value */

  static function parseMailToAuthors($stringToParse, $article) {
    
    $parsedString = Tools::parseMail($stringToParse);
    $parsedString = str_replace("[authors]", $article->getAuthors(), $parsedString);
    $parsedString = str_replace("[title]", $article->getTitle(), $parsedString);
    $parsedString = str_replace("[article_number]", $article->getArticleNumber(), $parsedString);    
    $parsedString = str_replace("[reports]", $article->getAllCompletedReports(), $parsedString);    
    $parsedString = str_replace("[attach_article]", "[attach_article_checked]", $parsedString);    
    return $parsedString;

  }

  static function printMailToAuthorsTags() {
    print('<tr><td>[authors]</td><td>Names of the authors</td></tr>');
    print('<tr><td>[title]</td><td>Title of the article</td></tr>');
    print('<tr><td>[article_number]</td><td>Article number</td></tr>');
    print('<tr><td>[reports]</td><td>Reports from the reviewers</td></tr>');
    print('<tr><td>[attach_article]</td><td>Attach the authors\' submission</td></tr>');
  }

  static function parseMailToCommittee($stringToParse, $reviewer) {
    
    $parsedString = $stringToParse;
    $parsedString = str_replace("[name]", $reviewer->getFullName(), $parsedString);
    $parsedString = str_replace("[login]", $reviewer->getLogin(), $parsedString);

    $time = Tools::getConfig("review/preferredDeadlineTime");
    $date = Tools::getConfig("review/preferredDeadlineDate");
    $deadline = gmdate('r', Tools::getUFromDateAndTime($date, $time));
    $parsedString = str_replace("[preference_deadline]", $deadline, $parsedString);

    $time = Tools::getConfig("review/deadlineTime");
    $date = Tools::getConfig("review/deadlineDate");
    $deadline = gmdate('r', Tools::getUFromDateAndTime($date, $time));
    $parsedString = str_replace("[review_deadline]", $deadline, $parsedString);

    return Tools::parseMail($parsedString);

  }

  static function printMailToCommitteeTags() {
    print('<tr><td>[name]</td><td>Full name of the reviewer</td></tr>');
    print('<tr><td>[login]</td><td>Login of the reviewer</td></tr>');
    print('<tr><td>[preference_deadline]</td><td>Deadline for choosing preferred articles</td></tr>');
    print('<tr><td>[review_deadline]</td><td>Deadline for reviews</td></tr>');
  }

  static function parseMailToSubmiters($stringToParse, $submission, $type) {
    
    $parsedString = $stringToParse;

    $paperInfo =  "   ID:              " . $submission->getId() . "\n";
    $paperInfo .= "   Title:           " . $submission->getVersion()->getTitle() . "\n";
    $paperInfo .= "   Authors:         " . $submission->getVersion()->getAuthors() . "\n";
    if(Tools::useAffiliations()) {
      $paperInfo .= "   Affiliations:    " . $submission->getVersion()->getAffiliations() . "\n";
    }
    if(Tools::useCountry()) {
      $paperInfo .= "   Countries:       " . $submission->getVersion()->getCountry() . "\n";
    }
    if(Tools::useCategory()) {
      $paperInfo .= "   Category:        " . Tools::getCategoryNameById($submission->getVersion()->getCategory()) . "\n";
    }
    if(Tools::useKeywords()) {
      $paperInfo .= "   Keywords:        " . $submission->getVersion()->getKeywords() . "\n";
    }
    if($type == "S") {
      $paperInfo .= "   Submission Date: " . gmdate('r',$submission->getVersion()->getDate()) . "\n";
    } else if($type == "R") {
      $paperInfo .= "   Revision Date:   " . gmdate('r',$submission->getVersion()->getDate()) . "\n";
    } 
    $paperInfo .= "   MD5:             " . $submission->getVersion()->getMD5();

    $parsedString = str_replace("[paper_info]", $paperInfo, $parsedString);

    return Tools::parseMail($parsedString);

  }

  static function printMailToSubmitersTags() {
    print('<tr><td>[paper_info]</td><td>Information about the Submission (ID, Date, MD5, etc.)</td></tr>');
  }

  static function parseMailForPassword($stringToParse, $reviewer, $password) {

    $parsedString = $stringToParse;
    $login = $reviewer->getLogin();
    $parsedString = str_replace("[login_and_password]", "  login:    " . $login .
				                      "\n  password: " . $password, $parsedString);
    return Tools::parseMailToCommittee($parsedString, $reviewer);
   
  }

  static function printMailForPasswordTags() {
    print('<tr><td>[login_and_password]</td><td>Login and password of the reviewer</td></tr>');
  }

  /* replace general tags in mass mails */

  static function parseMail($stringToParse) {

    $parsedString = $stringToParse;
    $parsedString = str_replace("[conf]", Tools::getConfig('conference/name'), $parsedString);
    $parsedString = str_replace("[site]", Tools::getConfig('server/location'), $parsedString);

    $time = Tools::getConfig("server/deadlineTime");
    $date = Tools::getConfig("server/deadlineDate");
    $deadline = gmdate('r', Tools::getUFromDateAndTime($date, $time));
    $parsedString = str_replace("[submission_deadline]", $deadline, $parsedString);

    $date = Tools::getConfig("miscellaneous/notificationDate");
    $deadline = gmdate('D, j M Y', Tools::getUFromDateAndTime($date, 0));
    $parsedString = str_replace("[notification]", $deadline, $parsedString);

    $date = Tools::getConfig("miscellaneous/finalVersionDate");
    $deadline = gmdate('D, j M Y', Tools::getUFromDateAndTime($date, 0));
    $parsedString = str_replace("[final_version]", $deadline, $parsedString);

    return $parsedString;
  }

  static function printMailTags() {
    print('<tr><td>[conf]</td><td>Conference name</td></tr>');
    print('<tr><td>[site]</td><td>iChair server location</td></tr>');
    print('<tr><td>[submission_deadline]</td><td>Official submission/revision/withdrawal deadline</td></tr>');
    print('<tr><td>[notification]</td><td>Notification of acceptance/rejection date</td></tr>');
    print('<tr><td>[final_version]</td><td>Deadline for final versions</td></tr>');
  }

  static function getAllNonReplacedTags($string) {
    $tags = array('\[authors\]', '\[title\]', '\[reports\]', '\[name\]', '\[login\]', '\[preference_deadline\]', '\[review_deadline\]', 
		  '\[paper_info\]', '\[login_and_password\]', '\[conf\]', '\[site\]', '\[submission_deadline\]', '\[notification\]', '\[final_version\]', '\[attach_article\]');
    $nonReplacedTags = array();
    $i = 0;
    foreach($tags as $tag) {
      if(preg_match('/' . $tag . '/', $string)) {
	$nonReplacedTags[$i++] = str_replace("\\", "", $tag);
      }
    }
    return $nonReplacedTags;
  }

  static function hasCallForPaperHTML($root) {
    return file_exists($root . "/cfp/cfp.html");
  }

  static function hasCallForPaperHTMLbak($root) {
    return file_exists($root . "/cfp/cfp.html.bak");
  }

  static function hasCallForPaperPDF($root) {
    return file_exists($root . "/cfp/cfp.pdf");
  }

  static function hasCallForPaperPDFbak($root) {
    return file_exists($root . "/cfp/cfp.pdf.bak");
  }

  static function hasCallForPaperTXT($root) {
    return file_exists($root . "/cfp/cfp.txt");
  }

  static function hasCallForPaperTXTbak($root) {
    return file_exists($root . "/cfp/cfp.txt.bak");
  }



  static function hasSubmissionGuidelinesHTML($root) {
    return file_exists($root . "/guidelines/guidelines.html");
  }

  static function hasSubmissionGuidelinesHTMLbak($root) {
    return file_exists($root . "/guidelines/guidelines.html.bak");
  }

  static function hasSubmissionGuidelinesPDF($root) {
    return file_exists($root . "/guidelines/guidelines.pdf");
  }

  static function hasSubmissionGuidelinesPDFbak($root) {
    return file_exists($root . "/guidelines/guidelines.pdf.bak");
  }

  static function hasSubmissionGuidelinesTXT($root) {
    return file_exists($root . "/guidelines/guidelines.txt");
  }

  static function hasSubmissionGuidelinesTXTbak($root) {
    return file_exists($root . "/guidelines/guidelines.txt.bak");
  }


  static function hasReviewGuidelinesHTML($root) {
    return file_exists($root . "/review/guidelines/guidelines.html");
  }

  static function hasReviewGuidelinesHTMLbak($root) {
    return file_exists($root . "/review/guidelines/guidelines.html.bak");
  }

  static function hasReviewGuidelinesPDF($root) {
    return file_exists($root . "/review/guidelines/guidelines.pdf");
  }

  static function hasReviewGuidelinesPDFbak($root) {
    return file_exists($root . "/review/guidelines/guidelines.pdf.bak");
  }

  static function hasReviewGuidelinesTXT($root) {
    return file_exists($root . "/review/guidelines/guidelines.txt");
  }

  static function hasReviewGuidelinesTXTbak($root) {
    return file_exists($root . "/review/guidelines/guidelines.txt.bak");
  }

  static function use12HourFormat() {
    return (Tools::getConfig('server/timeFormat') == "12");
  }

  static function tagAndCopy($oldFile, $newFile, $article) {

    if(preg_match('/\.ps$/', $oldFile)) {

      $nf = fopen($newFile, 'w');

      $fileArray = file($oldFile);
      $numberOfLines = count($fileArray);
      $i = 0;
      while(($i < $numberOfLines) && (substr($fileArray[$i], 0, 1) == '%')) {
	fwrite($nf, $fileArray[$i]);
	$i++;
      } 

      /* Add tag */
      $parsedTag = Tools::parseMailToAuthors(Tools::getConfig('review/tagArticlesString'), $article);
      fwrite($nf, "/saved-matrix matrix currentmatrix def\n");
      fwrite($nf, "/oldshowpage /showpage load def\n");
      fwrite($nf, "/pagenumberxx 0 def\n");
      fwrite($nf, "/showpage {gsave saved-matrix setmatrix\n");
      fwrite($nf, "           /Times-Roman findfont 16 scalefont setfont\n");
      fwrite($nf, "           90 rotate 0.65 setgray\n");
      fwrite($nf, "           75 -32 moveto (" . $parsedTag . ") show\n");
      fwrite($nf, "           grestore\n");
      fwrite($nf, "           /oldshowpage load exec} bind def\n");

      /* Copy the rest of the article */
      while($i < $numberOfLines) {
	fwrite($nf, $fileArray[$i]);
	$i++;
      }
      
      fwrite($nf, "/showpage /oldshowpage load def\n");
      fclose($nf);
      return true;

    } else if(preg_match('/\.pdf$/', $oldFile) && (Tools::getConfig('server/pdftkPath') != '')) {

      $fileName = tempnam('/tmp', 'bozo');

      $parsedTag = Tools::parseMailToAuthors(Tools::getConfig('review/tagArticlesString'), $article);
      $parsedTag = str_replace('(', '\(', $parsedTag);
      $parsedTag = str_replace(')', '\)', $parsedTag);
      $length = 56 + strlen($parsedTag);
      $tagArticle = file_get_contents('../utils/tag_article.pdf');
      $tagArticle = str_replace('[longueur]', $length, $tagArticle);
      $tagArticle = str_replace('[chaine_de_caracteres]', $parsedTag, $tagArticle);
      file_put_contents($fileName, $tagArticle);

      exec(Tools::getConfig('server/pdftkPath') . "pdftk " . escapeshellarg($oldFile) . " background " . escapeshellarg($fileName) . " output " . escapeshellarg($newFile), $output, $returnVal);
      unlink($fileName);
      if($returnVal == 0) {
	return true;
      } 

    } 

    copy($oldFile, $newFile);
    return false;

  }

  static function reformatText($string) {
    $newstring = "";
    $lineStarted = false;
    $lines = explode("\n", str_replace("\r\n","\n",$string));
    foreach($lines as $line) {
      if($lineStarted) {
	$line = ltrim($line);
      }
      if (preg_match("/^\s*$/", $line)) {
	if($lineStarted) {
	  $newstring .= "\n";
	  $lineStarted = false;
	}
        $newstring .= "\n";
      } else if (preg_match("/^([ -]|[0-9]+[.)])/", $line)) {
	$newstring .= "\n" . rtrim($line);
	$lineStarted = true;
      } else {
	if($lineStarted) {
	  $newstring .= " ";
	}
        $newstring .= rtrim($line);
	$lineStarted = true;
      }
    }
    return $newstring;
  }
}

/* Parse the admin xml config file */

Tools::$config = new DOMDocument();
if(file_exists("utils/config.xml")) {
  Tools::$config->load("utils/config.xml");
} else if(file_exists("../utils/config.xml")) {
  Tools::$config->load("../utils/config.xml");
} else if(file_exists("utils/default_config.xml")) {
  Tools::$config->load("utils/default_config.xml");
} else if(file_exists("../utils/default_config.xml")) {
  Tools::$config->load("../utils/default_config.xml");
} else {
  print("ERROR in tools.php: Could not find the file config.xml");
}

/* Parse the admin xml config file */

Tools::$chairConfig = new DOMDocument();
$chairConfigFileName = Tools::getConfig('server/reviewsPath') . "reviews_config.xml";
if(file_exists(Tools::getConfig('server/reviewsPath'))) {
    if(!file_exists($chairConfigFileName)) {
	Tools::createDefaultReviewsConfigFile($chairConfigFileName);
    }
    Tools::$chairConfig->load($chairConfigFileName);
}

?>
