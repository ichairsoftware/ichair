<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 

class Meeting {

  private $messageNumber;
  private $articleNumber;
  private $trashed;
  private $date;
  private $content;

  function __construct() {}

  static function openDB() {
    $dbFile = Tools::getConfig('server/reviewsPath') . "meeting.db";
    if (!file_exists($dbFile)) {
      Meeting::createDB();
    }
    return new SQLite3($dbFile);
  }

  static function createDB() {
    $db = new SQLite3(Tools::getConfig('server/reviewsPath') . "meeting.db");
    $db->exec("CREATE TABLE messages( messageNumber INTEGER PRIMARY KEY, articleNumber, trashed, date, content)");
  }

  function createFromDB($db_row) {
    $this->messageNumber = $db_row['messageNumber'];
    $this->articleNumber = $db_row['articleNumber'];
    $this->trashed = $db_row['trashed'];
    $this->date = $db_row['date'];
    $this->content = base64_decode($db_row['content']);
  }

  function getMessageNumber() {
    return $this->messageNumber;
  }

  function getArticleNumber() {
    return $this->articleNumber;
  }

  function getDate() {
    return $this->date;
  }

  function getContent() {
    return $this->content;
  }

  function isTrashed() {
    return ($this->trashed == "true");
  }

  static function addMessage($articleNumber, $content, $currentReviewer) {
    $date = gmdate("U");
    $db = Meeting::openDB();
    $trashed = "false";
    $db->exec('INSERT INTO messages(articleNumber, trashed, date, content) VALUES ("'
	       . $articleNumber . '", "'
	       . $trashed . '", "'
	       . $date . '", "'
	       . base64_encode($content) . '")');
    if($articleNumber != "None") {
      $article = Article::getByArticleNumber($articleNumber);
      $article->setLastModificationDate();
      Log::logSpecificMessage($currentReviewer, $articleNumber);
    } else {
      Log::logGeneralMessage($currentReviewer);
    }
  }

  static function trashMessage($messageNumber) {
    $db = Meeting::openDB();
    $db->exec('UPDATE messages SET trashed="true" WHERE messageNumber="' . $messageNumber . '"');
  }

  static function trashAllArticleMessages($articleNumber) {
    $db = Meeting::openDB();
    $db->exec('UPDATE messages SET trashed="true" WHERE articleNumber="' . $articleNumber . '"');
  }

  static function deleteMessage($messageNumber) {
    $db = Meeting::openDB();
    $db->exec('DELETE FROM messages WHERE messageNumber="' . $messageNumber . '"');
  }

  static function updateMessage($messageNumber, $content, $currentReviewer) {
    $date = gmdate("U");
    $db = Meeting::openDB();
    $db->exec('UPDATE messages SET trashed="false", date="' . $date . '", content="'.base64_encode($content).'" WHERE messageNumber="'.$messageNumber.'"');
    $articleNumber = $db->querySingle('SELECT articleNumber FROM messages WHERE messageNumber="'.$messageNumber.'"');
    if($articleNumber != "None") {
      $article = Article::getByArticleNumber($articleNumber);
      $article->setLastModificationDate();
      Log::logSpecificMessage($currentReviewer, $articleNumber);
    } else {
      Log::logGeneralMessage($currentReviewer);
    }
  }

  static function getAllNonTrashedGeneralMessages() {
    $messages = array();
    $db = Meeting::openDB();
    $i = 0;
    $result = $db->query('SELECT * FROM messages WHERE articleNumber="None" AND trashed<>"true" ORDER BY date DESC');
    while($db_row = $result->fetchArray()) {
      $messages[$i] = new Meeting();
      $messages[$i]->createFromDB($db_row);
      $i++;
    }
    return $messages;
  }

  static function getAllNonTrashedArticleMessages() {
    $messages = array();
    $db = Meeting::openDB();
    $i = 0;
    $result = $db->query('SELECT * FROM messages WHERE articleNumber<>"None" AND trashed<>"true" ORDER BY articleNumber, date DESC');
    while($db_row = $result->fetchArray()) {
      $messages[$i] = new Meeting();
      $messages[$i]->createFromDB($db_row);
      $i++;
    }
    return $messages;
  }

  static function getAllNonTrashedAuthorizedArticleMessages($reviewer) {
    $authorizedArticlesNumbers = $reviewer->getNotBlockedArticles();
    $messages = array();
    $db = Meeting::openDB();
    $i = 0;
    $result = $db->query('SELECT * FROM messages WHERE articleNumber<>"None" AND trashed<>"true" ORDER BY articleNumber, date DESC');
    while($db_row = $result->fetchArray()) {
      /* Check if the reviewer can see the messages concerning this article */
      if(!is_null(array_search($db_row['articleNumber'], $authorizedArticlesNumbers))) { 
	$messages[$i] = new Meeting();
	$messages[$i]->createFromDB($db_row);
	$i++;
      }
    }
    return $messages;
  }

  static function getAllTrashedMessages() {
    $messages = array();
    $db = Meeting::openDB();
    $i = 0;
    $result = $db->query('SELECT * FROM messages WHERE trashed="true" ORDER BY date DESC');
    while($db_row = $result->fetchArray()) {
      $messages[$i] = new Meeting();
      $messages[$i]->createFromDB($db_row);
      $i++;
    }
    return $messages;
  }

  static function getAllNonTrashedAuthorizedArticleMessagesInDoubleArrayIndexedByArticleNumber($reviewer) {
    $authorizedArticlesNumbers = $reviewer->getNotBlockedArticles();
    $messages = array();
    $bozo = array();
    $db = Meeting::openDB();
    $i = 0;
    $result = $db->query('SELECT * FROM messages WHERE articleNumber<>"None" AND trashed<>"true" ORDER BY articleNumber, date DESC');
    while($db_row = $result->fetchArray()) {
      /* Check if the reviewer can see the messages concerning this article */
      if(!is_null(array_search($db_row['articleNumber'], $authorizedArticlesNumbers))) { 
	$messages[$i] = new Meeting();
	$messages[$i]->createFromDB($db_row);
	$i++;
      }
    }
    if(count($messages) == 0) {
      return $messages;
    }

    $message = $messages[0];
    $previousArticleNumber = $message->getArticleNumber();
    $i = 0;

    foreach($messages as $message) {
      $currentArticleNumber = $message->getArticleNumber();
      if($currentArticleNumber != $previousArticleNumber) {
	$i = 0;
	$previousArticleNumber = $currentArticleNumber;
      }
      $bozo[$currentArticleNumber][$i] = $message;
      $i++;
    }
    return $bozo;
  }
  
  static function getAllNonTrashedMessagesInDoubleArrayIndexedByArticleNumber() {
    $messages = array();
    $bozo = array();
    $db = Meeting::openDB();
    $i = 0;
    $result = $db->query('SELECT * FROM messages WHERE articleNumber<>"None" AND trashed<>"true" ORDER BY articleNumber, date DESC');
    while($db_row = $result->fetchArray()) {
      $messages[$i] = new Meeting();
      $messages[$i]->createFromDB($db_row);
      $i++;
    }
    if(count($messages) == 0) {
      return $messages;
    }

    $message = $messages[0];
    $previousArticleNumber = $message->getArticleNumber();
    $i = 0;

    foreach($messages as $message) {
      $currentArticleNumber = $message->getArticleNumber();
      if($currentArticleNumber != $previousArticleNumber) {
        $i = 0;
        $previousArticleNumber = $currentArticleNumber;
      }
      $bozo[$currentArticleNumber][$i] = $message;
      $i++;
    }
    return $bozo;
  }



  static function getNonTrashedAuthorizedArticleMessages($articleNumber) {
    $messages = array();
    $db = Meeting::openDB();
    $i = 0;
    $result = $db->query('SELECT * FROM messages WHERE articleNumber="'. $articleNumber .'" AND trashed<>"true" ORDER BY date DESC');
    while($db_row = $result->fetchArray()) {
      $messages[$i] = new Meeting();
      $messages[$i]->createFromDB($db_row);
      $i++;
    }
    return $messages;
  }
  

}

?>
