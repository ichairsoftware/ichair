<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
class Reviewer {

  public static $CHAIR_GROUP = "chair";
  public static $OBSERVER_GROUP = "observer";
  public static $REVIEWER_GROUP = "reviewer";

  public static $DEFAULT_NUMBER_OF_ARTICLES_PER_PAGE = 20;

  public static $ALLOWED_NONE = "false";
  public static $ALLOWED_ALL = "true";
  public static $ALLOWED_UNASSIGNED = "unassigned";
  public static $ALLOWED_FINISHED = "finished";

  public static $NOTIFY_NEVER = "notifynever";
  public static $NOTIFY_SUBSCRIBED = "notifysubscribed";
  public static $NOTIFY_ALWAYS = "notifyalways";

  public static $AUTOSUBSCRIBE = "auto";
  public static $MANUALSUBSCRIBE = "manual";

  /* The following constants are used to parse the sortKeyString */

  static $SORT_BOOL_REVERSE = 0x0001;
  static $SORT_BOOL_EXTEND = 0x0002;    /* See an extended view */
  static $SORT_BOOL_RESTRICT = 0x0004;  /* Only show reviewed, assigned or commented articles */
  static $SORT_BOOL_SNAPSHOT = 0x0008;  /* For chair: sort according to snapshot instead of current average grades */
  static $SORT_BOOL_RECENT_ONLY = 0x0010;  /* only show when lastModificationDate is more recent than lastVisitDate */
  static $SORT_BOOL_SHOW_ASSIGNED = 0x0020; 
  static $SORT_BOOL_BY_ARTICLE_NUMBER = 0x0040; 
  static $SORT_BOOL_SHOW_PREFERRED = 0x0080;

  public static $SORT_BY_ARTICLE_NUMBER = 0x0100;
  public static $SORT_BY_OVERALL_GRADE = 0x0200;
  public static $SORT_BY_ACCEPTANCE_STATUS = 0x0300;
  public static $SORT_BY_ACCEPTANCE_STATUS_AND_GRADE = 0x0400;
  public static $SORT_BY_LAST_MODIFICATION = 0x0500;
  static $SORT_MASK = 0x0F00;

  static $SORT_MASK_PREF = 0xF000;
  public static $SORT_BY_ARTICLE_NUMBER_PREF = 0x1000;
  public static $SORT_BY_CATEGORY = 0x2000;
  public static $SORT_BY_AFFINITY = 0x3000;

  private $reviewerNumber;        /* Unique reviewer number */
  private $login;                 /* Unique reviewer login */
  private $fullName;
  private $group;                 /* Group to which the review belongs: chair, observer, or anything else for reviewers */
  private $email;                 /* email of the reviewer */
  private $passwordHash;
  private $status = "";
  private $numberOfArticles;
  private $isAllowedToViewOtherReviews;
  private $sortKeyString;         /* This is used to store the preferences of a reviewer about the ways the different reviews are shown to him/her */
  private $lastVisitDate;
  private $preferedCategory;
  private $mailNotification;
  private $autoSubscription;
  private $numberOfArticlesPerPage;

  function __construct(){}

  function createFromPOST() {

    /* If the database does not exist, create an empty one */
    Reviewer::openDB();

    /* Before the object is actually created, the arguments must be tested */
    $this->checkReviewerPost();

    if ($this->status == ""){
      
      $this->login = Tools::readPost('login');
      $this->fullName = Tools::readPost('fullName');
      $this->group = Tools::readPost('group');
      $this->email = Tools::readPost('email');
      $this->reviewerNumber = Reviewer::getNewReviewerNumber();
      $this->passwordHash = "";
      $this->numberOfArticles = "";
      $this->isAllowedToViewOtherReviews = Reviewer::$ALLOWED_NONE;
      $this->lastVisitDate = 0;
      $this->preferedCategory = "";
      $this->mailNotification = Reviewer::$NOTIFY_SUBSCRIBED;
      $this->autoSubscription = Reviewer::$MANUALSUBSCRIBE;
      $this->numberOfArticlesPerPage = Reviewer::$DEFAULT_NUMBER_OF_ARTICLES_PER_PAGE;
      $this->sortKeyString = Reviewer::$SORT_BY_OVERALL_GRADE;

      $this->insertInDB();

      /* Once the user has been created, create an assignement for each article of the database. By default, this user */
      /* is blocked for all articles until the chair decides to assign or to authorize some article. */
      
      $this->createAllAssignements(); 

    }

  }

  function updateFromPOST() {

    /* If the database does not exist, create an empty one */
    Reviewer::openDB();

    /* Before the object is actually updated, the email must be tested */
    if(!Tools::isValidEmail(Tools::readPost('email'))) {
      $this->status .= "Please provide a valid email address for this reviewer (either a single email address, or a comma separated list of addresses).\n";
    }

    /* Check that reviewer that we want to update actually exists in the database */
    if(is_null(Reviewer::getByReviewerNumber(Tools::readPost('reviewerNumber')))) {
      $this->status .= "Cannot find any reference to this reviewer in the database.\n";
    }

    if ($this->status == ""){
      
      $this->login = Tools::readPost('login');
      $this->fullName = Tools::readPost('fullName');
      $this->group = Tools::readPost('group');
      $this->email = Tools::readPost('email');

      $this->updateInDB();

    }
  }

  static function createDB() {
    $db = new SQLite3(Tools::getConfig("server/reviewsPath")."reviewers.db");
    $db->exec("CREATE TABLE reviewers(reviewerNumber INTEGER PRIMARY KEY, login, fullName, ".
	       "groupp, email, passwordHash, numberOfArticles, isAllowedToViewOtherReviews, sortKeyString, ".
	       "lastVisitDate, preferedCategory, mailNotification, autoSubscription, numberOfArticlesPerPage)");
  }

  static function openDB() {
    $dbfile = Tools::getConfig("server/reviewsPath")."reviewers.db";
    if (!file_exists($dbfile)){
      Reviewer::createDB();
    }
    return new SQLite3($dbfile);
  }

  function updateInDB() {
      $db = Reviewer::openDB();
      $db->exec("UPDATE reviewers SET " . 
		 "login=\"" . base64_encode($this->login) . "\", " . 
		 "fullName=\"" . base64_encode($this->fullName) . "\", " . 
		 "groupp=\"" . base64_encode($this->group) . "\", " . 
		 "email=\"" . base64_encode($this->email) . "\", " . 
		 "numberOfArticles=\"" . base64_encode($this->numberOfArticles) . "\", " . 
		 "isAllowedToViewOtherReviews=\"" . base64_encode($this->isAllowedToViewOtherReviews) . "\", " . 
		 "sortKeyString=\"" . base64_encode($this->sortKeyString) . "\", " . 
		 "lastVisitDate=\"" . base64_encode($this->lastVisitDate) . "\", " . 
		 "preferedCategory=\"" . base64_encode($this->preferedCategory) . "\", " . 
		 "mailNotification=\"" . $this->mailNotification . "\", " . 
		 "autoSubscription=\"" . $this->autoSubscription . "\", " . 
		 "numberOfArticlesPerPage=\"" . base64_encode($this->numberOfArticlesPerPage) .  "\" " .
		 "WHERE reviewerNumber=\"" . $this->reviewerNumber . "\"");
  }

  function insertInDB() {
    $db = Reviewer::openDB();
    $db->exec("INSERT INTO reviewers VALUES (\"" 
	       . $this->reviewerNumber . "\", \"" 
	       . base64_encode($this->login) . "\", \"" 
	       . base64_encode($this->fullName) . "\", \"" 
	       . base64_encode($this->group) . "\", \"" 
	       . base64_encode($this->email) . "\", \"" 
	       . base64_encode($this->passwordHash) . "\", \"" 
	       . base64_encode($this->numberOfArticles) . "\", \"" 
	       . base64_encode($this->isAllowedToViewOtherReviews) . "\", \"" 
	       . base64_encode($this->sortKeyString) . "\", \"" 
	       . base64_encode($this->lastVisitDate) . "\", \"" 
	       . base64_encode($this->preferedCategory) . "\", \"" 
	       . $this->mailNotification . "\", \"" 
	       . $this->autoSubscription . "\", \"" 
	       . base64_encode($this->numberOfArticlesPerPage) . "\")");
  }

  static function removeFromDB($reviewerNumber) {
    $db = Reviewer::openDB();
    $db->exec("DELETE FROM reviewers WHERE reviewerNumber=\"" . $reviewerNumber . "\"");
  }

  function createFromDB($db_row) {
    $this->reviewerNumber = $db_row['reviewerNumber'];
    $this->login = base64_decode($db_row['login']);
    $this->fullName = base64_decode($db_row['fullName']);
    $this->group = base64_decode($db_row['groupp']);
    $this->email = base64_decode($db_row['email']);
    $this->passwordHash = base64_decode($db_row['passwordHash']);
    $this->numberOfArticles = base64_decode($db_row['numberOfArticles']);
    $this->isAllowedToViewOtherReviews = base64_decode($db_row['isAllowedToViewOtherReviews']);
    $this->sortKeyString = base64_decode($db_row['sortKeyString']);    
    $this->lastVisitDate = base64_decode($db_row['lastVisitDate']);    
    $this->preferedCategory = base64_decode($db_row['preferedCategory']);
    $this->mailNotification = $db_row['mailNotification'];
    $this->autoSubscription = $db_row['autoSubscription'];
    $this->numberOfArticlesPerPage = base64_decode($db_row['numberOfArticlesPerPage']);
  }

  function createDummy() {
    $this->reviewerNumber = rand() % 20;
    $this->login = "dummyLogin";
    $this->fullName = "Dummy Reviewer";
    $this->group = Reviewer::$REVIEWER_GROUP;
    $this->email = "dummy@reviewer.com";
    $this->passwordHash = "";
    $this->numberOfArticles = "";
    $this->isAllowedToViewOtherReviews = Reviewer::$ALLOWED_ALL;
    $this->sortKeyString = "";
    $this->lastVisitDate = "";
    $this->preferedCategory = "";
    $this->mailNotification = Reviewer::$NOTIFY_SUBSCRIBED;
    $this->autoSubscription = Reviewer::$MANUALSUBSCRIBE;
    $this->numberOfArticlesPerPage = 20;
  }

  function checkReviewerPost() {
    $db = Reviewer::openDB();
    $result = $db->querySingle("SELECT count(*) FROM reviewers WHERE login = \"" . base64_encode(Tools::readPost('login')) . "\"");
    if ($result != 0) {
      $this->status .= "The login you chose already exists in the database. Please chose another one.\n";
    }
    if(!Tools::isValidEmail(Tools::readPost('email'))) {
      $this->status .= "Please provide a valid email address for this reviewer (either a single email address, or a comma separated list of addresses).\n";
    }
  }

  function createAllAssignements() {
    $articles = Article::getAllArticles();
    foreach($articles as $article) {
      if(!is_null($article)) {
	/* create an assignement for this reviewer with the current article */
	$assignement = new Assignement();
	$assignement->createFromScratch($article->getArticleNumber(), $this->getReviewerNumber());
      }
    }
  }

  /* Returns a reviewer instance, given its number */

  static function getByReviewerNumber($reviewerNumber) {
    if(!preg_match("/[0-9]*/",$reviewerNumber)) {
      return;
    }
    $db = Reviewer::openDB();
    $result = $db->querySingle("SELECT * FROM reviewers WHERE reviewerNumber = \"" . $reviewerNumber . "\"", true);
    if (empty($result)) {
      return null;
    } else {
      $bozo = new Reviewer();
      $bozo->createFromDB($result);
      return $bozo;
    }    
  }

  /* Returns a reviewer instance, given its login */

  static function getByLogin($login) {
    $db = Reviewer::openDB();
    $result = $db->querySingle('SELECT * FROM reviewers WHERE login = "' . base64_encode($login) . '"', true);
    if (empty($result)) {
      return null;
    } else {
      $bozo = new Reviewer();
      $bozo->createFromDB($result);
      return $bozo;
    }    
  }

  function getStatus() {
    return $this->status;
  }

  function getPasswordHash() {
    return $this->passwordHash;
  }

  function setPasswordHash($passwordHash) {
    $this->passwordHash = $passwordHash;
  }

  function getNumberOfArticles() {
    return $this->numberOfArticles;
  }

  function setNumberOfArticles($numberOfArticles) {
    $this->numberOfArticles = $numberOfArticles;
    $this->updateInDB();
  }

  function setLastVisitDate($date) {
    if(!preg_match("/^[0-9]+$/", $date)) {
      return false;
    }
    $dates = explode(' ',$this->lastVisitDate);
    if (count($dates) == 1) {
      $this->lastVisitDate = $date;
    } else {
      $this->lastVisitDate = $date . ' ' . $dates[1];
    }
    $this->updateInDB();
    return true;
  }

  function getLastVisitDate() {
    $dates = explode(' ',$this->lastVisitDate);
    return $dates[0];
  }

  function autoUpdateVisitDate() {
    $dates = explode(' ',$this->lastVisitDate);
    if (count($dates) == 2) {
      $currentTime = gmdate('U');
      if (($currentTime - $dates[1]) > (Tools::getConfig('miscellaneous/visitTimeout') * 60)) {
	$this->lastVisitDate = $dates[1] . ' ' . $currentTime;
      } else {
        $this->lastVisitDate = $dates[0] . ' ' . $currentTime;
      }
      $this->updateInDB();
    }
  }

  function useAutoVisitDate() {
    return (count(explode(' ',$this->lastVisitDate)) == 2);
  }

  function setAutoVisitDate($autoVisit) {
    $dates = explode(' ',$this->lastVisitDate);
    if ($autoVisit) {
      $this->lastVisitDate = $dates[0] . ' ' . gmdate('U');
    } else {
      $this->lastVisitDate = $dates[0];
    }
  }

  function setNumberOfArticlesPerPage($nbr) {
    $this->numberOfArticlesPerPage = $nbr;
    $this->updateInDB();
  }

  function getNumberOfArticlesPerPage() {
    return $this->numberOfArticlesPerPage;
  }
  
  function getGroup() {
    return $this->group;
  }

  function isChair() {
    return ($this->group == "chair");
  }

  function isObserver() {
    return ($this->group == "observer");
  }

  function getReviewerNumber() {
    return $this->reviewerNumber;
  }

  function getSortKeyString() {
    return $this->sortKeyString;
  }

  function printPasswordSet() {
    if($this->passwordHash == "") {
      print("No");
    } else {
      print("Yes");
    }
  }

  function isPasswordSet() {
    if($this->passwordHash == "") {
      return false;
    } 
    return true;
  }

  /* $sortByKey should be either $SORT_BY_ARTICLE_NUMBER, or $SORT_BY_OVERALL_GRADE, or... */
  /* All the other variables are booleans */

  function setSortKeyString($sortByKey, $reverse, $extended, $restrict, $recent, $useSnapshot) {

    $this->sortKeyString &= ~(Reviewer::$SORT_BOOL_REVERSE | Reviewer::$SORT_BOOL_EXTEND | Reviewer::$SORT_BOOL_RESTRICT | Reviewer::$SORT_BOOL_RECENT_ONLY | Reviewer::$SORT_BOOL_SNAPSHOT | Reviewer::$SORT_MASK);
    $this->sortKeyString |= $sortByKey;
    if($reverse) {
      $this->sortKeyString |= Reviewer::$SORT_BOOL_REVERSE;
    }
    if($extended) {
      $this->sortKeyString |= Reviewer::$SORT_BOOL_EXTEND;
    }
    if($restrict) {
      $this->sortKeyString |= Reviewer::$SORT_BOOL_RESTRICT;
    }
    if($recent) {
      $this->sortKeyString |= Reviewer::$SORT_BOOL_RECENT_ONLY;
    }
    if($useSnapshot) {
      $this->sortKeyString |= Reviewer::$SORT_BOOL_SNAPSHOT;
    }
    
    $this->updateInDB();

  }

  function setSortKeyStringForPreferences($sortByKey) {
    $this->sortKeyString &= ~Reviewer::$SORT_MASK_PREF; /* reset all the bits that interest us */
    $this->sortKeyString |= $sortByKey;
    $this->updateInDB();
  }

  function setSortKeyStringForReviews($showAssignedOnly, $showPreferred, $sortByArticleNumber, $reverse) {
    $this->sortKeyString &= ~(Reviewer::$SORT_BOOL_REVERSE | Reviewer::$SORT_BOOL_SHOW_PREFERRED | Reviewer::$SORT_BOOL_SHOW_ASSIGNED | Reviewer::$SORT_BOOL_BY_ARTICLE_NUMBER); /* reset all the bits that interest us */
    if($reverse) {
      $this->sortKeyString |= Reviewer::$SORT_BOOL_REVERSE;
    }
    if($showPreferred) {
      $this->sortKeyString |= Reviewer::$SORT_BOOL_SHOW_PREFERRED;
    }
    if($showAssignedOnly) {
      $this->sortKeyString |= Reviewer::$SORT_BOOL_SHOW_ASSIGNED;
    }
    if($sortByArticleNumber) {
      $this->sortKeyString |= Reviewer::$SORT_BOOL_BY_ARTICLE_NUMBER;
    }
    $this->updateInDB();
  }

  function sortUseSnapshot() {
    return (($this->sortKeyString & Reviewer::$SORT_BOOL_SNAPSHOT) != 0);
  }

  function sortIsReversed() {
    return (($this->sortKeyString & Reviewer::$SORT_BOOL_REVERSE) != 0);
  }

  function sortIsExtended() {
    return (($this->sortKeyString & Reviewer::$SORT_BOOL_EXTEND) != 0);
  }

  function sortIsRecentOnly() {
    return (($this->sortKeyString & Reviewer::$SORT_BOOL_RECENT_ONLY) != 0);
  }

  function sortIsRestricted() {
    return (($this->sortKeyString & Reviewer::$SORT_BOOL_RESTRICT) != 0);
  }

  function getSortKey() {
    return ($this->sortKeyString & Reviewer::$SORT_MASK);
  }

  function getSortKeyPreferred() {
    return ($this->sortKeyString & Reviewer::$SORT_MASK_PREF);
  }

  function sortIsAssignedOnly() {
    return (($this->sortKeyString & Reviewer::$SORT_BOOL_SHOW_ASSIGNED) == 0);
  }

  function sortIsAlsoPreferred() {
    return (($this->sortKeyString & Reviewer::$SORT_BOOL_SHOW_PREFERRED) != 0);
  }

  function sortIsByArticleNumber() {
    return (($this->sortKeyString & Reviewer::$SORT_BOOL_BY_ARTICLE_NUMBER) == 0);
  }

  static function getGroupByLogin() {
    $login = $_SERVER['PHP_AUTH_USER'];
    $db = Reviewer::openDB();
    $result = $db->querySingle("SELECT * FROM reviewers WHERE login = \"" . base64_encode($login) . "\"", true);
    if(!empty($result)) {
      return base64_decode($result['groupp']);
    }
    return;
  }

  static function getReviewerByLogin() {
    $login = $_SERVER['PHP_AUTH_USER'];
    $db = Reviewer::openDB();
    $result = $db->querySingle("SELECT * FROM reviewers WHERE login = \"" . base64_encode($login) . "\"", true);
    if(!empty($result)) {
      $reviewer = new Reviewer();
      $reviewer->createFromDB($result);
      return $reviewer;
    }
    return;
  }

  function generateAndEmailPassword() {
    $password = md5(time() . getmypid() . rand());
    $salt = '$1$' . substr($password,0,8) .'$';
    $password = substr($password,8,16);
    $this->passwordHash = crypt($password, $salt);
    /* send mail to the reviewer */
    $subject = Tools::getConfig("mail/passwordSubject");
    $message = Tools::getConfig("mail/passwordMessage");
    $message = Tools::parseMailForPassword($message, $this, $password);
    $subject = Tools::parseMailToCommittee($subject, $this);
    if(!$this->sendMail($subject, $message)) {
      return false;
    }
    /* update passwordHash in database */
    $this->updatePasswordHashInDB();
    return true;
  }

  function updatePasswordHashInDB() {
    $db = Reviewer::openDB();
    $db->exec("UPDATE reviewers SET " . 
	       "passwordHash=\"" . base64_encode($this->passwordHash) .  "\" " .
	       "WHERE reviewerNumber=\"" . $this->reviewerNumber . "\"");
    /* write the .htpasswd file */
    Reviewer::writeHtpasswdFile();
  }

  static function writeHtpasswdFile() {
    /* when no previous .htaccess file exists, create a new one */
    if(!file_exists("../review/.htaccess")) {
      $htaccessContents = 'AuthUserFile ' . realpath("../review") . "/.htpasswd\n";
      $htaccessContents .= 'AuthName Reviewer
AuthType Basic
require valid-user
Allow from all';
      file_put_contents("../review/.htaccess", $htaccessContents . "\n");
    }
    if(file_exists("../review/.htpasswd")) {
      copy("../review/.htpasswd", "../review/.htpasswd.bak");
    }
    $file = fopen("../review/.htpasswd.new", "w");
    foreach(Reviewer::getAllReviewers() as $reviewer) {
      if($reviewer->isPasswordSet()) {
	fprintf($file, $reviewer->login . ":" . $reviewer->passwordHash . "\n"); 
      }
    }
    fclose($file);
    rename("../review/.htpasswd.new", "../review/.htpasswd");
  }

  /* Return an array containing all the reviewers*/

  static function getAllReviewers() {
    $reviewers=array();
    $i=0;
    $db = Reviewer::openDB();
    $result = $db->query("SELECT * FROM reviewers");
    while ($db_row = $result->fetchArray()){
      $nr = new Reviewer();
      $nr->createFromDB($db_row);
      $reviewers[$i] = $nr;
      $i++;
    }
    return $reviewers;
  }

  /* Return a table of reviewer names indexed by the reviewer number */
  static function getAllReviewersSparse() {
    $reviewers=array();
    $db = Reviewer::openDB();
    $result = $db->query("SELECT * FROM reviewers");
    while ($db_row = $result->fetchArray()){
      $nr = new Reviewer();
      $nr->createFromDB($db_row);
      $reviewers[$nr->getReviewerNumber()] = $nr;
    }
    return $reviewers;
  }

  /* Return an array containing all the reviewers that are not observers */

  static function getAllActiveReviewers() {
    $reviewers=array();
    $i=0;
    $db = Reviewer::openDB();
    $result = $db->query('SELECT * FROM reviewers WHERE groupp <> "' . base64_encode(REVIEWER::$OBSERVER_GROUP) . '"');
    while ($db_row = $result->fetchArray()){
      $nr = new Reviewer();
      $nr->createFromDB($db_row);
      $reviewers[$i] = $nr;
      $i++;
    }
    return $reviewers;
  }

  /* Return the total number of reviewers that are not observer */

  static function getNumberOfActiveReviewers() {
    $db = Reviewer::openDB();
    $result = $db->querySingle('SELECT COUNT("reviewerNumber") FROM reviewers WHERE groupp <> "' . base64_encode(REVIEWER::$OBSERVER_GROUP) . '"');
    if(!is_null($result)){
      return $result;
    }
    return 0;
  }


  /* Return an array containing all the reviewers belonging to the given group */

  static function getAllByGroup($group) {
    $reviewers=array();
    $i=0;
    $db = Reviewer::openDB();
    $result = $db->query("SELECT * FROM reviewers WHERE groupp=\"" . base64_encode($group) . "\"");
    while ($db_row = $result->fetchArray()){
      $nr = new Reviewer();
      $nr->createFromDB($db_row);
      $reviewers[$i] = $nr;
      $i++;
    }
    return $reviewers;
  }

  function getLogin() {
    return $this->login;
  }
  
  function getEmail() {
    return $this->email;
  }

  function getFullName() {
    return $this->fullName;
  }

  function setFullName($fullName) {
    $this->fullName = $fullName;
  }

  function getPreferedCategory() {
    return $this->preferedCategory;
  }

  function setPreferedCategories($preferedCategory) {
    $this->preferedCategory = $preferedCategory;
  }

  function setEmail($email) {
    $this->email = $email;
  }
    
  function sendMail($subject, $message) {
    $headers = "From: " . Tools::getConfig("mail/reviewersFrom") . "\r\n" .
      "Reply-To: " . Tools::getConfig("mail/reviewersFrom") . "\r\n" .
      "Bcc: " . Tools::getConfig("mail/admin");
    if(Tools::useGlobalBCC()) {
      $headers .= "," . Tools::getConfig("mail/globalBCC") . "\r\n";
    } else {
      $headers .= "\r\n";
    }
    $headers .= 'X-Mailer: PHP/' . phpversion();
    return mail($this->email, $subject, $message, $headers, "-f ".Tools::getConfig("mail/reviewersFrom"));
  }

  static function getNewReviewerNumber() {
    $db = Reviewer::openDB();
    $result = $db->querySingle("SELECT reviewerNumber FROM reviewers ORDER BY reviewerNumber DESC");
    if(is_null($result)) {
      return 0;
    }
    return $result+1;
  }

  /* Returns a list of all the number of the articles that this reviewer can see */
  function getNotBlockedArticles() {
    $articleNumbers = array();
    $i = 0;
    $db = Assignement::openDB();
    $result = $db->query("SELECT articleNumber FROM assignements WHERE " . 
			 "reviewerNumber = \"" . $this->reviewerNumber . "\" AND " . 
			 "assignementStatus <> \"" . Assignement::$BLOCKED . "\" " . 
			 "ORDER BY articleNumber");
    while($db_row = $result->fetchArray()) {
      $articleNumbers[$i] = $db_row['articleNumber'];
      $i++;
    }
    return $articleNumbers;
  }

  /* Returns a list of all the number of the articles that this reviewer can see, limited to $numberOfArticles articles, containing the article $articleNumber  */
  function getRangeOfNotBlockedArticles($articleNumber) {
    $articleNumbers = array();
    $db = Assignement::openDB();
    $result = $db->querySingle('SELECT COUNT(*) FROM assignements WHERE ' . 
			       'reviewerNumber = "' . $this->reviewerNumber . '" AND ' .
			       'assignementStatus <> "' . Assignement::$BLOCKED . '" AND ' .
			       'articleNumber < "' . $articleNumber .'"');
    $offset = floor($result / $this->numberOfArticlesPerPage) * $this->numberOfArticlesPerPage;
    $result = $db->query("SELECT articleNumber FROM assignements WHERE " . 
			 "reviewerNumber = \"" . $this->reviewerNumber . "\" AND " . 
			 "assignementStatus <> \"" . Assignement::$BLOCKED . "\" " . 
			 "ORDER BY articleNumber LIMIT " . $this->numberOfArticlesPerPage . ' OFFSET ' . $offset );
    $i = 0;
    while($db_row = $result->fetchArray()) {
      $articleNumbers[$i] = $db_row['articleNumber'];
      $i++;
    }
    return $articleNumbers;    
  }

  /* Return a list of article numbers for a given reviewer and a given category */
  function getNotBlockedArticlesByCategory($category) {
    $articleNumbers = array();
    $notblocked = $this->getNotBlockedArticles();
    $articles = Article::getAllArticles();
    $i = 0;
    foreach($notblocked as $notblockedArticleNumber) {
      $article = $articles[$notblockedArticleNumber];
      if($article->getCategory() == $category) {
	$articleNumbers[$i] = $notblockedArticleNumber;
	$i++;
      }
    }
    return $articleNumbers;
  }

  /* return a list of article numbers corresponding to assigned articles for which the review has a given status */

  function getAssignedArticlesByStatus($reviewStatus) {
    $articleNumbers = array();
    $i = 0;
    $db= Assignement::openDB();
    $result = $db->query("SELECT articleNumber FROM assignements WHERE " .
                         "reviewerNumber = \"" . $this->reviewerNumber . "\" AND " .
                         "assignementStatus = \"" . Assignement::$ASSIGNED . "\" AND " .
                         "reviewStatus = \"" . $reviewStatus . "\" " .
                         "ORDER BY articleNumber");
    while($db_row = $result->fetchArray()) {
      $articleNumbers[$i] = $db_row['articleNumber'];
      $i++;
    }
    return $articleNumbers;
  }

  /* return a list of article numbers corresponding to NOT assigned articles for which the review has a given status */

  function getAuthorizedArticlesByStatus($reviewStatus) {
    $articleNumbers = array();
    $i = 0;
    $db= Assignement::openDB();
    $result = $db->query("SELECT articleNumber FROM assignements WHERE " .
                         "reviewerNumber = \"" . $this->reviewerNumber . "\" AND " .
                         "assignementStatus = \"" . Assignement::$AUTHORIZED . "\" AND " .
                         "reviewStatus = \"" . $reviewStatus . "\" " .
                         "ORDER BY articleNumber");
    while($db_row = $result->fetchArray()) {
      $articleNumbers[$i] = $db_row['articleNumber'];
      $i++;
    }
    return $articleNumbers;
  }


  /* Returns a list of all the number of the articles that are $status to this reviewer, where $status can be */
  /* either assigned, authorized, or blocked */

  function getArticlesByStatus($status) {
    $articleNumbers = array();
    $i = 0;
    $db = Assignement::openDB();
    $result = $db->query('SELECT articleNumber FROM assignements WHERE ' . 
			 'reviewerNumber = "' . $this->reviewerNumber . '" AND ' . 
			 'assignementStatus = "' . $status . '" ' . 
			 'ORDER BY articleNumber');
    while($db_row = $result->fetchArray()) {
      $articleNumbers[$i] = $db_row['articleNumber'];
      $i++;
    }
    return $articleNumbers;
  }

  function printInfo() {
    if ($this->group == Reviewer::$CHAIR_GROUP) {
      print('<div class="floatRight bigNumber">chair</div>');
    } else if ($this->group == Reviewer::$REVIEWER_GROUP) {
      print('<div class="floatRight bigNumber">reviewer</div>');
    } else {
      print('<div class="floatRight bigNumber">observer</div>');
    }
    print("Login: ".htmlentities($this->login, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')."<br />");
    print("E-mail: ".htmlentities($this->email, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')."<br />");
  }

  function printLong() {
    print('<div class="versionTitle">Reviewer Name: ' . $this->getFullName() . '</div>');
    print('<div class="versionAuthors">E-mail: ' . $this->getEmail() . '</div>');
    print('Group: ' . $this->getGroup() . '<br/>');
    print('Login: ' . $this->getLogin() . '<br/>');
  }


  /* print a table with the number,titles, category of the articles having the given $status for this reviewer. */
  /* $status can be either assigned, authorized, or blocked */

  function printArticlesByStatus($status, $allArticles) {
    $allcategories;
    if (Tools::useCategory()) {
      $allcategories = Tools::getAllCategories();
    }
                      
    print('<div class="articlesList">');
    $listOfArticles = $this->getArticlesByStatus($status);
    if($status == ASSIGNEMENT::$ASSIGNED) {
      print("List of Assigned Articles:<br />");
    } else if($status == ASSIGNEMENT::$AUTHORIZED) {
      print("List of Authorized Articles:<br />");
    } else if($status == ASSIGNEMENT::$BLOCKED) {
      print("List of Blocked Articles:<br />");
    } else {
      print('</div>');
      return;
    }
    /* If there is no article with this status, print "None." */
    if(count($listOfArticles) == 0) {
      print('</div>');
      print('<div class="articlesListTable"><en>None.</em></div>');
      return;
    }
    /* If the list includes all articles, print "All." */
    if(count($listOfArticles) == Article::getNumberOfArticles()) {
      print('</div>');
      print('<div class="articlesListTable red"><em>All Articles.</em></div>');
      return;
    }    
    print('<table class="articlesListTable">' . "\n");
    print('<tr>' . "\n");
    print('<td>Number and Title</td>');
    print('<td>Authors</td>');
    if(Tools::useCategory()) {
      print('<td>Category</td>');
    }
    print('</tr>' . "\n");
    foreach($listOfArticles as $articleNumber) {
      $article = $allArticles[$articleNumber];
      if(!is_null($article)) {
	print('<tr>' . "\n");
	$title = $article->getTitle();
	print('<td>' . $article->getArticleNumber() . ": " . Tools::HTMLsubstr($article->getTitle(),30) . '</td>');
	print('<td>' . Tools::HTMLsubstr($article->getAuthors(),30) . '</td>');
	if(Tools::useCategory()) {
	  print('<td>' . htmlentities($allcategories[$article->getCategory()], ENT_COMPAT | ENT_HTML401, 'ISO-8859-1') . '</td>');
	}
	print('</tr>' . "\n");
      }
    }
    print('</table>');
    print('</div>');
  }

  /* This function is used in assign_list.php to change the max total number of articles per reviewer */

  function printAssignedRatio() {
    $currentNumberOfArticles = $this->getNumberOfAssignedArticles();
    print('<div class="floatRightClear">');
    print('<form action="assign_articles_list.php#nbr' . $this->getReviewerNumber() . '" method="post">');
    print('<input type="hidden" name="reviewerNumber" value="' . $this->getReviewerNumber() . '" />');
    print('<table><tr><td>');
    if(($this->getNumberOfArticles() != "") && ($currentNumberOfArticles > $this->numberOfArticles)) {
      print('<div class="badRatio">');
    } else {
      print('<div class="goodRatio">');
    }
    print($currentNumberOfArticles . '/');
    print("</td>\n");
    print('<td><input type="text" class="largeInput" name="maxArticles" size="2" value="'.$this->numberOfArticles.'" /></td>');
    print('<td><input type="submit" name="update" class="buttonLink" value="Ok" /></td>');
    print('</tr>');
    print("</table>\n");
    print("</form>");
    print("</div>");
  }



  /* This function returns the number of articles assigned to this reviewer */
  function getNumberOfAssignedArticles() {
    $articleNumbers = array();
    $i = 0;
    $db = Assignement::openDB();
    $result = $db->querySingle("SELECT COUNT(\"articleNumber\") FROM assignements WHERE " . 
			 "reviewerNumber = \"" . $this->reviewerNumber . "\" AND " . 
			 "assignementStatus = \"" . Assignement::$ASSIGNED . "\" " . 
			 "ORDER BY articleNumber");
    if(!is_null($result)) {
      return $result;
    }
    return 0;
  }

  /* Return the number of the next reviewer (not an observer) in the database */
  function getNextActiveReviewer() {
    $db = Reviewer::openDB();
    $result = $db->querySingle('SELECT reviewerNumber FROM reviewers WHERE ' . 
			 'reviewerNumber > "' . $this->getReviewerNumber() . '"' . 
			 'AND groupp <> "' . base64_encode(Reviewer::$OBSERVER_GROUP) . '" ORDER BY reviewerNumber');
    return $result;
  }

  /* Return the number of the next observer in the database */
  function getNextPassiveReviewer() {
    $db = Reviewer::openDB();
    $result = $db->querySingle('SELECT reviewerNumber FROM reviewers WHERE ' . 
			 'reviewerNumber > "' . $this->getReviewerNumber() . '"' . 
			 'AND groupp = "' . base64_encode(Reviewer::$OBSERVER_GROUP) . '" ORDER BY reviewerNumber');
    return $result;
  }

  function getAffinityTotals() {
    $totals = array();
    $db = Assignement::openDB();
    $totals[Assignement::$WANTED] = $db->querySingle('SELECT count(affinity) FROM assignements WHERE affinity="'. Assignement::$WANTED . '" AND reviewerNumber="'. $this->reviewerNumber .'" AND assignementStatus<>"'.Assignement::$BLOCKED.'"');
    $totals[Assignement::$NOTWANTED] = $db->querySingle('SELECT count(affinity) FROM assignements WHERE affinity="'. Assignement::$NOTWANTED . '" AND reviewerNumber="'. $this->reviewerNumber .'" AND assignementStatus<>"'.Assignement::$BLOCKED.'"');
    $totals[Assignement::$DONTCARE] = $db->querySingle('SELECT count(affinity) FROM assignements WHERE affinity="'. Assignement::$DONTCARE . '" AND reviewerNumber="'. $this->reviewerNumber .'" AND assignementStatus<>"'.Assignement::$BLOCKED.'"');
    return $totals;
  }

  function printAffinityTotalsTable() {
    $totals = $this->getAffinityTotals();
    print('<table class="dottedTable"><tr>');
    print('<th class="topRow" colspan="3">Summary of your preferences regarding the reviews</th>');
    print('</tr><tr>');
    print('<th class="center">Not able</th><th class="center">Able / Not Willing</th><th class="center">Willing</th>');
    print('</tr><tr>');
    print('<td class="bigNumber voidReview center">' . $totals[Assignement::$NOTWANTED] . '</td>');
    print('<td class="bigNumber inProgressReview center">' . $totals[Assignement::$DONTCARE] . '</td>');
    print('<td class="bigNumber completedReview center">' . $totals[Assignement::$WANTED] . '</td>');
    print('</tr></table>');
  }

  function getReviewTotals() {
    $totals = array();
    $db = Assignement::openDB();
    $totals[Assignement::$VOID] = $db->querySingle('SELECT count(reviewStatus) FROM assignements WHERE reviewStatus="'. Assignement::$VOID . '" AND reviewerNumber="'. $this->reviewerNumber .'" AND assignementStatus="'. Assignement::$ASSIGNED .'"');
    $totals[Assignement::$INPROGRESS] = $db->querySingle('SELECT count(reviewStatus) FROM assignements WHERE reviewStatus="'. Assignement::$INPROGRESS . '" AND reviewerNumber="'. $this->reviewerNumber .'" AND assignementStatus="'. Assignement::$ASSIGNED .'"');
    $totals[Assignement::$COMPLETED] = $db->querySingle('SELECT count(reviewStatus) FROM assignements WHERE reviewStatus="'. Assignement::$COMPLETED . '" AND reviewerNumber="'. $this->reviewerNumber .'" AND assignementStatus="'. Assignement::$ASSIGNED .'"');
    return $totals;
  }

  function printReviewTotalsTable() {
    $totals = $this->getReviewTotals();
      print('<table class="dottedTable"><tr>');
      print('<th class="topRow" colspan="3">Your assigned reviews</th>');
      print('</tr><tr>');
      print('<th class="center">Not started</th><th class="center">In progress</th><th class="center">Completed</th>');
      print('</tr><tr>');
      print('<td class="bigNumber center voidReview">' . $totals[Assignement::$VOID] . '</td>');
      print('<td class="bigNumber center inProgressReview">' . $totals[Assignement::$INPROGRESS] . '</td>');
      print('<td class="bigNumber center completedReview">' . $totals[Assignement::$COMPLETED] . '</td>');
      print('</tr></table>');
  }

  function isAllowedToViewOtherReviews() {
    return ($this->isAllowedToViewOtherReviews != Reviewer::$ALLOWED_NONE);
  }

  function isAllowedToViewOtherReviewsForArticle($article) {
    if ($this->isAllowedToViewOtherReviews == Reviewer::$ALLOWED_ALL) {
      return true;
    }
    if ($this->isAllowedToViewOtherReviews == Reviewer::$ALLOWED_NONE) {
      return false;
    }
    $assignement = Assignement::getByNumbers($article->getArticleNumber(), $this->reviewerNumber);
    return (($this->isAllowedToViewOtherReviews == Reviewer::$ALLOWED_FINISHED) && ($assignement->getReviewStatus() == Assignement::$COMPLETED)) || ($assignement->getAssignementStatus() == Assignement::$AUTHORIZED);
  }

  function getAllowedToViewOtherReviews() {
    return $this->isAllowedToViewOtherReviews;
  }

  function setAllowedToViewOtherReviews($value) {
    if($value == Reviewer::$ALLOWED_ALL) {
      $this->isAllowedToViewOtherReviews = Reviewer::$ALLOWED_ALL;
    } else if($value == Reviewer::$ALLOWED_FINISHED) {
      $this->isAllowedToViewOtherReviews = Reviewer::$ALLOWED_FINISHED;
    } else if($value == Reviewer::$ALLOWED_UNASSIGNED) {
      $this->isAllowedToViewOtherReviews = Reviewer::$ALLOWED_UNASSIGNED;
    } else {
      $this->isAllowedToViewOtherReviews = Reviewer::$ALLOWED_NONE;
    }
    $this->updateInDB();
  }

  function sendMassMail($subject, $message) {
    $headers = "From: " . Tools::getConfig("mail/reviewersFrom") . "\r\n" .
      "Reply-To: " . Tools::getConfig("mail/reviewersFrom") . "\r\n";
    if(Tools::useGlobalBCC()) {
      $headers .= "Bcc: " . Tools::getConfig("mail/globalBCC") . "\r\n";
    }
    $headers .= 'X-Mailer: PHP/' . phpversion();
    if(!mail($this->getEmail(), $subject, $message, $headers, "-f ".Tools::getConfig("mail/reviewersFrom"))) {
      Log::logMailError($this->getEmail(), $subject, $message, $headers);
      return false;
    }
    return true;
  }


}

?>
