<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><h2>What is iChair?</h2>

<p>iChair is a powerful submission/review server software designed to help
the program chair of a conference with:</p>
<ul>
<li>submission collection</li>
<li>assignment of articles to reviewers</li>
<li>review collection</li>
<li>discussions</li>
<li>mailing to authors and reviewers</li>
<li>plenty of other things!</li>
</ul>

<p>iChair was developed with a view to being as easy to install as possible.
As a result, installation on a Linux box should be possible by anyone (even
with only basic knowledge of server administration) in about one or two hours.
It was developed in <a href="http://www.php.net">PHP</a> (version 5, now ported
to version 8) and intended to run on an 
<a href="http://www.apache.org">apache web server</a>.</p>


<h2>The developers</h2>

<p>iChair was developed in the third quarter of 2005 by 
<a href="http://www.baigneres.net">Thomas Baign&egrave;res</a>
and <a href="https://finiasz.net/">Matthieu Finiasz</a> 
at <a href="http://www.epfl.ch">EPFL</a>, 
<a href="http://lasecwww.epfl.ch">LASEC</a>. The first release of this software 
was intended for the Eurocrypt 2006 submission/review server. 
<a href="https://lasec.epfl.ch/people/vaudenay/">Serge Vaudenay</a> (program 
chair of this conference) wanted some new features to be added to some existing 
software: it appeared that re-programming everything from scratch was easier and 
much more fun!</p>

<div class="OKmessage">For more information, please visit the 
<a href="http://www.baigneres.net/ichair" target="_blank">iChair homepage</a>.</div>
