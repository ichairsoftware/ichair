<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Submission Form';
include 'utils/tools.php';
if(!Tools::isConfigured()) {return;}
include 'header.php';

/* Check that the author is still on time... */

if(Tools::serverIsShutdown()) {
  Tools::printServerShutdownMessage();
} else {

?>

<p>Use this form for new submissions only. A <a href="revise.php">Revision
Form</a> is available to modify a previous submission.</p>

<form action="submit_process.php" method="post" enctype="multipart/form-data">
  <h2>Submission Information</h2>
  Title:<br /><input name="title" type="text" size="80" value="<?php Tools::printHTML(Tools::readPost('title'));?>"/><br />
  <div id="authorsDiv">
  Authors (include ALL authors to appear in the final version):<a id="addAuthor" class="buttonLink" href="#">Add an author</a><br/>
  <?php
    if (array_key_exists('authors', $_POST)) {
      $posted_authors = $_POST['authors'];
    } else {
      $posted_authors = array('', '');
    }
    foreach($posted_authors as $j => $val) {
  ?>
  <div class="authorBox"><div class="authorBoxWidth">
  <a class="floatRight removeAuthor" href="#">Remove author</a>
  Full name:<br /><input name="authors[]" type="text" size="35"  value="<?php Tools::printHTML(Tools::readPost('authors', $j));?>"/>
  <?php if(Tools::useAffiliations()) {?>
    <br/>Affiliation: <br /><input name="affiliations[]" type="text" size="35"  value="<?php Tools::printHTML(Tools::readPost('affiliations', $j));?>"/>
  <?php }?>
  <?php if(Tools::useCountry()) {?>
    <br/>Country: <br /><?php Tools::printCountrySelect(Tools::readPost('country', $j)); ?>
  <?php } ?>
  </div></div>
  <?php } ?>
  </div>
  <div class="clear"></div>
  <table><tr><td>
  <input type="radio" class="noBorder" name="committeeMember" id="notCommittee" value="no"<?php 
    if (Tools::readPost('committeeMember') != "yes") {
      print(' checked="checked"');
    }
  ?> /></td><td><label for="notCommittee">None of the authors above is a member of the <?php Tools::printHTML(Tools::getConfig('conference/name'));?> program committee.</label></td></tr><tr><td>
  <input type="radio" class="noBorder" name="committeeMember" id="yesCommittee" value="yes"<?php 
    if (Tools::readPost('committeeMember') == "yes") {
      print(' checked="checked"');
    }
  ?> /></td><td><label for="yesCommittee">This paper involves a member of the <?php Tools::printHTML(Tools::getConfig('conference/name'));?> program committee.</label></td></tr>
  </table><br/>
  <?php if(Tools::useCustomCheck1()) {?>
  <table>
  <tr><td>
      <input type="checkbox" class="noBorder" name="customCheck1" id="customCheck1" value="yes"<?php
      if (Tools::readPost('customCheck1') == "yes") {
        print(' checked="checked"');
      }
  ?> /></td><td><label for="customCheck1"><?php Tools::printHTML(Tools::getCustomCheck1(true)); ?></label>
  </td></tr>
  </table><br/>
  <?php }?>
  <?php if(Tools::useCustomCheck2()) {?>
  <table>
  <tr><td>
      <input type="checkbox" class="noBorder" name="customCheck2" id="customCheck2" value="yes"<?php
      if (Tools::readPost('customCheck2') == "yes") {
        print(' checked="checked"');
      }
  ?> /></td><td><label for="customCheck2"><?php Tools::printHTML(Tools::getCustomCheck2(true)); ?></label>
  </td></tr>
  </table><br/>
  <?php }?>
  <?php if(Tools::useCustomCheck3()) {?>
  <table>
  <tr><td>
    <input type="checkbox" class="noBorder" name="customCheck3" id="customCheck3" value="yes"<?php
      if (Tools::readPost('customCheck3') == "yes") {
        print(' checked="checked"');
      }
  ?> /></td><td><label for="customCheck3"><?php Tools::printHTML(Tools::getCustomCheck3(true)); ?></label>
  </td></tr>
  </table><br/>
  <?php }?>
  Email address of contact author (be sure to type it correctly as you will never be able to modify it), multiple addresses can be specified by separating them by commas (<em>i.e.</em> name@domain1.com, second.name@domain2.net):<br />
  <input name="contact" type="text" size="80"  value="<?php Tools::printHTML(Tools::readPost('contact'));?>"/><br />
  <?php if(Tools::useAbstract()) {?>
    Abstract:<br />
    <textarea name="abstract" cols="80" rows="25"><?php Tools::printHTML(Tools::readPost('abstract')); ?></textarea><br />
  <?php }?>
  <?php if(Tools::useCategory() || Tools::useKeywords()) {?>
    <h2>Topic</h2>
  <?php }?>
  <?php if(Tools::useCategory()) {?>
    Please select the most suitable topic from the following list:<br />
    <?php Tools::printCategories(Tools::readPost('category'),'category'); ?>
    <br />
  <?php }?>
  <?php if(Tools::useKeywords()) {?>
    Keywords (the size of this field is limited to 80 characters):<br />
    <input name="keywords" type="text" size="80" maxlength="80" value="<?php Tools::printHTML(Tools::readPost('keywords'));?>"/><br />
  <?php }?>
  <h2>File</h2>  
  Upload your file (limited to <?php print(ini_get('upload_max_filesize'));?>): <br /><input name="file" type="file" size="80" /><br />
  <input type="hidden" name="action" value="submit" />
  <center>
  <table class="disclaimer"><tr>
  <td><input type="checkbox" class="noBorder" name="disclaimer" id="disclaimer" value="yes" <?php if(Tools::readPost('disclaimer') != ""){ print('checked="checked" ');}?>/></td>
  <td><label for="disclaimer">
    On behalf of all authors, I hereby declare in good faith that this
    submitted paper is compliant with the instructions as stated in the call
    for papers and is not an irregular submission. In particular, it has not
    been submitted for publication in a substantially similar form in
    parallel. It does not substantially resubmit any previously published
    result, up to my knowledge. It is not a plagiarism of content as
    understood by the academic community.
  </label></td>
  </tr></table>
  <input type="submit" class="buttonLink bigButton" value="Submit your Paper" />
  </center>
</form>

<?php 
}
?>

<?php include("footer.php"); ?>
