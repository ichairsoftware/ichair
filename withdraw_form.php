<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Withdrawal Form';
include 'utils/tools.php';
if(!Tools::isConfigured()) {return;}
include 'header.php';

/* Check that the author is still on time... */

if(Tools::serverIsShutdown()) {
  Tools::printServerShutdownMessage();
} else {


/* Create a new object submission */

$id = trim(Tools::readPost('id'));
if (Tools::isAnId($id)) {
    $submission = Submission::getByID($id);
}
if (is_null($submission)) {
?>

<div class="ERRmessage">
 We could not find any submission matching ID <i><?php Tools::printHTML($id) ?></i> &nbsp;in our database. Please make sure you typed it correctly.
</div>
<form action="withdraw.php" method="post">
<div class="floatRight">
<input type="submit" class="buttonLink bigButton" value="Go Back" />
</div>
</form>

<?php   
} else if($submission->getIsWithdrawn()) {
?>

<div class="ERRmessage">
 The submission with ID <i><?php Tools::printHTML($id); ?></i> &nbsp;has already been withdrawn.
</div>
<form action="withdraw.php" method="post">
<div class="floatRight">
<input type="submit" class="buttonLink bigButton" value="Go Back" />
</div>
</form>

<?php   
  
}
else { 

  $version = $submission->getLastVersion();
  print '<div class="OKmessage">';
  $submission->printInfo();
  ?>
</div>
<div class="paperBox">
  <div class="paperBoxTitle">
    <span class='paperBoxNumber'>Last Submitted Version</span>
    <?php 
    $version->printShort();
    ?>
  </div>
  <div class="paperBoxDetails">
  <?php 
  $version->printLongBr();
  ?>
  </div>
</div>

<form action="withdraw_result.php" method="post" enctype="multipart/form-data">
  <input type="hidden" name="id" type="text" size=80 value="<?php print $submission->getId()?>">
  <center>
    <input type="submit" class="buttonLink bigButton" value="Withdraw your Submission" />
  </center>
</form>

<?php 
}}
?>
</body>
</html>
