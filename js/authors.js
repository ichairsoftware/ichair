$('document').ready(function(){
  function setClick(target) {
    target.click(function(){
      if ($('.authorBox').length != 1) {
        var parentBox = $(this).parents('.authorBox');
        parentBox.animate({width: '0px'}, 300, function(){
          parentBox.remove();
        });   
      } else {
        alert('All submissions must have at least one author!');
      }
      return false;
    });
  };

  setClick($('.removeAuthor'));

  $('#addAuthor').click(function(){
    var newAuthor = $('.authorBox').first().clone();
    newAuthor.hide();
    setClick(newAuthor.find('.removeAuthor'));
    newAuthor.find('input[name^=authors]').val('');
    newAuthor.find('input[name^=affiliations]').val('');
    newAuthor.appendTo('#authorsDiv');
    newAuthor.fadeIn();
    return false;
  });
});