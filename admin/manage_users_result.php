<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Manage Users";
include '../utils/tools.php';
include "header.php";

$reviewer = new Reviewer();
$reviewer->createFromPost();
if($reviewer->getStatus() != "") {
?>
There was an error with your new reviewer. See the error message bellow for more details.
<div class="ERRmessage">
<?php Tools::printHTMLbr($reviewer->getStatus()); ?>
</div>
<form action="manage_users.php" method="post">
  <input name="login" type="hidden" value="<?php Tools::printHTML(Tools::readPost('login')); ?>"  />
  <input name="fullName" type="hidden" value="<?php Tools::printHTML(Tools::readPost('fullName')); ?>"  />
  <input name="email" type="hidden" value="<?php Tools::printHTML(Tools::readPost('email')); ?>"  />
  <input name="group" type="hidden" value="<?php Tools::printHTML(Tools::readPost('group')); ?>"  />
  <div class="floatRight">
  <input type="submit" class="buttonLink bigButton" value="Go Back" />
  </div>
</form>
<?php 
} else {
Log::logAddUser($reviewer);
?>
<div class="OKmessage">
The user <?php Tools::printHTML(Tools::readPost('login')); ?> has been successfully inserted in the database. 
Don't forget to create a password for this user.
</div> 
<form action="manage_users.php" method="post">
  <div class="floatRight">
  <input type="submit" class="buttonLink bigButton" value="Ok" />
  </div>
</form>

<?php 
}
?>

</body>
</html>
