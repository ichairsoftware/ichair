<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Manage Users";
include '../utils/tools.php';
include "header.php";

/* Check if the admin has a password */
Tools::adminHasNoPassword();

/* Check if the admin has properly configured iChair */

$status = Tools::getAdminConfigStatus();
if($status != "") {
  print('<div class="ERRmessage">' . $status . '</div>');
  return null;
}

/* If we reach this point the config is ok */

/* Check if the database exists. If not, create an empty one. */
$reviewers = Reviewer::getAllByGroup(Reviewer::$CHAIR_GROUP);
if (count($reviewers) != 0) {
?>
<h2>Program Chairs</h2>
<table class="usersTable">
<tr>
  <td>&nbsp;</td>
  <td>Login</td>
  <td>Full Name</td>
  <td>e-mail</td>
  <td>password generated</td>
</tr>
<?php 
foreach($reviewers as $reviewer) {
  ?>
  <tr>
    <td><div class="bigNumber"><?php Tools::printHTML($reviewer->getReviewerNumber())?></div></td>
    <td><?php Tools::printHTML($reviewer->getLogin())?></td>
    <td><?php Tools::printHTML($reviewer->getFullName())?></td>
    <td><?php Tools::printHTML($reviewer->getEmail())?></td>
    <td><?php $reviewer->printPasswordSet()?></td>
    <td>
      <form action="manage_users_edit.php" method="post">
        <input type="hidden" name="reviewerNumber" value="<?php print($reviewer->getReviewerNumber()); ?>" />
        <input type="submit" class="buttonLink" value="Edit" />
      </form>
    </td>
    <td>
      <form action="manage_users_delete.php" method="post">
        <input type="hidden" name="reviewerNumber" value="<?php print($reviewer->getReviewerNumber()); ?>" />
        <input type="submit" class="buttonLink" value="Delete" />
      </form>
    </td>
    <td>
      <form action="manage_users_password.php" method="post">
        <input type="hidden" name="reviewerNumber" value="<?php print($reviewer->getReviewerNumber()); ?>" />
        <input type="submit" class="buttonLink" value="Generate and Email Password" />
      </form>
    </td>
  </tr>
  <?php 
}
?>
</table>
<?php 
}
$reviewers = Reviewer::getAllByGroup(Reviewer::$REVIEWER_GROUP);
if (count($reviewers) != 0) {   ?>

<h2>Program Committee</h2>
<table class="usersTable">
<tr>
  <td>&nbsp;</td>
  <td>Login</td>
  <td>Full Name</td>
  <td>e-mail</td>
  <td>password generated</td>
</tr>
<?php 
foreach($reviewers as $reviewer) {
  ?>
  <tr>
    <td><div class="bigNumber"><?php Tools::printHTML($reviewer->getReviewerNumber())?></div></td>
    <td><?php Tools::printHTML($reviewer->getLogin())?></td>
    <td><?php Tools::printHTML($reviewer->getFullName())?></td>
    <td><?php Tools::printHTML($reviewer->getEmail())?></td>
    <td><?php $reviewer->printPasswordSet()?></td>
    <td>
      <form action="manage_users_edit.php" method="post">
        <input type="hidden" name="reviewerNumber" value="<?php print($reviewer->getReviewerNumber()); ?>" />
        <input type="submit" class="buttonLink" value="Edit" />
      </form>
    </td>
    <td>
      <form action="manage_users_delete.php" method="post">
        <input type="hidden" name="reviewerNumber" value="<?php print($reviewer->getReviewerNumber()); ?>" />
        <input type="submit" class="buttonLink" value="Delete" />
      </form>
    </td>
    <td>
      <form action="manage_users_password.php" method="post">
        <input type="hidden" name="reviewerNumber" value="<?php print($reviewer->getReviewerNumber()); ?>" />
        <input type="submit" class="buttonLink" value="Generate and Email Password" />
      </form>
    </td>
  </tr>
<?php } ?>
</table>
<?php 
}        
$reviewers = Reviewer::getAllByGroup(Reviewer::$OBSERVER_GROUP);
if (count($reviewers) != 0) { ?>

<h2>Neutral Observers</h2>
<table class="usersTable">
<tr>
  <td>&nbsp;</td>
  <td>Login</td>
  <td>Full Name</td>
  <td>e-mail</td>
  <td>password generated</td>
</tr>
<?php 
foreach($reviewers as $reviewer) {
  ?>
  <tr>
    <td><div class="bigNumber"><?php Tools::printHTML($reviewer->getReviewerNumber())?></div></td>
    <td><?php Tools::printHTML($reviewer->getLogin())?></td>
    <td><?php Tools::printHTML($reviewer->getFullName())?></td>
    <td><?php Tools::printHTML($reviewer->getEmail())?></td>
    <td><?php $reviewer->printPasswordSet()?></td>
    <td>
      <form action="manage_users_edit.php" method="post">
        <input type="hidden" name="reviewerNumber" value="<?php print($reviewer->getReviewerNumber()); ?>" />
        <input type="submit" class="buttonLink" value="Edit" />
      </form>
    </td>
    <td>
      <form action="manage_users_delete.php" method="post">
        <input type="hidden" name="reviewerNumber" value="<?php print($reviewer->getReviewerNumber()); ?>" />
        <input type="submit" class="buttonLink" value="Delete" />
      </form>
    </td>
    <td>
      <form action="manage_users_password.php" method="post">
        <input type="hidden" name="reviewerNumber" value="<?php print($reviewer->getReviewerNumber()); ?>" />
        <input type="submit" class="buttonLink" value="Generate and Email Password" />
      </form>
    </td>
  </tr>
  <?php 
}
?>
</table>
<?php } ?>
<h2>Add a New User</h2>
<form action="manage_users_result.php" method="post">
<table class="usersTable">
<tr>
  <td>Login</td>
  <td>Full Name</td>
  <td>e-mail</td>
  <td colspan="3">Reviewer Group</td>
</tr>
<tr>
  <td><input type="text" size="10" name="login" value="<?php Tools::printHTML(Tools::readPost('login')); ?>"/></td>
  <td><input type="text" size="30" name="fullName" value="<?php Tools::printHTML(Tools::readPost('fullName')); ?>"/></td>
  <td><input type="text" size="30" name="email" value="<?php Tools::printHTML(Tools::readPost('email')); ?>"/></td>
  <td>
    <input class="noBorder" type="radio" id="chairRadio" name="group" value="<?php print(Reviewer::$CHAIR_GROUP); ?>" <?php if(Tools::readPost('group') == Reviewer::$CHAIR_GROUP) print("checked=\"checked\""); ?>/><label for="chairRadio">Chair</label>
  </td>
  <td>
    <input class="noBorder" type="radio" id="revRadio" name="group" value="<?php print(Reviewer::$REVIEWER_GROUP); ?>" <?php if((Tools::readPost('group') != Reviewer::$CHAIR_GROUP) && (Tools::readPost('group') != Reviewer::$OBSERVER_GROUP)) print("checked=\"checked\""); ?>/><label for="revRadio">Reviewer</label>
  </td>
  <td>
    <input class="noBorder" type="radio" id="obsRadio" name="group" value="<?php print(Reviewer::$OBSERVER_GROUP); ?>" <?php if(Tools::readPost('group') == Reviewer::$OBSERVER_GROUP) print("checked=\"checked\""); ?>/><label for="obsRadio">Observer</label>
  </td>
  <td><input type="submit" class="buttonLink" value="Add User" /></td>
</tr>
</table>
</form>

</body>
</html>
