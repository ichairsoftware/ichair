<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Main Page';
include('../utils/tools.php');
include('header.php');
Tools::adminHasNoPassword();
?>

<p>Welcome to the iChair administration page. </p>

<h2><a href="configuration.php">Configuration</a></h2>

<p>The <a href="configuration.php">Configuration</a> page is the most important
page in iChair: this is where you can configure everything from the conference name
to the directories where the reviews should be stored. The software will only function
correctly once everything is configured as it should. A detailed description of this
page can be found in the manual coming with iChair.</p>

<h2><a href="admin_password.php">Admin Password</a></h2>

<p>Use the <a href="admin_password.php">Admin Password</a> page to set/modify the admin
password. Setting an admin password is the first thing to do after installing iChair.
Remember that the login for the administration pages is always <em>admin</em>, to
change it you will have to edit the .htpasswd file by hand.</p>

<h2><a href="manage_users.php">Manage Users</a></h2>

<p>The <a href="manage_users.php">Manage Users</a> page will let you edit the list of
users having access to the review pages. You have the possibility to create three types
of users: chairs (having many access privileges), reviewers and observers (those can
see everything, but cannot interfere with the review process). You always need to
configure at least one account as chair, otherwise it will not be possible to operate
the software correctly.</p>

<p>Once you have created some users do not forget to generate their passwords
otherwise they will never have the possibility to log on the server. Also be sure
to generate these passwords before the program chair starts sending emails to
the reviewers telling them to log on!</p>


<h2><a href="submit.php">Emergency Submission</a> &amp; <a href="revise.php">Emergency Revision</a></h2>

<p>When the submission deadline is passed and the server is shutdown, the administrator
still has the possibility to submit or revise a paper using the
<a href="submit.php">Emergency Submission</a> and the 
<a href="revise.php">Emergency Revision</a> pages.</p>


<h2><a href="logs.php">Logs</a></h2>

The <a href="logs.php">Logs</a> page can be used by the administrator to monitor the
activity on the server. Every action performed by him, an author or a reviewer is
logged. Note that this is not intended to replace the apache logs, but only comes as
a complement, especially for statistical purpose.


<h2><a href="rescue_emails.php">Rescue E-mails</a></h2>

The <a href="rescue_emails.php">Rescue E-mails</a> page should normally not have to
be used. However, it can happen that sendmail sometimes is not able to fulfil an email
request: when this is the case, you should be able to recover the content of this mail
with the <a href="rescue_emails.php">Rescue E-mails</a> page. Note that the email might
still be stored in the mail queue of sendmail: if the error was due to a strangely
configured SMTP server you might have to remove it manually.

<?php include('w3c.php'); ?>
</body>
</html>
