<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Change Admin Password";
include("../utils/tools.php");
include("header.php");

?>

<center>
<form action="admin_password_result.php" method="post">
  <table>
    <tr>
      <td>Old Administrator Password:</td>
      <td><input type="password" name="old" /></td>
    </tr>
    <tr>
      <td>New Administrator Password:</td>
      <td><input type="password" name="new1" /></td>
    </tr>
    <tr>
      <td>Repeat New Administrator Password:</td>
      <td><input type="password" name="new2" /></td>
    </tr>
  </table>
  <input type="submit" class="buttonLink bigButton" value="Update Password" />
</form>
</center>

</body>
</html>
