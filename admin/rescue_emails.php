<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Main Page';
include '../utils/tools.php';
include 'header.php';

/* Check if the admin has a password */
Tools::adminHasNoPassword();

/* Check if the admin has properly configured iChair */

$status = Tools::getAdminConfigStatus();
if($status != "") {
  print('<div class="ERRmessage">' . $status . '</div>');
  return null;
}

$result = Log::getAllBadEmails();
if (!$result->fetchArray()) {
  print('<div class="OKmessage">There are no emails to rescue at the moment.</div>');
} else {
  $result->reset();
  while($db_row = $result->fetchArray()) {
    $code = explode(" ", base64_decode($db_row['description']));
    $subject = base64_decode($code[1]);
    $body = base64_decode($code[2]); 
    $header = base64_decode($code[3]); 
    ?>

<div class="paperBox">
  <div class="paperBoxDetails">
    <div class="versionTitle"><?php Tools::printHTML(base64_decode($db_row['performer']));?></div>
    <b>Header:</b><br/>
    <div class="versionAbstract"><?php Tools::printHTMLbr($header);?></div>
    <b>Subject:</b><br/>
    <div class="versionAbstract"><?php Tools::printHTMLbr($subject);?></div>
    <b>Body:</b><br/>
    <div class="versionAbstract"><?php Tools::printHTMLbr($body);?></div>
    <?php if(Tools::use12HourFormat()) { ?>
      <div class="floatRight"><?php print(gmdate("D, j M Y h:i:s a",$db_row['date']));?></div>
    <?php } else { ?>
      <div class="floatRight"><?php print(gmdate("D, j M Y H:i:s",$db_row['date']));?></div>
    <?php } ?>
    <div class="clear"></div>
  </div>
</div>

    <?php  
  }
}
?>
</body>
</html>
