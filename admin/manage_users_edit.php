<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Edit User";
include '../utils/tools.php';
include "header.php";

$login;
$fullName;
$email;
$group;

$reviewer = Reviewer::getByReviewerNumber(Tools::readPost('reviewerNumber'));
if(!is_null($reviewer)) {
  
  if(Tools::readPost('login') == "") {
    $login = $reviewer->getLogin();
  } else {
    $login = Tools::readPost('login');
  }

  if(Tools::readPost('fullName') == "") {
    $fullName = $reviewer->getFullName();
  } else {
    $fullName = Tools::readPost('fullName');
  }

  if(Tools::readPost('email') == "") {
    $email = $reviewer->getEmail();
  } else {
    $email = Tools::readPost('email');
  }

  if(Tools::readPost('group') == "") {
    $group = $reviewer->getGroup();
  } else {
    $group = Tools::readPost('group');
  }
  
  ?>

  <form action="manage_users_edit_result.php" method="post">
  <input name="reviewerNumber" type="hidden" value="<?php Tools::printHTML(Tools::readPost('reviewerNumber')); ?>" />
  <center>
  <table class="usersTable">
  <tr>
    <td>Login</td>
    <td>Full Name</td>
    <td>e-mail</td>
    <td colspan="3">Reviewer Group</td>
  </tr>
  <tr>
    <td><input type="text" size="10" name="login" value="<?php Tools::printHTML($login); ?>"/></td>
    <td><input type="text" size="30" name="fullName" value="<?php Tools::printHTML($fullName); ?>"/></td>
    <td><input type="text" size="30" name="email" value="<?php Tools::printHTML($email); ?>"/></td>
  <td>
    <input class="noBorder" type="radio" id="chairRadio" name="group" value="<?php print(Reviewer::$CHAIR_GROUP); ?>" <?php if($group == Reviewer::$CHAIR_GROUP) print("checked=\"yes\""); ?>/><label for="chairRadio">Chair</label>
  </td>
  <td>
    <input class="noBorder" type="radio" id="revRadio" name="group" value="<?php print(Reviewer::$REVIEWER_GROUP); ?>" <?php if(($group != Reviewer::$CHAIR_GROUP) && ($group != Reviewer::$OBSERVER_GROUP)) print("checked=\"yes\""); ?>/><label for="revRadio">Reviewer</label>
  </td>
  <td>
    <input class="noBorder" type="radio" id="obsRadio" name="group" value="<?php print(Reviewer::$OBSERVER_GROUP); ?>" <?php if($group == Reviewer::$OBSERVER_GROUP) print("checked=\"yes\""); ?>/><label for="obsRadio">Observer</label>
  </td>
  </tr>
  </table>
    <input type="submit" class="buttonLink bigButton" value="Update User" />
  </center>
  </form>

<?php 
}
?>

</body>
</html>
