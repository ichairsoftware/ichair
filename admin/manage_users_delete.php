<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Delete User";
include '../utils/tools.php';
include "header.php";

$reviewer = Reviewer::getByReviewerNumber(Tools::readPost('reviewerNumber'));
if(!is_null($reviewer)) {
  ?>
  <center>
  <table class="usersTable">
    <tr>
      <td>Login</td>
      <td>Full Name</td>
      <td>e-mail</td>
      <td>Reviewer Group</td>
    </tr>
    <tr>
      <td><?php Tools::printHTML($reviewer->getLogin())?></td>
      <td><?php Tools::printHTML($reviewer->getFullName())?></td>
      <td><?php Tools::printHTML($reviewer->getEmail())?></td>
      <td><?php Tools::printHTML($reviewer->getGroup())?></td>
    </tr>
  </table> 
  <table>
  <tr>
  <td>
  <form action="manage_users.php" method="post">
    <input type="submit" class="buttonLink bigButton" value="Go Back" />
  </form>
  </td>
  <td>
  <form action="manage_users_delete_result.php" method="post">
    <input name="reviewerNumber" type="hidden" value="<?php Tools::printHTML(Tools::readPost('reviewerNumber')); ?>" />
    <input type="submit" class="buttonLink bigButton" value="Delete User" />
  </form>
  </td>
  </tr>
  </table>
  </center>

<?php 
}
?>

</body>
</html>
