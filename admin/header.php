<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
header("Content-type: text/html; charset=iso-8859-1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=iso-8859-1" />
  <title><?php Tools::printHTML(Tools::getConfig("conference/name")." Admin - ".$page_title); ?></title>
  <link href="../css/admin_<?php print(Tools::getConfig("miscellaneous/adminSkin"));?>.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="../css/print_<?php print(Tools::getConfig("miscellaneous/adminSkin"));?>.css" rel="stylesheet" type="text/css" media="print" />
  <link rel="SHORTCUT ICON" type="image/png" href="../images/icon_admin.png" />
  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/authors.js"></script>  
</head>

<body>

<div class="navigate">

  <div class="navigateTitle"><a href="../index.php"><?php Tools::printHTML(Tools::getConfig("conference/name"));?></a></div>
    <div class="navigateLinks">
      <p><a href="index.php">Main Page</a></p>
      <p><a href="admin_password.php">Admin Password</a></p>
      <p><a href="admin_login.php">Admin Login</a></p>
      <p><a href="configuration.php">Configuration</a></p>
      <p><a href="file_uploads.php">Upload Files</a></p>
      <p><a href="manage_users.php">Manage Users</a></p>
      <p><a href="logs.php">Logs</a></p>
      <p><a href="rescue_emails.php">Rescue E-mails</a></p>
      <p class="topLine"><a href="submit.php">Emergency Submission</a></p>
      <p><a href="revise.php">Emergency Revision</a></p>
      <p><a href="change_contact_email.php">Change Contact e-mail</a></p>
      <p class="topLine"><a href="about.php">About iChair</a></p>
    </div>

    <div class="navigateTitle">
      <a href="http://www.time.gov/timezone.cgi?UTC/s/0/java" target="_new">Current Time</a>
    </div>
    <div class="navigateTimes">
      <div class="timeTitleTop">UTC Time</div>
      <?php if(Tools::use12HourFormat()) { ?>
        <div class="time"><?php print(gmdate("j M y - h:i&#160;a")); ?></div>
      <?php } else {?>
        <div class="time"><?php print(gmdate("j M y - H:i")); ?></div>      
      <?php } ?>
    </div>

    <div class="navigateTitle">
      Server Shutdown
    </div>
    <div class="navigateTimes">
      <div class="timeTitleTop">UTC Time</div>
      <div class="time">
        <?php 
          $U = Tools::getUFromDateAndTime(Tools::getConfig("server/shutdownDate"), Tools::getConfig("server/shutdownTime"));
	  if(Tools::use12HourFormat()) {
	    print(gmdate("j M y - h:i&#160;a", $U)); 
	  } else {
	    print(gmdate("j M y - H:i", $U)); 
	  }
        ?>
      </div>
      <div class="timeTitle">Time left</div>
      <div class="time">
      <?php 
      $timeLeft = $U - gmdate("U");
      if($timeLeft > 0) {
        print(Tools::stringTimeLeft($timeLeft));
      } else {
        print("<div class=\"timeUp\">Time is up!</div>");
      }
      ?>
      </div>
    </div>

  </div>

<h1><?php Tools::printHTML(Tools::getConfig("conference/name")." Admin - ".$page_title); ?></h1>
