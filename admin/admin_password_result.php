<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php 
$page_title = "Change Admin Password";
include '../utils/tools.php';
include "header.php";

$status = "";
$oldHashExists = file_exists(".htpasswd") || file_exists(".htaccess");
$htpasswd="";
$login="admin";
$oldHash = "";
$oldSalt = "";
if ($oldHashExists) {
  $htpasswd = file_get_contents(".htpasswd");
  $htpasswd = trim($htpasswd);
  $pos = strpos($htpasswd,":");
  $login = substr($htpasswd,0,$pos);
  $oldHash = substr($htpasswd,$pos+1);
  if(preg_match("/^[$]1[$]/", $oldHash)) {
    $pos2 = strpos(substr($oldHash,3),"$");
    $oldSalt = substr($oldHash,0,$pos2+4);
  } else {
    $oldSalt = substr($oldHash,0,2);
  }
}



if($oldHashExists && ($oldHash != crypt(Tools::readPost('old'), $oldSalt))) {
  $status .= "Incorrect old password.\n";
} else if(Tools::readPost('new1') != Tools::readPost('new2')) {
  $status .= "The two new passwords do not match.\n";  
} 

if($status != "") {
  print("<div class=\"ERRmessage\">" . $status . "</div>");

?>
  <form action="admin_password.php" method="post">
  <div class="floatRight">
  <input type="submit" class="buttonLink bigButton" value="Go Back" />
  </div>
  </form>
<?php 
  
} else {

  $newSalt = '$1$' . substr(md5(time() . getmypid()),0,8) . '$';
  $newHash = crypt(Tools::readPost('new1'),$newSalt);
  $htaccessContents = 'AuthUserFile ' . realpath(".") . "/.htpasswd\n";
  $htaccessContents .= 'AuthName Admin
AuthType Basic
require valid-user
Allow from all';
  file_put_contents(".htaccess", $htaccessContents . "\n");
  file_put_contents(".htpasswd", $login . ":" . $newHash . "\n");
  if (file_exists(Tools::getConfig('server/logsPath'))) {
    Log::logAdminPassword();
  }
  print("<div class=\"OKmessage\"> Password for user <em>" . $login . "</em> has been successfuly updated.</div>");

  ?>
  
  <form action="index.php" method="post">
  <div class="floatRight">
    <input type="submit" class="buttonLink bigButton" value="Ok" />
  </div>
  </form>

<?php 

}

?>

