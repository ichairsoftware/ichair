<?php 

/*
 * Copyright (C) 2006, 2007 Thomas Baigneres, Matthieu Finiasz
 *
 * This file is part of iChair.
 *
 * iChair is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 * 
 * iChair is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

?><?php $page_title='Revision Form';
include '../utils/tools.php';
include 'header.php';

/* Create a new object submission */

$id = Tools::readPost('id');

if (Tools::isAnId($id)) {
  $submission = Submission::getByID($id);
}
if (is_null($submission)) {
?>

<div class="ERRmessage">
 We could not find any submission matching ID <i><?php Tools::printHTML(Tools::readPost('id'));?></i> &nbsp;in our database. Please make sure you typed it correctly.
</div>
<form action="revise.php" method="post">
<div class="floatRight">
<input type="submit" class="buttonLink bigButton" value="Go Back" />
</div>
</form>

<?php   
} else if($submission->getIsWithdrawn()) {
?>

<div class="ERRmessage">
 The submission with ID <i><?php Tools::printHTML(Tools::readPost('id'));?></i> &nbsp;has been withdrawn. Please use the submission form to re-submit it.
</div>
<form action="revise.php" method="post">
<div class="floatRight">
<input type="submit" class="buttonLink bigButton" value="Go Back" />
</div>
</form>

<?php   
  
}
else { 

  $versions = $submission->getAllVersions();
  print '<div class="OKmessage">';
  $submission->printInfo();
  print '</div>';
  for ($i = count($versions) ; $i>0; $i--){
    print '<div class="paperBox"><div class="paperBoxTitle">';
    print("<span class=\"paperBoxNumber\">Version " . $versions[$i]->getVersionNumber() ."</span>");
    $versions[$i]->printShort();
    print '</div><div class="paperBoxDetails">';
    if($i == count($versions)) {
    ?>
    <form action="revise_process.php" method="post" enctype="multipart/form-data">
      Title:            <br>
      <input name="title" type="text" size=80 value="<?php Tools::printHTML($versions[$i]->getTitle());?>"/><br>
      <div id="authorsDiv">
      Authors (include ALL authors to appear in the final version):<a id="addAuthor" class="buttonLink" href="#">Add an author</a><br/>
      <?php
        $authors = $versions[$i]->getAuthorsArray();
        $affiliations = $versions[$i]->getAffiliationsArray();
        $country = $versions[$i]->getCountryArray();
        foreach ($authors as $key => $author) {
      ?>
      <div class="authorBox"><div class="authorBoxWidth">
      <a class="floatRight removeAuthor" href="#">Remove author</a>
        Full name:<br /><input name="authors[]" type="text" size="35"  value="<?php Tools::printHTML($authors[$key]);?>"/>
        <?php if(Tools::useAffiliations()) {?>
          <br/>Affiliation: <br /><input name="affiliations[]" type="text" size="35"  value="<?php Tools::printHTML($affiliations[$key]); ?>"/>
        <?php }?>
        <?php if(Tools::useCountry()) {?>
          <br/>Countries: <br /><?php Tools::printCountrySelect($country[$key]); ?>
        <?php } ?>
      </div></div>
      <?php } ?>
      </div>
    <div class="clear"></div>
  <table><tr><td>
  <input type="radio" class="noBorder" name="committeeMember" id="notCommittee" value="no"<?php 
    if (!$submission->getIsCommitteeMember()) {
      print(' checked="checked"');
    }
  ?> /></td><td><label for="notCommittee">None of the authors above is a member of the <?php Tools::printHTML(Tools::getConfig('conference/name'));?> program committee.</label></td></tr><tr><td>
  <input type="radio" class="noBorder" name="committeeMember" id="yesCommittee" value="yes"<?php 
    if ($submission->getIsCommitteeMember()) {
      print(' checked="checked"');
    }
  ?> /></td><td><label for="yesCommittee">This paper involves a member of the <?php Tools::printHTML(Tools::getConfig('conference/name'));?> program committee.</label></td></tr></table><br/>
  <?php if(Tools::useCustomCheck1()) {?>
  <table><tr><td>
    <input type="checkbox" class="noBorder" name="customCheck1" id="customCheck1" value="yes"<?php
      if ($submission->getIsCustomCheck1()) {
        print(' checked="checked"');
      }
  ?> /></td><td><label for="customCheck1"><?php Tools::printHTML(Tools::getCustomCheck1(true)); ?></label>
  </td></tr></table><br />
  <?php }?>
  <?php if(Tools::useCustomCheck2()) {?>
  <table><tr><td>
    <input type="checkbox" class="noBorder" name="customCheck2" id="customCheck2" value="yes"<?php
      if ($submission->getIsCustomCheck2()) {
        print(' checked="checked"');
      }
  ?> /></td><td><label for="customCheck2"><?php Tools::printHTML(Tools::getCustomCheck2(true)); ?></label>
  </td></tr></table><br />
  <?php }?>
  <?php if(Tools::useCustomCheck3()) {?>
  <table><tr><td>
    <input type="checkbox" class="noBorder" name="customCheck3" id="customCheck3" value="yes"<?php
      if ($submission->getIsCustomCheck3()) {
        print(' checked="checked"');
      }
  ?> /></td><td><label for="customCheck3"><?php Tools::printHtml(Tools::getCustomCheck3(true)); ?></label>
  </td></tr></table><br />
  <?php }?>
      <?php if(Tools::useAbstract()) {?>
        Abstract: <br>
        <textarea name="abstract" cols="80" rows="25"><?php Tools::printHTML($versions[$i]->getAbstract());?></textarea><br>
      <?php }?>
     <?php if(Tools::useCategory() || Tools::useKeywords()) {?>
       <h2>Topic</h2>
     <?php }?>
     <?php if(Tools::useCategory()) {?>
       Please select the most suitable topic from the following list:<br/>
     <?php 
       Tools::printCategories($versions[$i]->getCategory(),'category');
     ?>
     <br>
     <?php }?>
    <?php if(Tools::useKeywords()) {?>
      Keywords (the size of this field is limited to 80 characters):<br>
      <input name="keywords" type="text" size=80 maxlength="80" value="<?php Tools::printHTML($versions[$i]->getKeywords());?>"/><br>
     <?php }?>	
    <h2>File</h2>    
    <input type="hidden" name="id" value="<?php Tools::printHTML(Tools::readPost('id')); ?>" type="text">
    <input type="hidden" name="selected_version" value="<?php print $i;?>">
    Upload your file (limited to <?php print(ini_get('upload_max_filesize'));?>, leave blank if you do not want to submit a new version of your file): <br>
    <input name="file" type="file" size="80"><br>
    <input type="checkbox" class="noBorder" name="mailNotification" id="mailNotification" value="nomail" />
    <label for="mailNotification">Do NOT send a notification email to the contact author.</label>
    <center>
      <input type="submit" class="buttonLink bigButton" value="Revise your Paper" />
    </center>
    </form>
    <div class="clear"> </div>
    <?php 
    } else {
      $versions[$i]->printLong();
    }
    print '</div></div>';
  }



?>


<?php 
}
?>

</body>
</html>

